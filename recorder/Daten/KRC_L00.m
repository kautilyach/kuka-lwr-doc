load -MAT KRC_L00D.MAT
%signals to Follow 
cmdBuf_cmd_jntPos = doubleVals(:,1:7);
msrBuf_jntPosMsr = doubleVals(:,8:14);
msrBuf_jntTorqueMsr = doubleVals(:,15:21);
msrBuf_forceTorqueTCPMsr = doubleVals(:,22:27);
msrBuf_forceTorqueTCPVariance = doubleVals(:,28:33);
cmdBuf_cart = doubleVals(:,34:39);
cmdBuf_tool = doubleVals(:,40:45);
msrBuf_cart = doubleVals(:,46:51);
msrBuf_cart_est = doubleVals(:,52:57);


%%%%%%%%%%%%%%%%%%%%%%%%%%
% 58 57


load -MAT KRC_L00S.MAT
%signals to Follow 
msrBuf_robstate = shortVals(:,1);
curInteractionControlStrategy = shortVals(:,2);
msrBuf_activeControlStrategy = shortVals(:,3);
resetState = shortVals(:,4);
cmdBuf_cmd_motion = shortVals(:,5);
cmdBuf_cmd_motorOn = shortVals(:,6);
KRC_KRLU_Count = shortVals(:,7);


%%%%%%%%%%%%%%%%%%%%%%%%%%
% 8 7


%

% memOne 478
% steps 4184
theTiming = [ -86923478 -86904080 3233 145.244 184.04 ;  ];
KRC_LBRTiming = theTiming;
totalSamples = 3233;
totalTime = [ 19398 38.796 ];
timeBase = [ 1/500 1/1000 ];

KRCLBR_jntOffsets = [ 0 -1.5708 0 0 0 0 0  ];
