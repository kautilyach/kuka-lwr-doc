LBR Record Torques 

Start des Recorders mit RECstart_()
Stoppen des Recorders mit RECstop_()
Stoppen mit schreiben der Messfiles auf HDD mit RECstopWithWrite_()

Beispiel f�r die Anwendung in TestRec.src

F�r eine Aufzeichnung (ein Datenfile) kann der Recorder mehrmals gestartet und gestopt werden.
Nach Ausf�hrung von RECstopWithWrite_() werden die Files auf HDD geschrieben. Mit einem folgenden RECstart_() wird ein neues Datenfile gestartet.

Maximale Aufzeichnungsdauer ist ca. 60 s f�r ein Datenfile.

Die Matlab-Messfiles (Beispiele siehe Folder "Daten") werden bei der Messung auf der KRC im Verzeichnis C:\KRC\Roboter\Dlrrc\Output abgelegt.
Bei mehreren Messungen hintereinander wird die letzte Zahl im Dateinamen inkrementiert.

nach der Aufzeichnung werden mehrere Datenfiles gespeichert mit folgenden Namen:
ACYC*.* f�r azyklische Daten wie z.B. der aktuelle Controller
KRC_*.* f�r zyklische Daten die im 12ms Kontext der KRC existieren (z.B. kommandierte Position)
ROB_*.* f�r zyklische Daten die im 1ms Kontext existieren (z.B. aktuell gemessene Momente)