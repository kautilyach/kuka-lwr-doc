LBR Record Torques 

Start of recorder with RECstart_()
Stop of recorder with RECstop_()
Stop with writing of measurementfiles to HDD with RECstopWithWrite_()

Exampleprogram for usage in TestRec.src

for one recording (one datarecord) the recorder can be started and stopped several times.
After executing RECstopWithWrite_() the files will be written to HDD. After a following RECstart_() a new datafile will be started.

Maximum recording time app. 60 s for one record.

The matlab files (examples in folder "Daten") will be stored in the directory C:\KRC\Roboter\Dlrrc\Output.
they are packed and can be unpacked by executing the *.bat- files. (you have to have gzip installed).
for subsequent measurements the last number of filename will be incremented.

during on record several datafiles are written named 
ACYC*.* for acyclic data like active controller, etc.
KRC_*.* for cyclic data existing in 12ms KRC context (e.g. commanded position)
ROB_*.* for cyclic data existing in 1ms context (e.g. actual torques)