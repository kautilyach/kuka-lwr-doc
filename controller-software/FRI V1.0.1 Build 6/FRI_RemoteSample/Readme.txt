The Sample Code for KUKA.FastResearchInterface (FRI) is organized as follows:


FRI_Remote        -> All library code, which is essential to run the FRI on the remote side
FRI_FirstApp      -> The first getting started sample code
                     Note: This (and all other code) is linked against the Code of FRI_Remote
FRI_SecondApp     -> a more versatile sample, utitlizing more features of FRI
                     Note: This (and all other code) is linked against the Code of FRI_Remote

For Windows/ Microsoft Visual Studio 2008:
FRI_TestClnt.sln/suo  -> A solution, which indexes all of the projects
FRI_GuiApp         -> A simple sample GUI for utilizing the FRI Features
                     Note: This (and all other code) is linked against the Code of FRI_Remote
