
/*{{[PH]
****************************************************************************
 Project:  FRI

  This material is the exclusive property of KUKA Roboter GmbH 
  and must be returned to KUKA Roboter GmbH immediately upon 
  request.  This material and the information illustrated or 
  contained herein may not be used, reproduced, stored in a 
  retrieval system, or transmitted in whole or in part in any 
  way - electronic, mechanical, photocopying, recording, or 
  otherwise, without the prior written consent of KUKA Roboter GmbH.  
  
                 All Rights Reserved
                 Copyright (C)  2009
                  KUKA Roboter GmbH
                  Augsburg, Germany
  
[PH]}}
*/

/*
{{[FH]
****************************************************************************
  FRISimulink.cpp
       
      NOTE: This sample, as the corresponding FRI (Fast Research inteface) is subject to radical change
	  
      
[FH]}}
*/ 
/** *************************************************************************
 \author (Jonas Ruemping)
\file
\brief Interaction to Simulink (legacyCoder)
 *                                                                         *
 ***************************************************************************/

#ifndef _FRI_SIMULINK_H
#define _FRI_SIMULINK_H


#ifdef __cplusplus
extern "C" {
#endif

#include <friComm.h>

/** 	Initializes FRI Connection between Simulink and FRI
	def.StartFcnSpec = ['void friSimulinkInit(int32 p1,int32 work1[1])']
*/

void friSimulinkInit(int p1,int work1[1]);
/** Send - Receive
The Work Routine, which does simple position control w.r.t the fri interface
def.OutputFcnSpec = ['void friSimulinkSendRecv(int32  work1[1], float u1[7], float y1[7], float u2[16], int32 u3[16], int16 u4, float y2[16], int32 y3[16], int16 y4[1])'   ] ;
\note: Fixme - enhance the interface for a lot bunch of variables, which can be exchanged
*/

void friSimulinkMonitor(int  work1[1], tFriCmdData *u1, tFriMsrData *y1, float y2[7]);

/**  Shutdown routine - termination of the simulink model
def.TerminateFcnSpec =   ['void friSimulinkClose(int32 work1[1])']
*/

void friSimulinkClose(int work1[1]);
#ifdef __cplusplus
}
#endif

#endif 