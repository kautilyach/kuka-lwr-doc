#ifndef FRICOMM_H
#define FRICOMM_H

typedef unsigned short uint16; /* 2 byte unsigned */
typedef unsigned int   uint32; /* 4 byte unsigned */
typedef int            int32;  /* 4 byte signed */
typedef float          single; /* 4 byte IEEE 754 float */

typedef struct {
    uint16 sendSeqCount;
    uint16 reflSeqCount;
    uint16 packetSize;
    uint16 datagramId;
} tFriHeader;

typedef struct {
    single realData[16];
    int32 intData[16];
    uint16 boolData;
    uint16 fill;
} tFriKrlData;

typedef struct {
    single answerRate;
    single latency;
    single jitter;
    single missRate;
    uint32 missCounter;
} tFriIntfStatistics;

typedef struct {
    single timestamp;
    uint16 state;
    uint16 quality;
    single desiredMsrSampleTime;
    single desiredCmdSampleTime;
    single safetyLimits;
    tFriIntfStatistics stat;
} tFriIntfState;

typedef struct {
    uint16 power;
    uint16 control;
    uint16 error;
    uint16 warning;
    single temperature[7];
} tFriRobotState;

typedef struct {
    single msrJntPos[7];
    single msrCartPos[12];
    single cmdJntPos[7];
    single cmdJntPosFriOffset[7];
    single cmdCartPos[12];
    single cmdCartPosFriOffset[12];
    single msrJntTrq[7];
    single estExtJntTrq[7];
    single estExtTcpFT[6];
    single jacobian[6*7];
    single massMatrix[7*7];
    single gravity[7];
} tFriRobotData;

typedef struct {
    uint32 cmdFlags;
    single jntPos[7];
    single cartPos[12];
    single addJntTrq[7];
    single addTcpFT[6];
    single jntStiffness[7];
    single jntDamping[7];
    single cartStiffness[6];
    single cartDamping[6];
} tFriRobotCommand;

#endif /* _FRICOMM_H */

#ifndef _BUS_H_
#define _BUS_H_

typedef struct {
    tFriHeader head;
    tFriKrlData krl;
    tFriIntfState intf;
    tFriRobotState robot;
    tFriRobotData data;
} msr;

typedef struct {
    tFriHeader head;
    tFriKrlData krl;
    tFriRobotCommand cmd;
} cmd;

#endif /* _BUS_H_ */