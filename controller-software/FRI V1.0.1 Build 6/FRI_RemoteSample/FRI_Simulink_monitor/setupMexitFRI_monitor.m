

myFilePathInfo=mfilename('fullpath');


disp(myFilePathInfo);
removePathInfo=strcat('FRI_Simulink_monitor\',mfilename);
disp(removePathInfo);
myPathInfo1=myFilePathInfo(1:end-length(removePathInfo));
disp(myPathInfo1);

%%myPathInfo2=myFilePathInfo(1:end-1);



SRLBRLibIncPaths={strcat(myPathInfo1,'FRI_Simulink_monitor'), strcat(myPathInfo1,'FRI_Remote')};
SRLBRLibSrcPaths=SRLBRLibIncPaths;

disp(SRLBRLibIncPaths);

FRISources={'FRISimulink_monitor.cpp','friRemote.cpp','friUdp.cpp'};
FRIHeaders={'FRISimulink_monitor.h','friRemote.h','friUdp.h'};


evalin('base','load bus.mat')

defs = [];


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FRI Kin Bereich

def = legacy_code('initialize');
def.SFunctionName = 'FRIMonitor';
def.StartFcnSpec = 'void friSimulinkInit(int32 p1,int32 work1[1])';
def.OutputFcnSpec = 'void friSimulinkMonitor(int32  work1[1], cmd u1[1], msr y1[1], single y2[7])';
def.TerminateFcnSpec = 'void friSimulinkClose(int32 work1[1])';

def.HeaderFiles   = {'FRISimulink_monitor.h'};
def.SourceFiles   = FRISources;
def.IncPaths      = SRLBRLibIncPaths;  
def.SrcPaths      = SRLBRLibSrcPaths

defs = [defs; def];


legacy_code('generate_for_sim', defs);
legacy_code('slblock_generate', defs);
%
%
legacy_code('rtwmakecfg_generate', def);

%
%
