
/*{{[PH]
****************************************************************************
 Project:  FRI

  This material is the exclusive property of KUKA Roboter GmbH 
  and must be returned to KUKA Roboter GmbH immediately upon 
  request.  This material and the information illustrated or 
  contained herein may not be used, reproduced, stored in a 
  retrieval system, or transmitted in whole or in part in any 
  way - electronic, mechanical, photocopying, recording, or 
  otherwise, without the prior written consent of KUKA Roboter GmbH.  
  
                 All Rights Reserved
                 Copyright (C)  2009
                  KUKA Roboter GmbH
                  Augsburg, Germany
  
[PH]}}
*/

/*
{{[FH]
****************************************************************************
  FRISimulink.cpp
       
      NOTE: This sample, as the corresponding FRI (Fast Research inteface) is subject to radical change
	  
      
[FH]}}
*/ 
/** *************************************************************************
 \author (Jonas Ruemping)
\file
\brief Interaction to Simulink (legacyCoder)
 *                                                                         *
 ***************************************************************************/



#include <FRISimulink_monitor.h>
#include <friremote.h>
#include "mex.h"



/** def.StartFcnSpec = ['void friSimulinkInit(int32 p1,int32 work1[1])']
	Initializes FRI Connection between Simulink and FRI
*/

void friSimulinkInit(int p1,int work1[1])
{
	mexPrintf("friSimulinkInit(%d) -- initializing\n", p1);

	 friRemote * friInst = new friRemote(p1);
	 work1[0]= (int) friInst;
}

/*
def.OutputFcnSpec = ['void friSimulinkMonitor(int32  work1[1], cmd u1[1], msr y1[1])', float y2[7]] ;
*/

void friSimulinkMonitor(int  work1[1], tFriCmdData *u1, tFriMsrData *y1, float y2[7])
{
	friRemote *friInst = (friRemote *) work1[0];

	if ( friInst )
	{	
		// output data
		*y1 = friInst->getMsrBuf();
        friInst->getCurrentCmdJntPosition(y2);
		
		// input data
		for ( int i = 0; i < FRI_USER_SIZE; i++)
		{
			friInst->setToKRLReal(i,u1->krl.realData[i]);
			friInst->setToKRLInt(i,u1->krl.intData[i]);
		}
		friInst->setToKRLBool(u1->krl.boolData);
        // Wenn irgendwo anders kommandiert wird, darf hier nicht Exchange aufgerufen werden !!
        friInst->doDataExchange();
	}
}

/*
def.TerminateFcnSpec =   ['void friSimulinkClose(int32 work1[1])']
*/

void friSimulinkClose(int work1[1])
{
	friRemote * friInst = (friRemote *) work1[0];

	if ( friInst )
	{	
		mexPrintf("friSimulinkClose() -- destroying \n");

		delete friInst;
	}
}