Setup for KUKA technology package
===================================
1. Insert the setup CD
2. Press CTRL+ESC, select "run".
3. Type in the dialog box "E:\setup". Press ENTER.
4. After the corresponding message has appeared restart the computer.
5. The setup has finished after the reboot.


Setup f�r KUKA technologie Paket
=================================================
1. Installations-CD in das CD-Laufwerk des Steuerungs-PC 
   einlegen.
2. Mit der Tastenkombination Ctrl+Esc und den Cursortasten den 
   Menuepunkt  "Ausf�hren" des Startmenues anw�hlen.
3. "E:\Setup" eingeben und Eingabetaste dr�cken.
4. Nach der entsprechenden Meldung Steuerung ausschalten,
   und wieder einschalten.
5. Mit dem Hochfahren der Benutzeroberfl�che ist das Setup beendet.

