Security Update for Windows XP embedded V2.x V3.x

17.01.2011 XPe V2.2 3.x
Vulnerabilities in Microsoft Data Access Components Could Allow Remote Code Execution (2419632)

17.01.2011 XPe V2.2 3.x
New version of MSRemovalTool.exe 3.15.4500.0

-------------------------------------------------------------------------------------

14.12.2010 XPe V2.2 3.x
Cumulative Security Update for Internet Explorer (2416400)

14.12.2010 XPe V2.2 3.x
Vulnerabilities in the OpenType Font (OTF) Driver Could Allow Remote Code Execution (2296199)

14.12.2010 XPe V2.2 3.x
Vulnerability in Windows Address Book Could Allow Remote Code Execution (2423089)

14.12.2010 XPe V2.2 3.x
Insecure Library Loading in Internet Connection Signup Wizard Could Allow Remote Code Execution (2443105)

14.12.2010 XPe V2.2 3.x
Vulnerabilities in Windows Kernel-Mode Drivers Could Allow Elevation of Privilege (2436673)

14.12.2010 XPe V2.2 3.x
Vulnerability in Routing and Remote Access Could Allow Elevation of Privilege (2440591)

14.12.2010 XPe V2.2 3.x
New version of MSRemovalTool.exe

-------------------------------------------------------------------------------------

18.10.2010 XPe V2.2 3.x
Cumulative Security Update for Internet Explorer (2360131)

18.10.2010 XPe V2.2 3.x
Vulnerabilities in Windows Kernel-Mode Drivers Could Allow Elevation of Privilege (981957)

18.10.2010 XPe V2.2 3.x
Vulnerability in Microsoft Foundation Classes Could Allow Remote Code Execution (2387149)

18.10.2010 XPe V2.2 3.x
Vulnerability in the Embedded OpenType Font Engine Could Allow Remote Code Execution (982132)

18.10.2010 XPe V2.2 3.x
Vulnerabilities in the OpenType Font (OTF) Format Driver Could Allow Elevation of Privilege (2279986)

18.10.2010 XPe V2.2 3.x
Vulnerability in Windows Common Control Library Could Allow Remote Code Execution (2296011)

18.10.2010 XPe V2.2 3.x
Vulnerability in COM Validation in Windows Shell and WordPad Could Allow Remote Code Execution (2405882)

18.10.2010 XPe V2.2 3.x
Vulnerability in Windows Local Procedure Call Could Cause Elevation of Privilege (2360937)

18.10.2010 XPe V2.2 3.x
New version of MSRemovalTool.exe

-------------------------------------------------------------------------------------

14.09.2010 XPe V2.2 3.x 
Vulnerability in Windows Client/Server Runtime Subsystem Could Allow Elevation of Privilege (2121546)

14.09.2010 XPe V2.2 3.x 
Vulnerability in WordPad Text Converters Could Allow Remote Code Execution (2259922)

14.09.2010 XPe V2.2 3.x 
Vulnerability in Remote Procedure Call Could Allow Remote Code Execution (982802)

14.09.2010 XPe V2.2
Vulnerabilities in Microsoft Internet Information Services (IIS) Could Allow Remote Code Execution (2267960)

14.09.2010 XPe V2.2 3.x 
Vulnerability in Unicode Scripts Processor Could Allow Remote Code Execution (2320113)

14.09.2010 XPe V2.2 3.x 
Vulnerability in Print Spooler Service Could Allow Remote Code Execution (2347290)

-------------------------------------------------------------------------------------

10.08.2010 XPe 3.x
Vulnerabilities in the Microsoft .NET Common Language Runtime and in Microsoft Silverlight 
Could Allow Remote Code Execution (2265906)

10.08.2010 XPe 2.2 3.x
Vulnerabilities in SMB Server Could Allow Remote Code Execution (982214)

10.08.2010 XPe 2.2 3.x
Cumulative Security Update for Internet Explorer (2183461)

10.08.2010 XPe 2.2 3.x
Vulnerability in Microsoft XML Core Services Could Allow Remote Code Execution (2079403)

10.08.2010 XPe 2.2 3.x
Vulnerabilities in SChannel Could Allow Remote Code Execution (980436)

10.08.2010 XPe 2.2 3.x
Vulnerabilities in Windows Kernel-Mode Drivers Could Allow Elevation of Privilege (2160329)

10.08.2010 XPe 2.2 3.x
Vulnerabilities in Windows Kernel Could Allow Elevation of Privilege (981852)

02.08.2010 XPe 2.2 3.x
Vulnerability in Windows Shell Could Allow Remote Code Execution (2286198)

-------------------------------------------------------------------------------------

23.06.2010 XPe V2.x 3.x
Vulnerabilities in Windows Kernel-Mode Drivers Could Allow Elevation of Privilege (979559)

23.06.2010 XPe V2.x 3.x
Vulnerabilities in Media Decompression Could Allow Remote Code Execution (KB979482)

23.06.2010 XPe V2.x 3.x
Cumulative Security Update of ActiveX Kill Bits (980195)

23.06.2010 XPe V2.x 3.x
Cumulative Security Update for Internet Explorer (982381)

23.06.2010 XPe V2.x 3.x
Vulnerability in the OpenType Compact Font Format (CFF) Driver Could Allow Elevation of Privilege (980218)

23.06.2010 XPe 3.x
Vulnerability in Microsoft .NET Framework Could Allow Tampering (KB979909)

-------------------------------------------------------------------------------------

21.04.2010 XPe V2.x 3.x
Cumulative Security Update for Internet Explorer (KB980182)

21.04.2010 XPe V2.x 3.x
Vulnerabilities in Windows Could Allow Remote Code Execution (KB978601)

21.04.2010 XPe V2.x 3.x
Vulnerabilities in Windows Could Allow Remote Code Execution (KB979309)

21.04.2010 XPe V2.x 3.x
Vulnerabilities in SMB Client Could Allow Remote Code Execution (KB980232)

21.04.2010 XPe V2.x 3.x
Vulnerabilities in Windows Kernel Could Allow Elevation of Privilege (KB979683)

21.04.2010 XPe V2.x 3.x
Vulnerability in VBScript Could Allow Remote Code Execution (KB981349)

21.04.2010 XPe V2.x 3.x
Vulnerability in VBScript Could Allow Remote Code Execution (KB981350)

21.04.2010 XPe V2.x 3.x
Vulnerabilities in Windows ISATAP Component Could Allow Spoofing (KB978338)

-------------------------------------------------------------------------------------

09.02.2010 XPe V2.x V3.x
Vulnerabilities in Windows Kernel Could Allow Elevation of Privilege (977165)

09.02.2010 XPe V2.x V3.x
Vulnerability in Microsoft DirectShow Could Allow Remote Code Execution (977935)

09.02.2010 XPe V2.x V3.x
Vulnerabilities in SMB Server Could Allow Remote Code Execution (971468)

09.02.2010 XPe V2.x V3.x
Vulnerability in Windows Client/Server Run-time Subsystem Could Allow Elevation of Privilege (978037)

09.02.2010 XPe V2.x V3.x
Cumulative Security Update of ActiveX Kill Bits (978262)

09.02.2010 XPe V2.x V3.x
Vulnerability in Windows Shell Handler Could Allow Remote Code Execution (975713)

09.02.2010 XPe V2.x V3.x
Vulnerabilities in SMB Client Could Allow Remote Code Execution (978251)

09.02.2010 XPe V2.x V3.x
Vulnerability in Microsoft Paint Could Allow Remote Code Execution (978706)

-------------------------------------------------------------------------------------

21.01.2010 XPe V2.x V3.x
Cumulative Security Update for Internet Explorer (978207)

-------------------------------------------------------------------------------------

12.01.2010 XPe V2.x V3.x
Vulnerability in the Embedded OpenType Font Engine Could Allow Remote Code Execution (972270)

-------------------------------------------------------------------------------------

08.12.2009 XPe V2.x V3.x
Vulnerability in Local Security Authority Subsystem Service Could Allow Denial of Service (974392)

08.12.2009 XPe V2.x V3.x
Vulnerability in WordPad and Office Text Converters Could Allow Remote Code Execution (975539)

-------------------------------------------------------------------------------------

10.11.2009 XPe V2.x V3.x
Vulnerabilities in Windows Kernel-Mode Drivers Could Allow Remote Code Execution (969947)

-------------------------------------------------------------------------------------

13.10.2009 XPe V2.x V3.x
Vulnerabilities in GDI+ Could Allow Remote Code Execution (957488)

13.10.2009 XPe V2.x
Vulnerabilities in FTP Service for Internet Information Services Could Allow Remote Code Execution (975254)
 
13.10.2009 XPe V2.x V3.x
Vulnerabilities in Windows CryptoAPI Could Allow Spoofing (974571)

13.10.2009 XPe V2.x V3.x
Vulnerability in Indexing Service Could Allow Remote Code Execution (969059)
 
13.09.2009 XPe V2.x V3.x
Vulnerability in JScript Scripting Engine Could Allow Remote Code Execution (971961)

13.09.2009 XPe V2.x V3.x
Vulnerability in DHTML Editing Component ActiveX Control Could Allow Remote Code Execution (956844)

-------------------------------------------------------------------------------------

12.08.2009 XPe V2.x
Vulnerability in Message Queuing Could Allow Elevation of Privilege (971032)

12.08.2009 XPe V2.x V3.x
Vulnerability in Workstation Service Could Allow Elevation of Privilege (971657)

12.08.2009 XPe V2.x V3.x
Vulnerabilities in Microsoft Active Template Library (ATL) Could Allow Remote Code Execution (973908)

12.08.2009 XPe V2.x V3.x
Vulnerabilities in Remote Desktop Connection Could Allow Remote Code Execution (970927)

12.08.2009 XPe V3.x
Vulnerabilities in Microsoft DirectShow Could Allow Remote Code Execution (971633)

-------------------------------------------------------------------------------------

09.06.2009 XPe V2.x V3.x
Vulnerabilities in Windows Print Spooler Could Allow Remote Code Execution (961501)

09.06.2009 XPe V2.x V3.x
Vulnerabilities in Windows Kernel Could Allow Elevation of Privilege (968537)

09.06.2009 XPe V2.x V3.x
Vulnerability in RPC Could Allow Elevation of Privilege (970238)

-------------------------------------------------------------------------------------

14.04.2009 XPe V2.x V3.x
Blended Threat Vulnerability in SearchPath Could Allow Elevation of Privilege (959426)

14.04.2009 XPe V2.x V3.x
Vulnerabilities in Windows HTTP Services Could Allow Remote Code Execution (960803)

14.04.2009 XPe V2.x V3.x
Vulnerabilities in Windows Could Allow Elevation of Privilege (959454)

14.04.2009 XPe V2.x V3.x
Vulnerabilities in WordPad and Office Text Converters Could Allow Remote Code Execution (960477)

-------------------------------------------------------------------------------------

24.02.2009 XPe V2.x V3.x
How to correct "disable Autorun registry key" enforcement in Windows (967115)

-------------------------------------------------------------------------------------

10.03.2009 MS09-007 XPe V2.x V3.x
Vulnerability in SChannel Could Allow Spoofing (960225)

-------------------------------------------------------------------------------------

09.12.2008 MS08-073  XPe V2.x V3.x
Cumulative Security Update for Internet Explorer (958215)

09.12.2008 MS08-071  XPe V2.x V3.x
Vulnerabilities in GDI Could Allow Remote Code Execution (956802)

11.11.2008 MS08-069  XPe V2.x V3.x
Vulnerabilities in Microsoft XML Core Services Could Allow Remote Code Execution (955218)

23.10.2008 MS08-067  XPe V2.x V3.x
Vulnerability in Server Service Could Allow Remote Code Execution (958644)

14.10.2008 MS08-066  XPe V2.x V3.x
Vulnerability in the Microsoft Ancillary Function Driver Could Allow Elevation of Privilege (956803)

14.10.2008 MS08-063  XPe V2.x V3.x
Vulnerability in SMB Could Allow Remote Code Execution (957095)

14.10.2008 MS08-062  XPe V2.x V3.x
Vulnerability in Windows Internet Printing Service Could Allow Remote Code Execution (953155)

14.10.2008 MS08-058  XPe V2.x V3.x
Cumulative Security Update for Internet Explorer (956390)

13.08.2008 MS08-049  XPe V2.x V3.x
Vulnerabilities in Event System Could Allow Remote Code Execution (950974)

13.08.2008 MS08-046  XPe V2.x V3.x
Vulnerability in Microsoft Windows Image Color Management System Could Allow Remote Code Execution (952954)

13.08.2008 MS08-045  XPe V2.x V3.x
Cumulative Security Update for Internet Explorer (953838)

08.07.2008 MS08-037  XPe V2.x V3.x
Vulnerabilities in DNS Could Allow Spoofing (953230 --> 951748)

10.06.2008 MS08-036  XPe V2.x
Vulnerabilities in Pragmatic General Multicast (PGM) Could Allow Denial of Service (950762)

10.06.2008 MS08-031  XPe V2.x
Cumulative Security Update for Internet Explorer (950759)

13.05.2008 MS08-028  XPe V2.x
Vulnerability in Microsoft Jet Database Engine Could Allow Remote Code Execution (950749)

08.04.2008 MS08-025  XPe V2.x
Vulnerability in Windows Kernel Could Allow Elevation of Privilege (941693)

08.04.2008 MS08-024  XPe V2.x
Cumulative Security Update for Internet Explorer (947864)

08.04.2008 MS08-022  XPe V2.x
Vulnerability in VBScript and JScript Scripting Engines Could Allow Remote Code Execution (944338)

08.04.2008 MS08-021  XPe V2.x
Vulnerabilities in GDI Could Allow Remote Code Execution (948590)

08.04.2008 MS08-020  XPe V2.x
Vulnerability in DNS Client Could Allow Spoofing (945553)

12.02.2008 MS08-010  XPe V2.x
Cumulative Security Update for Internet Explorer (944533)

12.02.2008 MS08-008  XPe V2.x
Vulnerability in OLE Automation Could Allow Remote Code Execution (947890)

12.02.2008 MS08-007  XPe V2.x
Vulnerability in WebDAV Mini-Redirector Could Allow Remote Code Execution (946026)

12.02.2008 MS08-006  XPe V2.x
Vulnerability in Internet Information Services Could Allow Remote Code Execution (942830)

12.02.2008 MS08-005  XPe V2.x
Vulnerability in Internet Information Services Could Allow Elevation of Privilege (942831)

08.01.2008 MS08-002  XPe V2.x
Vulnerability in LSASS Could Allow Local Elevation of Privilege (943485)

08.01.2008 MS08-001  XPe V2.x
Vulnerabilities in Windows TCP/IP Could Allow Remote Code Execution (941644)

11.12.2007 MS07-069  XPe V2.x
Cumulative Security Update for Internet Explorer (942615)

13.11.2007 MS07-061  XPe V2.x
Vulnerability in Windows URI Handling Could Allow Remote Code Execution (943460)

09.10.2007 MS07-057  XPe V2.x
Cumulative Security Update for Internet Explorer (939653)

14.08.2007 MS07-050  XPe V2.x
Vulnerability in Vector Markup Language Could Allow Remote Code Execution (938127)

14.08.2007 MS07-046  XPe V2.x
Vulnerability in GDI Could Allow Remote Code Execution (938829)

14.08.2007 MS07-045  XPe V2.x
Cumulative Security Update for Internet Explorer (937143)

14.08.2007 MS07-043  XPe V2.x
Vulnerability in OLE Automation Could Allow Remote Code Execution (921503)

14.08.2007 MS07-042  XPe V2.x
Vulnerability in Microsoft XML Core Services Could Allow Remote Code Execution (936227 -- > 936021)

10.07.2007 MS07-041  XPe V2.x
Vulnerability in Microsoft Internet Information Services Could Allow Remote Code Execution (939373)

10.07.2007 MS07-040  XPe V2.x
Vulnerabilities in .NET Framework Could Allow Remote Code Execution (931212)

12.06.2007 MS07-035  XPe V2.x
Vulnerability in Win 32 API Could Allow Remote Code Execution (935839)
Replaced by 959426

12.06.2007 MS07-033  XPe V2.x
Cumulative Security Update for Internet Explorer (933566)

08.05.2007 MS07-027  XPe V2.x
Cumulative Security Update for Internet Explorer (931768)

11.04.2007 MS07-022  XPe V2.x
Vulnerability in Windows Kernel Could Allow Elevation of Privilege (931784)

11.04.2007 MS07-021  XPe V2.x
Vulnerabilities in CSRSS Could Allow Remote Code Execution (930178)

11.04.2007 MS07-020  XPe V2.x
Vulnerability in Microsoft Agent Could Allow Remote Code Execution (932168)

03.04.2007 MS07-017  XPe V2.x
Vulnerabilities in GDI Could Allow Remote Code Execution (925902)

14.02.2007 MS07-016  XPe V2.x
Cumulative Security Update for Internet Explorer (928090)

14.02.2007 MS07-013  XPe V2.x
Vulnerability in Microsoft RichEdit Could Allow Remote Code Execution (918118)

14.02.2007 MS07-012  XPe V2.x
Vulnerability in Microsoft MFC Could Allow Remote Code Execution (924667)

14.02.2007 MS07-011  XPe V2.x
Vulnerability in Microsoft OLE Dialog Could Allow Remote Code Execution (926436)

14.02.2007 MS07-009  XPe V2.x
Vulnerability in Microsoft Data Access Components Could Allow Remote Code Execution (927779)

14.02.2007 MS07-008  XPe V2.x
Vulnerability in HTML Help ActiveX Control Could Allow Remote Code Execution (928843)

14.02.2007 MS07-006  XPe V2.x
Vulnerability in Windows Shell Could Allow Elevation of Privilege (928255)

12.12.2006 MS06-75  XPe V2.x
Vulnerability in Windows Could Allow Elevation of Privilege (926255)

12.12.2006 MS06-74  XPe V2.x
Vulnerability in SNMP Could Allow Remote Code Execution (926247)

12.12.2006 MS06-72  XPe V2.x
Cumulative Security Update for Internet Explorer (925454)

14.11.2006 MS06-70  XPe V2.x
Vulnerability in Workstation Service Could Allow Remote Code Execution (924270)

14.11.2006 MS06-67  XPe V2.x
Cumulative Security Update for Internet Explorer (922760)

14.11.2006 MS06-66  XPe V2.x
Vulnerabilities in Client Service for NetWare Could Allow Remote Code Execution (923980)

10.10.2006 MS06-65  XPe V1.1, XPe V2.x
Vulnerability in Windows Object Packager Could Allow Remote Execution (924496)

10.10.2006 MS06-64  XPe V1.1, XPe V2.x
Vulnerabilities in TCP/IP IPv6 Could Allow Denial of Service (922819)

10.10.2006 MS06-63  XPe V1.1, XPe V2.x
Vulnerability in Server Service Could Allow Denial of Service (923414)

10.10.2006 MS06-61 XPe V1.1, XPe V2.x
Vulnerabilities in Microsoft XML Core Services Could Allow Remote Code Execution (924191) 

10.10.2006 MS06-57  XPe V1.1, XPe V2.x
Vulnerability in Windows Explorer Could Allow Remote Execution (923191)

10.10.2006 MS06-56   XPe V2.x         
Vulnerability in ASP.NET 2.0 Could Allow Information Disclosure (922770)

13.09.2006 MS06-52  XPe V1.1, XPe V2.x
Vulnerability in Pragmatic General Multicast (PGM) Could Allow Remote Code Execution (919007)

09.08.2006 MS06-051  XPe V1.1, XPe V2.x
Vulnerability in Windows Kernel Could Result in Remote Code Execution (917422)

09.08.2006 MS06-050  XPe V1.1, XPe V2.x
Vulnerabilities in Microsoft Windows Hyperlink Object Library Could Allow Remote Code Execution (920670)

09.08.2006 MS06-046  XPe V1.1, XPe V2.x
Vulnerability in HTML Help Could Allow Remote Code Execution (922616)

09.08.2006 MS06-045  XPe V1.1, XPe V2.x
Vulnerability in Windows Explorer Could Allow Remote Code Execution (921398)

09.08.2006 MS06-042  XPe V1.1, XPe V2.x
Cumulative Security Update for Internet Explorer (918899)

09.08.2006 MS06-041  XPe V1.1, XPe V2.x
Vulnerabilities in DNS Resolution Could Allow Remote Code Execution (920683)

09.08.2006 MS06-040  XPe V1.1, XPe V2.x
Vulnerability in Server Service Could Allow Remote Code Execution (921883)

13.07.2006 MS06-036  XPe V1.1, XPe V2.x
Vulnerability in DHCP Client Service Could Allow Remote Code Execution (914388)

13.07.2006 MS06-035  XPe V1.1, XPe V2.x
Vulnerability in Server Service Could Allow Remote Code Execution (917159)

13.07.2006 MS06-033  XPe V2.x
Vulnerability in ASP.NET Could Allow Information Disclosure (917283)

13.06.2006 MS06-032  XPe V1.1
Vulnerability in TCP/IP Could Allow Remote Code Execution (917953)

13.06.2006 MS06-030  XPe V1.1
Vulnerability in Server Message Block Could Allow Elevation of Privilege (914389)

13.06.2006 MS06-025  XPe V1.1
Vulnerability in Routing and Remote Access Could Allow Remote Code Execution (911280)

13.06.2006 MS06-024  XPe V1.1
Vulnerability in Windows Media Player Could Allow Remote Code Execution (917734)

13.06.2006 MS06-022  XPe V1.1
Vulnerability in ART Image Rendering Could Allow Remote Code Execution (918439)

13.06.2006 MS06-021  XPe V1.1
Cumulative Security Update for Internet Explorer (916281)

09.05.2006 MS06-018  XPe V1.1
Vulnerability in Microsoft Distributed Transaction Coordinator Could Allow Denial of Service (913580)

11.04.2006 MS06-015  XPe V1.1
Vulnerability in Windows Explorer Could Allow Remote Code Execution (908531)

11.04.2006 MS06-014  XPe V1.1
Vulnerability in the Microsoft Data Access Components (MDAC) Function Could Allow Code Execution (911562)

11.04.2006 MS06-013  XPe V1.1
Cumulative Security Update for Internet Explorer (912812)

14.02.2006 MS06-008  XPe V1.1
Vulnerability in Web Client Service Could Allow Remote Code Execution (911927)

14.02.2006 MS06-007  XPe V1.1
Vulnerability in TCP/IP Could Allow Denial of Service (913446)

05.01.2006 MS06-001  XPe V1.1
Vulnerability in Graphics Rendering Engine Could Allow Remote Code Execution (912919)

11.10.2005 MS05-051  XPe V1.1
Vulnerabilities in MSDTC and COM+ Could Allow Remote Code Execution (902400)

11.10.2005 MS05-049  XPe V1.1
Vulnerabilities in Windows Shell Could Allow Remote Code Execution (900725)

11.10.2005 MS05-048  XPe V1.1
Vulnerability in the Microsoft Collaboration Data Objects Could Allow Remote Code Execution (907245)

11.10.2005 MS05-047  XPe V1.1
Vulnerability in Plug and Play Could Allow Remote Code Execution and Local Elevation of Privilege (905749)

11.10.2005 MS05-046  XPe V1.1
Vulnerability in the Client Service for NetWare Could Allow Remote Code Execution (899589)

11.10.2005 MS05-045  XPe V1.1
Vulnerability in Network Connection Manager Could Allow Denial of Service (905414)

11.10.2005 MS05-044  XPe V1.1
Vulnerability in the Windows FTP Client Could Allow File Transfer Location Tampering (905495)

17.08.2005 MS05-043  XPe V1.1
Vulnerability in Print Spooler Service Could Allow Remote Code Execution (896423)

09.08.2005 MS05-042  XPe V1.1
Vulnerabilities in Kerberos Could Allow Denial of Service, Information Disclosure, and Spoofing (899587)		

09.08.2005 MS05-041  XPe V1.1
Vulnerability in Remote Desktop Protocol Could Allow Denial of Service (899591)

17.08.2005 MS05-040  XPe V1.1
Vulnerability in Telephony Service Could Allow Remote Code Execution (893756)

20.07.2005 MS05-037  XPe V1.1
Vulnerability in JView Profiler Could Allow Remote Code Execution (903235)

20.07.2005 MS05-036  XPe V1.1
Vulnerability in Microsoft Color Management Module Could Allow Remote Code Execution (901214)

17.06.2005 MS05-033   XPe V1.1
Vulnerability in Telnet Client Could Allow Information Disclosure (896428)

17.06.2005 MS05-027   XPe V1.1
Vulnerability in Server Message Block Could Allow Remote Code Execution (896422)

17.06.2005 MS05-026   XPe V1.1
Vulnerability in HTML Help Could Allow Remote Code Execution (896358)

12.04.2005 MS05-018  XPe V1.1
Vulnerabilities in Windows Kernel Could Allow Elevation of Privilege and Denial of Service (890859)	

12.04.2005 MS05-017  XPe V1.1
Vulnerability in Message Queuing Could Allow Code Execution (892944)

08.02.2005 MS05-015  XPe V1.1
Vulnerability in Hyperlink Object Library Could Allow Remote Code Execution (888113)

08.02.2005 MS05-013  XPe V1.1
Vulnerability in the DHTML Editing Component ActiveX Control Could Allow Remote Code Execution (891781)

08.02.2005 MS05-011  XPe V1.1
Vulnerability in Server Message Block Could Allow Remote Code Execution (885250)

08.02.2005 MS05-007  XPe V1.1
Vulnerability in Windows Could Allow Information Disclosure (888302)

11.01.2005 MS05-003  XPe V1.1
Vulnerability in the Indexing Service Could Allow Remote Code Execution (871250)

11.01.2005 MS05-001  XPe V1.1
Vulnerability in HTML Help Could Allow Code Execution (890175)

14.12.2004 MS04-044  XPe V1.1 
Vulnerabilities in Windows Kernel and LSASS Could Allow Elevation of Privilege (885835)

14.12.2004 MS04-043  XPe V1.1 
Vulnerability in HyperTerminal Could Allow Code Execution (873339)

14.12.2004 MS04-041  XPe V1.1 
Vulnerability in WordPad Could Allow Code Execution (885836)

12.10.2004 MS04-037  XPe V1.1 
Vulnerability in Windows Shell Could Allow Remote Code Execution (841356) <- MS04-024

12.10.2004 MS04-034  XPe V1.1 
Vulnerability in Compressed (zipped) Folders Could Allow Remote Code Execution (873376)

12.10.2004 MS04-032  XPe V1.1 
Security Update for Microsoft Windows (840987) <- MS03-045

12.10.2004 MS04-031  XPe V1.1 
Vulnerability in NetDDE Could Allow Remote Code Execution (841533)

12.10.2004 MS04-030  XPe V1.1 
Vulnerability in WebDAV XML Message Handler Could Lead to a Denial of Service (824151)

13.07.2004 MS04-022  XPe V1.1 
Vulnerability in Task Scheduler Could Allow Code Execution (841873)

08.06.2004 MS04-016  XPe V1.1
Vulnerability in DirectPlay Could Allow Denial of Service (839643)

11.05.2004 MS04-015  XPe V1.1 
Vulnerability in Help and Support Center Could Allow Remote Code Execution (840374)

13.04.2004 MS04-014  XPe V1.1
Vulnerability in the Microsoft Jet Database Engine Could Allow Code Execution (837001)

13.04.2004 MS04-012  XPe V1.1
Cumulative Update for Microsoft RPC/DCOM (828741) <- MS03-026, MS03-039

13.04.2004 MS04-011  XPe V1.1
Security Update for Microsoft Windows (835732)       

13.01.2004 MS04-003  XPe V1.1 
Buffer Overrun in MDAC Function Could Allow Code Execution (832483) <- MS03-033

11.11.2003 MS03-051  XPe V1.1
Buffer Overrun in Microsoft FrontPage Server Extensions Could Allow Code Execution (813360)

15.10.2003 MS03-043 Maximum Severity Rating: Critical  XPe V1.1
Buffer Overrun in Messenger Service Could Allow Code Execution (828035)

15.10.2003 MS03-041 Maximum Severity Rating: Critical  XPe V1.1 
Vulnerability in Authenticode Verification Could Allow Remote Code Execution (823182)

23.07.2003 MS03-030  XPe V1.1
Unchecked Buffer in DirectX Could Enable System Compromise (819696)

09.07.2003 MS03-023  XPe V1.1
Buffer Overrun In HTML Converter Could Allow Code Execution (823559)

28.05.2003 MS03-018  XPe V1.1
Cumulative Patch for Internet Information Service (811114) 

07.05.2003 MS03-017  XPe V1.1
Flaw in Windows Media Player Skins Downloading could allow Code Execution (817787)

16.04.2003 MS03-013  XPe V1.1
Buffer Overrun in Windows Kernel Message Handling could Lead to Elevated Privileges (811493)

17.03.2003 MS03-007  XPe V1.1 
Unchecked Buffer in Windows Component Could Cause Server Compromise (815021)

18.12.2002 MS02-072  XPe V1.1
Unchecked Buffer in Windows Shell Could Enable System Compromise (329390) 

02.10.2002 MS02-055  XPe V1.1
Unchecked Buffer in Windows Help Facility Could Enable Code Execution (Q323255)

04.09.2002 MS02-050  XPe V1.1
Certificate Validation Flaw Could Enable Identity Spoofing (Q329115) 

27.02.2002 MS02-012  XPe V1.1
Malformed Data Transfer Request can Cause Windows SMTP Service to Fail

-------------------------------------------------------------------------------------

2. Firewall XPe V1.1 / V2.x

The integrated firewall (ICF) will be activated on all network connections except of 
the Shared Memory network connection. That means all incoming traffic will be blocked 
while outgoing traffic can pass through. Therefore some applications (e.g. Remote 
Desktop, Web Diagnosis) will not work.

To guarantee the functionality of nedded remote applications, it is possible to open 
the involved ports for communication. E.g. to use the Web Diagnosis you have to open 
port 80 (HTTP).

-------------------------------------------------------------------------------------

3. Disable Email

Outlook Express is no longer available. Even though this application was never 
configured and used by the KRC, it is now deleted and can not be started any more. 
Additionally, the functionality "Send to -> Mail Recipient" when right-clicking a 
file in the Windows Explorer is removed, too.

-------------------------------------------------------------------------------------

4. Rename

Some programs on the computer can be abused by hacking tools to manipulate the system. 
Anyway, these programs are still used by the local user. As a result, the following 
programs are renamed:

TFTP.exe   -> TFTPk.exe
FTP.exe    -> FTPk.exe
Telnet.exe -> Telnetk.exe

-------------------------------------------------------------------------------------

5. Services

Services provide functionality for local and remote purposes. Especially 
the latter ones are possible security vulnerabilities. Therefore not needed 
services should be disabled. The following list displays the used services 
with their new startup type:

							XPe V1.1		XPe V2.x		XPe V3.x

6to4							Disabled		Disabled		Disabled
Alerter							Disabled		Disabled		Disabled
Application Layer Gateway Service				Manual		Automatic	Manual
Application Management					Manual		Manual		Manual
ASP.NET State Service					Disabled		Disabled		Disabled
Background Intelligent Tranfer Service				Disabled		Disabled		Disabled
Client Service for NetWare					Disabled		Disabled		Disabled
Clipbook							Disabled		Disabled		Disabled
COM+ Event System					Manual		Manual		Manual
COM+ System Application					Manual		Manual		Manual
Computer Browser						Disabled		Disabled		Disabled
Cryptographic Services					Automatic	Automatic	Automatic
Device Update Agent					Automatic	Automatic	Automatic
DHCP Client						Automatic	Automatic	Automatic
DNS Client						Automatic	Automatic	Automatic
Error Reporting Service					Automatic	Automatic	Automatic
Event Log						Automatic	Automatic	Automatic
Fast User Switching Compatibility				Disabled		Disabled		Disabled
FTP Publishing						Automatic	Automatic	Automatic
Help and Support						Disabled		Disabled		Disabled
HID Input Device						Disabled		Disabled		Disabled
IIS Admin						Automatic	Automatic	Automatic
Indexing Service						Disabled		Disabled		Disabled
IP Network Address Translator				Manual		Automatic	Manual
IPSEC Services						Automatic	Automatic	Automatic
Logical Disk Manager					Manual		Manual		Manual
Logical Disk Manager Administrative Service			Manual		Manual		Manual
Message Queuing						Disabled		Disabled		Disabled
Message Queuing Triggers					Disabled		Disabled		Disabled
Messenger						Disabled		Disabled		Disabled
MS Software Shadow Copy Provider				Disabled		Disabled		Disabled
Netmeeting Remote Desktop Sharing				Disabled		Disabled		Disabled
Network Connections					Manual		Manual		Manual
Network DDE						Automatic	Automatic	Automatic
Network DDE DSDM					Automatic	Automatic	Automatic
Network Location Awareness (NLA)				Manual		Automatic	Manual
NT LM Security Support Provider				Manual		Manual		Manual
Performance Logs and Alerts				Disabled		Disabled		Manual
Plug and Play						Automatic	Automatic	Automatic
Portable Media Serial Number				Disabled		Disabled		Disabled
Print Spooler						Automatic	Automatic	Automatic
Protected Storage						Automatic	Automatic	Automatic
QoS RSVP						Manual		Manual		Manual
Remote Access Auto Connection Manager			Disabled		Disabled		Disabled
Remote Access Connection Manager				Manual		Manual		Manual
Remote Desktop Help Session Manager			Disabled		Disabled		Disabled
Remote Procedure Call (RPC)				Automatic	Automatic	Automatic
Remote Procedure Call (RPC) Locator				Manual		Manual		Manual
Remote Registry						Disabled		Disabled		Disabled
Removable Storage					Disabled		Disabled		Disabled
Routing and Remote Access					Disabled		Disabled		Disabled
Secondary Logon Service					Automatic	Automatic	Automatic
Security Accounts Manager					Automatic	Automatic	Automatic
Server							Automatic	Automatic	Automatic
Shell Hardware Detection					Automatic	Automatic	Automatic
Simple Mail Transfer Protocol				Disabled		Disabled		Disabled
Smart Card						Disabled		Disabled		Disabled
SNMP Service						Disabled		Disabled		Disabled
SNMP Trap Service					Disabled		Disabled		Disabled
SSDP Discovery Service					Disabled		Disabled		Disabled
Still Image Service						Disabled		Disabled		Disabled
System Restore Service					Disabled		Disabled		Disabled
Task Scheduler						Disabled		Automatic	Automatic
TCP/IP NetBIOS Helper					Automatic	Automatic	Automatic
Telephony						Manual		Manual		Manual
Terminal Services						Manual		Manual		Manual
Terminal Services Session Directory				Manual		Manual		Manual
Themes							Disabled		Disabled		Disabled
Universal Plug and Play Device Host				Disabled		Disabled		Disabled
Upload Manager						Automatic	Automatic	Automatic
Volume Shadow Copy					Disabled		Disabled		Disabled
WebClient						Disabled		Disabled		Disabled
Windows Audio						Disabled		Disabled		Disabled
Windows Firewall/Internet Connection Sharing			Automatic	Automatic	Disabled
Windows Installer						Manual		Manual		Manual
Windows Management Instrumentation			Automatic	Automatic	Automatic
Windows Management Instrumentation Driver Extensions		Manual		Manual		Manual
Windows Time						Automatic	Automatic	Automatic
Wireless Zero Configuration					Disabled		Disabled		Disabled
WMI Performance Adapter					Manual		Manual		Manual
Workstation						Automatic	Automatic	Automatic
World Wide Web Publishing					Automatic	Automatic	Automatic

-------------------------------------------------------------------------------------

6. Antivirus Measures

- The current removal tool for removing malicious software is run (silent mode, quick scan).

- The write access to the registry key HKLM\Software\Microsoft\Windows NT\CurrentVersion\SvcHost
  is denied (see KB 962007).
