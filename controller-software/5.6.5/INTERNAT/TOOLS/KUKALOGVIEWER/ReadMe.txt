
********************************************************************************
KUKA Robot Group, 2005
********************************************************************************

OVERVIEW:
=========

This tool allows you to view KUKA log messages, starting with KRC version 3.2.

The tool can access the old KukaLog.mdb file as well as the new event log 
based messages.
In order to start the tool, you need to have Kuka_tab.mdb file (mdb file 
containing the various language translations available in KRC software). 
The path to this file should be configured in KukaLogViewer.cfg under the 
"LanguageDB" key. You can get such a file from the KRC Software installation CD, 
under the path "INTERNAT\KRCSETUP\KRC\DATA" or from an already installed KRC 
Software, usually from the path "C:\Krc\Data".

If you have an archive containing .evt-files, you have to chose backup 
eventlogs when asked.

If you would like to open .evt-files located on a remote machine, make sure, 
that you have administrative rights AND the SERVICE "Remote Registry" 
is running on the target computer.


FILES:
======
	
	- KukaLogViewer.exe
	- KukaLogViewer.cfg
	- ReadMe.txt

These files have to located in the same directory!



KukaLogViewer.cfg:
==================

	;***** KukaLogViewer Configuration File *****
	;RULES: use only spaces

	Filter = 7943
	Language = English
	LanguageDB = C:\KRC\DATA\Kuka_tab.mdb
	ExportFile = C:\KRC\KukaLog.txt
	LastDBFile = pcuhr2\\"KrcLog", "KrcLogB", "KrcLogI", "KrcLogP", "KrcLogS", "KrcLogU"


Absolutely Necessary to run the tool is only the path to "Kuka_tab.mdb".
This can be done with we following line
   LanguageDB = X:\path\to\Kuka_tab.mdb


