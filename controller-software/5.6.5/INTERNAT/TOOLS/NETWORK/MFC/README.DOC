                 STANDARD MICROSYSTEMS CORPORATION

                              SuperDisk
                      For Ethernet LAN Adapters

   This software is licensed by SMC for use by its customers only.
        Copyright (c) 1997 Standard Microsystems Corporation.
                         All rights reserved.

                    Version 3.1       30 January 1997
               Compressed Driver File SMCG31.EXE


DISK INFORMATION

This SMC SuperDisk is designed to be used with SMC's EZStart
Program, which is on this SuperDisk. EZStart configures and tests
your adapter, copies the files you need from this disk to your PC,
and displays or prints appropriate instruction files that are located
on this disk.


SYSTEM REQUIREMENTS

To use EZStart, your system must meet the following minimum
requirements:

o  MS-DOS or PC-DOS operating system, version 3.11 or later, which
   is required to run the EZStart program

o  To take advantage of EZStart's graphics features, a VGA or better
   monitor; however, EZStart can operate in text mode on any monitor

o  A minimum amount of available hard disk space (typically 50 KB)
   for installing a single driver from the SuperDisk using EZStart

   OR

   At least 1.3 MB of available hard disk space if you intend to
   extract all of the drivers from the SuperDisk

Note: Refer to the "Installing A Network Driver" section later in
      this document or the User Manual for more information on
      copying driver files from the SuperDisk.


EZDOSODI - For Automatic NetWare Client Installation

With a single keystroke, EZDOSODI configures the EtherEZ adapter and
your computer for use as a DOS client in a NetWare server environment
by installing one of two SMC DOS ODI drivers:

o  If your adapter is configured for the default I/O Mapped
   Addressing Mode, EZDOSODI installs SMC's Turbo DOS ODI driver
   (an enhanced performance driver), SMC8416.COM. This driver can
   significantly improve your workstation's networking performance.

o  If your adapter is configured for Memory Mapped Addressing Mode,
   EZDOSODI installs SMC's dual-mode DOS ODI driver, SMC8000.COM.
   This driver works in either I/O Mapped or Memory Mapped Addressing
   Mode. It supports the EtherEZ adapter as well as SMC's EtherCard
   Elite16 Ultra adapter.

For more information about EZDOSODI, refer to the Quick and EZ Guide
provided with your adapter and to the DOS ODI driver installation
document in the DOS_ODI subdirectory (INSTALL.DOC).

NOTE: To install some other network driver, or if your environment
      requires greater customization, use EZStart, as described
      in the section titled, USING EZSTART.

Before running EZDOSODI:

   Exit Windows

   Install the adapter into an EISA or ISA bus slot

   Load other device drivers for your system, but do not load any
   other network drivers

   Cable the adapter to the network


To run EZDOSODI:

   Insert the SuperDisk in a drive, change to that drive, type
   EZDOSODI, and press Enter. (If you know are using 802.3 frame
   type, type EZDOSODI 802.3 and press Enter for a faster
   installation.)

EZDOSODI will automatically:

   Configure and test the adapter.

   Edit your AUTOEXEC.BAT file and save the old version as
   AUTOEXEC.00x.

   Copy the driver files to C:\DOS_ODI.

   Select an Ethernet frame type (802.2/802.3).

   Load the appropriate DOS ODI driver.

   Display a login prompt, allowing you to login to the
   nearest server.

If any step cannot be completed successfully, EZDOSODI exits to
EZStart (described below), where you can continue the installation.

NOTE: For those environments where Windows for Workgroups is
     installed and configured for networking capability in a Novell
     environment, EZDOSODI is not recommended. (EZDOSODI loads
     Novell ODI drivers and may conflict with Windows for Workgroups,
     which commonly uses NDIS drivers.) For those environments that
     run Windows for Workgroups and Novell concurrently, use the
     EZStart utility to manually configure and test the adapter and
     to install drivers.


USING EZStart

EZStart is an all-purpose utility program that lets you:

        Automatically configure and test the adapter

        Install the network driver
 
        Install and configure PC Agent/SNMP software, and view
        network management information it collects

In addition, EZStart makes it easy to perform optional installation
and diagnostic procedures:

        Manually configure the adapter by selecting a value for
        each configuration option on an individual basis

        Test two or more adapters communicating with each other
        over your actual network

        View the configurations of all installed adapters


LOADING EZStart

EZStart can run from the diskette, or for faster operation it can be
copied onto a hard disk and run from there.

Note: To run EZStart from the hard disk, copy all the files from
      the SuperDisk to your computer's hard disk.  However, make sure
      that the compressed driver file, SMCG31.EXE, and EZSTART.EXE
      reside in the same directory.

To load EZStart, from the DOS command line, type:

        [path]ezstart<Enter>

To load EZStart in Text Mode (instead of Graphics Mode), type

        [path]ezstart/T<Enter>

To load EZStart from Microsoft Windows, select the File menu in
Program Manager.  When the Run dialog box appears, type:

        [path]ezstart

in the Command Line box, then select the OK button.


INSTALLING A NETWORK DRIVER

EZStart accesses network driver files through the compressed driver
file, SMCG31.EXE.  The file name, SMCG31.EXE, specifies the family
of adapters supported ("G" denotes the SMC EtherEZ ISA Adapters),
and the version of the SuperDisk ("31" denotes 3.1).

EZStart copies the network driver and any other supporting files
from the compressed driver file to a location compatible with
your network operating system.  If your network operating system
requires an installation or options floppy diskette, or your PC
has a single floppy drive, you can build the installation or
options diskette by following these steps:

         Copy all of the files from the SuperDisk to your hard drive.

         Load EZStart 

         When prompted for the Destination Directory by the EZStart
         Driver Installation window, specify the floppy drive and use
         a new diskette (e.g., a:\)

Note: If you prefer, you can extract all of the network drivers
      from the SuperDisk by:

         Changing your default directory to the floppy drive where
         the SuperDisk resides

         Typing SMCG31 <Destination Directory> at the DOS prompt
         (where <Destination Directory> is the path where you want
         the extracted SuperDisk contents to reside)

Note: There must be at least 1.3 MB of available hard disk space


SuperDisk CONTENTS

The following list describes some of the files contained in
SMCG31.EXE:

       File                      Contents

    README.DOC     This file.


    !SMC8416.CFG - EISA configuration files for EtherEZ Plug and Play
                                  Adapters (8416xx)


Drivers for many LAN operating systems and software utilities are
also available through EZStart:

    Utilities/     Contents
    Drivers

    DOS ODI        Open data-link interface driver to be used with
                            ODI protocol stacks.

    EZSTART        Documentation for the EZStart Program.

    EZDOSODI       A program that performs an EZStart macro for
                   automatic NetWare client installation.

    EZSETUP        An adapter configuration utility program designed
                   to simplify adapter configuration in environments
                   where the complete functionality of EZStart is not
                   required.

    NDIS           DOS and OS/2 NDIS drivers to be used with NDIS-
                   compliant network operating systems and protocol
                   stacks. Subdirectories contain documentation and
                   .INI files for specific network operating systems.

    32-BIT ODI    Driver for Novell NetWare 386 version 4.x and 3.12.

    OS2 ODI        Driver for OS/2 nodes in Novell NetWare
                   environments.

    PCAGENT        Documentation and driver for SMC's PC Agent/SNMP
                   network-management utility.

The following drivers for Microsoft Windows operating systems are
also available on the root of the SuperDisk:

    WINDOWS for     Driver for Microsoft Windows for Workgroups 3.11.
    WORKGROUPS

    WINDOWS NT     Drivers for Microsoft Windows NT v3.51 and v4.0.

    WINDOWS 95     Driver for Microsoft Windows 95.

In addition to the driver or utility program itself, each
driver has the following associated files:

o  An INSTALL.DOC or other .DOC file that explains how to install
   and use the software; and

o  A RELEASE.DOC file that states the current version, release date,
   and any improvements made in this version of the software.


SUPPORTED ADAPTERS

The software on this SuperDisk operates with the following SMC
or Western Digital (WD) ISA and Micro Channel bus LAN adapters:

    Ethernet Coax:
                 EtherEZ Plug and Play (SMC8416B)
                 EtherCard Elite 16 Ultra (SMC 8216)
                 EtherCard PLUS Elite 16 (8013EPC)
                 EtherCard PLUS Elite 10T/A (8013WP/A)
                 EtherCard PLUS Elite (8003WC)

    Ethernet Twisted Pair:
                 EtherEZ Plug and Play (SMC8416T)
                 EtherCard Elite 16T Ultra (SMC 8216T)
                 EtherCard PLUS Elite 16T (8013WC)
                 EtherCard PLUS Elite 10T/A (8013EP/A)
                 EtherCard PLUS Elite (8003EP)

    Ethernet Combo:
                 EtherEZ Plug and Play (SMC8416BTA)
                 EtherCard Elite 16C Ultra Combo (SMC 8216C)
                 Ethercard Plus Elite 16 Combo (8013EWC) 
                
    Ethernet Duo:
                 EtherEZ Adapter (SMC 8416BT)

BOOT ROMs FOR SMC ETHERNET ADAPTERS

  Model             Adapters             Protocols Supported
  Number           Supported

  SMC-EROM    Elite Ultra (8216xx)    Native NetWare Protocol and
               SMC EtherEZ (8416xx)   RPL (Remote Program Load)
                                              Boot Protocol


TECHNICAL SUPPORT

For technical support, first contact your place of purchase. If you
need additional assistance, call our Technical Support departments:

  From U.S.A. and Canada (8:30 am - 8:00 pm Eastern Time)
    (800) SMC-4-YOU
    (516) 435-6250
    (516) 434-9314 (Fax)

  From Europe (8:00 am - 5:30 pm UK Greenwich Mean Time)
    44 (0) 1344 420068
    44 (0) 1344 418835 (Fax)


SMC offers Bulletin Board Services (BBS) from which you can obtain
the latest information on adapters and drivers for SMC products via
modem:

New York      (516) 434-3162
               up to 28,800 baud, 8 data bits, no parity, 1 stop bit

  Germany       49 (89) 928806-50
               up to 28,800 baud, 8 data bits, no parity, 1 stop bit

  France        33 (1) 39.73.57.00
               up to 14,400 baud, 8 data bits, no parity, 1 stop bit

  United        44 (0) 1344 418838
  Kingdom      up to 28,800 baud, 8 data bits, no parity, 1 stop bit


INTERNET Support:  SMC Technical Support can also be obtained through
the INTERNET. Our address is:  techsupport@smc.com or
european.techsupport@smc.com.

CompuServe Support:  For information and drivers for SMC network
products, you can also access the SMC Forum through CompuServe. Log
into CompuServe, and at the "!" prompt type "GO SMC".

World Wide Web:  http://www.smc.com

FTP Server:  ftp.smc.com
   Login as anonymous using email address as password. Binary files
   must be transferred using the binary keyword or option.

EliteFax (SMC's Fax-on-Demand System):
   U.S.A. and Canada:  (800) SMC-8329
   Elsewhere:  (516) 435-6107


This commercial computer software and documentation is provided with
RESTRICTED RIGHTS.  Use, duplication, or disclosure by the
Government is subject to restrictions set forth in subparagraph
(c) (1) (ii) of the Rights in Technical Data and Computer Software
clause at DFARS 252.227-7013, or subparagraphs (c) (1) and (2) of the
Commercial Computer Software -- Restricted Rights clause at
FAR 52.227-19, or Alternate III of the Rights in Data -- General
clause at FAR 52.227-14, as applicable.  The manufacturer /
subcontractor is Standard Microsystems Corporation, 80 Arkay Drive,
Hauppauge, NY 11788. 


SMC and Standard Microsystems are registered trademarks; and EtherEZ,
EtherCard Elite 16 Ultra, EtherCard Elite 16T Ultra, EtherCard Elite
16C Ultra, EliteSeries, EZStart, and SuperDisk are trademarks of 
Standard Microsystems Corporation.  Other product and company names
are registered trademarks or trademarks of their respective holders.
