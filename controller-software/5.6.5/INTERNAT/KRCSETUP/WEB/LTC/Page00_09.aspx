<%@ Register TagPrefix="iewc" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Page language="c#" Codebehind="Page00_09.aspx.cs" AutoEventWireup="false" Inherits="LifeTimeCounter.Page00_09" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Page00_09</title>
		<SCRIPT language="JavaScript">
<!-- Begin

function ChangeBg() 
{
	document.getElementsById("btnAddEntry")[0].focus();
}
//  End -->
		</SCRIPT>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body onLoad="ChangeBg()" MS_POSITIONING="GridLayout" background="images/bg.jpg">
		<form id="Page00_09" method="post" runat="server">
			<asp:label id="lblPageTitle" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 8px" runat="server" Font-Size="Small" Font-Bold="True" Font-Names="Arial" Width="240px" Height="8px">Replacement minutes</asp:label><asp:label id="LblDescription" style="Z-INDEX: 112; LEFT: 152px; POSITION: absolute; TOP: 104px" runat="server" Font-Size="8pt" Font-Names="Arial" Width="188px" Height="16px">Description</asp:label><asp:label id="LblSerialNo" style="Z-INDEX: 111; LEFT: 80px; POSITION: absolute; TOP: 104px" runat="server" Font-Size="8pt" Font-Names="Arial" Width="68px" Height="16px">SerialNo.</asp:label><asp:label id="LblOrderNo" style="Z-INDEX: 110; LEFT: 8px; POSITION: absolute; TOP: 104px" runat="server" Font-Size="8pt" Font-Names="Arial" Width="68px" Height="16px">OrderNo.</asp:label><asp:textbox id="ctlEdtOrderNo" style="Z-INDEX: 109; LEFT: 8px; POSITION: absolute; TOP: 120px" runat="server" Font-Size="8pt" Font-Names="Arial" Width="72px" Visible="False" ToolTip="enter serial no."></asp:textbox><asp:textbox id="ctlEdtSerial" style="Z-INDEX: 108; LEFT: 80px; POSITION: absolute; TOP: 120px" runat="server" Font-Size="8pt" Font-Names="Arial" Width="72px" Visible="False" ToolTip="enter serial no."></asp:textbox><asp:textbox id="ctlEdtDescription" style="Z-INDEX: 107; LEFT: 152px; POSITION: absolute; TOP: 120px" runat="server" Font-Size="8pt" Font-Names="Arial" Width="312px" Visible="False" ToolTip="enter description"></asp:textbox><asp:textbox id="ctl_UserName" style="Z-INDEX: 104; LEFT: 8px; POSITION: absolute; TOP: 40px" runat="server" Width="152px" ReadOnly="True" BorderStyle="None"></asp:textbox>
			<HR style="Z-INDEX: 102; LEFT: 8px; WIDTH: 93.79%; POSITION: absolute; TOP: 72px" width="93.79%" SIZE="2">
			<asp:table id="tblReplacement" style="Z-INDEX: 103; LEFT: 8px; POSITION: absolute; TOP: 140px" tabIndex="2" runat="server" Font-Size="10pt" Font-Names="Arial" Width="456px" Height="32px" BorderColor="Black" BorderWidth="1px" GridLines="Both"></asp:table><asp:dropdownlist id="ctlDDLPartList" style="Z-INDEX: 105; LEFT: 8px; POSITION: absolute; TOP: 80px" tabIndex="1" runat="server" Font-Size="8pt" Font-Names="Arial" Width="340px" Height="48px" AutoPostBack="True"></asp:dropdownlist><asp:button id="btnAddEntry" style="Z-INDEX: 106; LEFT: 352px; POSITION: absolute; TOP: 80px" tabIndex="2" runat="server" Font-Size="8pt" Font-Names="Arial" Width="112px" Height="24px" Text="Replace part"></asp:button></form>
	</body>
</HTML>
