<%@ Page language="c#" Codebehind="Page00_02.aspx.cs" AutoEventWireup="false" Inherits="LifeTimeCounter.Page00_02" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Motor & axis data.</title>
		<script type="text/javascript" src="KeyDummy.js"></script>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout" background="images/bg.jpg">
		<form id="Page00_02" method="post" runat="server">
			<asp:Label id="a_ctlSumBrakingDistance" style="Z-INDEX: 38; LEFT: 256px; POSITION: absolute; TOP: 276px" runat="server" Width="88px" Height="20px" Font-Names="Arial" BorderStyle="Solid" Font-Size="8pt" BackColor="#FFE0C0" BorderColor="#FFC080" BorderWidth="1px">.</asp:Label>
			<asp:Label id="lbl_UserName" style="Z-INDEX: 158; LEFT: 8px; POSITION: absolute; TOP: 40px" runat="server" BorderWidth="1px" BorderColor="#000040" Font-Size="8pt" BorderStyle="Solid" Font-Names="Arial" Height="20px" Width="244px">.</asp:Label>
			<asp:Button id="btnMainMenu_02" style="Z-INDEX: 157; LEFT: 352px; POSITION: absolute; TOP: 312px" tabIndex="3" runat="server" Font-Size="8pt" Font-Names="Arial" Height="24px" Width="88px" Text="Main menu"></asp:Button>
			<asp:Label id="m_lblShortCircuitBrakes" style="Z-INDEX: 67; LEFT: 8px; POSITION: absolute; TOP: 256px" runat="server" Font-Size="8pt" Font-Names="Arial" Height="20px" Width="200px" ForeColor="Crimson">ShortCircuitBrakes</asp:Label>
			<asp:Label id="m_ctlShortCircuitBrakes" style="Z-INDEX: 39; LEFT: 256px; POSITION: absolute; TOP: 256px" runat="server" BorderWidth="1px" BorderColor="#00C000" BackColor="#C0FFC0" Font-Size="8pt" BorderStyle="Solid" Font-Names="Arial" Height="20px" Width="88px">.</asp:Label>
			<asp:Label id="m_lblEmergencyStops" style="Z-INDEX: 75; LEFT: 8px; POSITION: absolute; TOP: 236px" runat="server" Font-Size="8pt" Font-Names="Arial" Height="20px" Width="200px" ForeColor="Crimson">Emergency stops</asp:Label>
			<asp:Label id="m_ctlEmergencyStops" style="Z-INDEX: 40; LEFT: 256px; POSITION: absolute; TOP: 236px" runat="server" BorderWidth="1px" BorderColor="#FFC080" BackColor="#FFE0C0" Font-Size="8pt" BorderStyle="Solid" Font-Names="Arial" Height="20px" Width="88px">.</asp:Label>
			<asp:Label id="a_lblSumBrakingDistance" style="Z-INDEX: 77; LEFT: 8px; POSITION: absolute; TOP: 280px" runat="server" Width="224px" Height="20px" Font-Names="Arial" Font-Size="8pt" ForeColor="Crimson">SumBrakingDistance</asp:Label>
			<asp:Label id="a_ctlRadialForceExpr" style="Z-INDEX: 37; LEFT: 352px; POSITION: absolute; TOP: 256px" runat="server" Width="88px" Height="20px" Font-Names="Arial" BorderStyle="Solid" Font-Size="8pt" BackColor="#FFE0C0" BorderColor="#FFC080" BorderWidth="1px">.</asp:Label>
			<asp:Label id="a_ctlAxialForceExpr" style="Z-INDEX: 57; LEFT: 256px; POSITION: absolute; TOP: 256px" runat="server" Width="88px" Height="20px" Font-Names="Arial" BorderStyle="Solid" Font-Size="8pt" BackColor="#FFE0C0" BorderColor="#FFC080" BorderWidth="1px">.</asp:Label>
			<asp:Label id="a_ctlBearingType" style="Z-INDEX: 151; LEFT: 112px; POSITION: absolute; TOP: 256px" runat="server" Width="121px" Height="16px" BorderStyle="None" Font-Names="Arial" Font-Bold="True" ForeColor="#0000C0" Font-Size="8pt">.</asp:Label>
			<asp:Label id="a_lblBearingType" style="Z-INDEX: 68; LEFT: 8px; POSITION: absolute; TOP: 256px" runat="server" Width="96px" Height="20px" Font-Names="Arial" Font-Size="8pt">Bearing:</asp:Label>
			<asp:Label id="a_ctlTorqueExprOverThres" style="Z-INDEX: 36; LEFT: 352px; POSITION: absolute; TOP: 236px" runat="server" Width="88px" Height="20px" Font-Names="Arial" BorderStyle="Solid" Font-Size="8pt" BackColor="#FFE0C0" BorderColor="#FFC080" BorderWidth="1px">.</asp:Label>
			<asp:Label id="a_ctlTorqueExprUnderThres" style="Z-INDEX: 58; LEFT: 256px; POSITION: absolute; TOP: 236px" runat="server" Width="88px" Height="20px" Font-Names="Arial" BorderStyle="Solid" Font-Size="8pt" BackColor="#FFE0C0" BorderColor="#FFC080" BorderWidth="1px">.</asp:Label>
			<asp:Label id="a_ctlGearingType" style="Z-INDEX: 148; LEFT: 112px; POSITION: absolute; TOP: 236px" runat="server" Width="121px" Height="16px" BorderStyle="None" Font-Names="Arial" Font-Bold="True" ForeColor="#0000C0" Font-Size="8pt">.</asp:Label>
			<asp:Label id="a_lblGearingType" style="Z-INDEX: 66; LEFT: 8px; POSITION: absolute; TOP: 236px" runat="server" Width="96px" Height="20px" Font-Names="Arial" Font-Size="8pt">Gearing:</asp:Label>
			<asp:Label id="a_lblMaximalTorqueTime01" style="Z-INDEX: 80; LEFT: 8px; POSITION: absolute; TOP: 216px" runat="server" Width="224px" Height="20px" Font-Names="Arial" Font-Size="8pt" ForeColor="Crimson">MaximalTorqueTime (1, 2, 3) [s]</asp:Label>
			<asp:Label id="a_ctlMaximalTorqueTime03" style="Z-INDEX: 51; LEFT: 384px; POSITION: absolute; TOP: 216px" runat="server" Width="56px" Height="20px" Font-Names="Arial" BorderStyle="Solid" Font-Size="8pt" BackColor="#C0FFC0" BorderColor="#00C000" BorderWidth="1px">.</asp:Label>
			<asp:Label id="a_ctlMaximalTorqueTime02" style="Z-INDEX: 52; LEFT: 320px; POSITION: absolute; TOP: 216px" runat="server" Width="56px" Height="20px" Font-Names="Arial" BorderStyle="Solid" Font-Size="8pt" BackColor="#C0FFC0" BorderColor="#00C000" BorderWidth="1px">.</asp:Label>
			<asp:Label id="a_ctlMaximalTorqueTime01" style="Z-INDEX: 48; LEFT: 256px; POSITION: absolute; TOP: 216px" runat="server" Width="56px" Height="20px" Font-Names="Arial" BorderStyle="Solid" Font-Size="8pt" BackColor="#C0FFC0" BorderColor="#00C000" BorderWidth="1px">.</asp:Label>
			<asp:Label id="a_lblMaximalTorquePeak" style="Z-INDEX: 81; LEFT: 8px; POSITION: absolute; TOP: 196px" runat="server" Width="224px" Height="20px" Font-Names="Arial" Font-Size="8pt" ForeColor="Crimson">MaximalTorquePeak</asp:Label>
			<asp:Label id="a_ctlMaximalTorquePeak" style="Z-INDEX: 59; LEFT: 256px; POSITION: absolute; TOP: 196px" runat="server" Width="88px" Height="20px" Font-Names="Arial" BorderStyle="Solid" Font-Size="8pt" BackColor="#FFE0C0" BorderColor="#FFC080" BorderWidth="1px">.</asp:Label>
			<asp:Label id="a_lblMeanTorqueValue" style="Z-INDEX: 82; LEFT: 8px; POSITION: absolute; TOP: 176px" runat="server" Width="224px" Height="20px" Font-Names="Arial" Font-Size="8pt" ForeColor="Crimson">MeanTorque [v / t]</asp:Label>
			<asp:Label id="a_lblMeanRotationalAccelerationValue" style="Z-INDEX: 83; LEFT: 8px; POSITION: absolute; TOP: 156px" runat="server" Width="224px" Height="20px" Font-Names="Arial" Font-Size="8pt" ForeColor="Crimson">MeanRotationalAccel. [v / t]</asp:Label>
			<asp:Label id="a_ctlMeanTorqueTime" style="Z-INDEX: 35; LEFT: 352px; POSITION: absolute; TOP: 176px" runat="server" Width="89px" Height="20px" Font-Names="Arial" BorderStyle="Solid" Font-Size="8pt" BackColor="#FFE0C0" BorderColor="#FFC080" BorderWidth="1px">.</asp:Label>
			<asp:Label id="a_ctlMeanTorqueValue" style="Z-INDEX: 60; LEFT: 256px; POSITION: absolute; TOP: 176px" runat="server" Width="88px" Height="20px" Font-Names="Arial" BorderStyle="Solid" Font-Size="8pt" BackColor="#FFE0C0" BorderColor="#FFC080" BorderWidth="1px">.</asp:Label>
			<asp:Label id="a_ctlMeanRotationalAccelerationTime" style="Z-INDEX: 34; LEFT: 352px; POSITION: absolute; TOP: 156px" runat="server" Width="89px" Height="20px" Font-Names="Arial" BorderStyle="Solid" Font-Size="8pt" BackColor="#FFE0C0" BorderColor="#FFC080" BorderWidth="1px">.</asp:Label>
			<asp:Label id="a_ctlMeanRotationalAccelerationValue" style="Z-INDEX: 61; LEFT: 256px; POSITION: absolute; TOP: 156px" runat="server" Width="88px" Height="20px" Font-Names="Arial" BorderStyle="Solid" Font-Size="8pt" BackColor="#FFE0C0" BorderColor="#FFC080" BorderWidth="1px">.</asp:Label>
			<asp:Label id="a_lblMeanRotationalSpeedValue" style="Z-INDEX: 84; LEFT: 8px; POSITION: absolute; TOP: 136px" runat="server" Width="224px" Height="20px" Font-Names="Arial" Font-Size="8pt" ForeColor="Crimson">MeanRotationalSpeed [v / t]</asp:Label>
			<asp:Label id="a_ctlMeanRotationalSpeedTime" style="Z-INDEX: 33; LEFT: 352px; POSITION: absolute; TOP: 136px" runat="server" Width="89px" Height="20px" Font-Names="Arial" BorderStyle="Solid" Font-Size="8pt" BackColor="#FFE0C0" BorderColor="#FFC080" BorderWidth="1px">.</asp:Label>
			<asp:Label id="a_ctlMeanRotationalSpeedValue" style="Z-INDEX: 62; LEFT: 256px; POSITION: absolute; TOP: 136px" runat="server" Width="88px" Height="20px" Font-Names="Arial" BorderStyle="Solid" Font-Size="8pt" BackColor="#FFE0C0" BorderColor="#FFC080" BorderWidth="1px">.</asp:Label>
			<asp:Label id="a_lblKSDOperatingTime" style="Z-INDEX: 95; LEFT: 8px; POSITION: absolute; TOP: 96px" runat="server" Width="224px" Height="20px" Font-Names="Arial" Font-Size="8pt">KSDOperatingTime  [h]</asp:Label>
			<asp:Label id="a_lblKSDControllingTime" style="Z-INDEX: 94; LEFT: 8px; POSITION: absolute; TOP: 76px" runat="server" Width="224px" Height="20px" Font-Names="Arial" Font-Size="8pt">KSDControllingTime [h]</asp:Label>
			<asp:Label id="a_lblMotionTime" style="Z-INDEX: 85; LEFT: 8px; POSITION: absolute; TOP: 116px" runat="server" Width="224px" Height="20px" Font-Names="Arial" Font-Size="8pt">MotionTime [h:mm:ss.ms]</asp:Label>
			<asp:Label id="a_ctlMotionTime" style="Z-INDEX: 63; LEFT: 256px; POSITION: absolute; TOP: 116px" runat="server" Width="88px" Height="20px" Font-Names="Arial" BorderStyle="Solid" Font-Size="8pt" BackColor="#C0FFC0" BorderColor="#00C000" BorderWidth="1px">.</asp:Label>
			<asp:Label id="a_ctlKSDControllingTime" style="Z-INDEX: 65; LEFT: 256px; POSITION: absolute; TOP: 76px" runat="server" Width="88px" Height="20px" Font-Names="Arial" BorderStyle="Solid" Font-Size="8pt" BackColor="#FFE0C0" BorderColor="#FFC080" BorderWidth="1px">.</asp:Label>
			<HR style="Z-INDEX: 133; LEFT: 8px; WIDTH: 93.79%; POSITION: absolute; TOP: 72px" width="93.79%" SIZE="2">
			<asp:button id="btnReload" style="Z-INDEX: 126; LEFT: 256px; POSITION: absolute; TOP: 312px" runat="server" Width="88px" Text="Reload" Font-Names="Arial" Font-Size="8pt" Height="24px" tabIndex="2"></asp:button><asp:Label id="m_ctlKPSDrivesContactorOnTime" style="Z-INDEX: 47; LEFT: 256px; POSITION: absolute; TOP: 76px" runat="server" Width="88px" Height="20px" Font-Names="Arial" BorderStyle="Solid" Font-Size="8pt" BackColor="#FFE0C0" BorderColor="#FFC080" BorderWidth="1px">.</asp:Label>
			<asp:Label id="m_ctlKPSOperatingTime" style="Z-INDEX: 46; LEFT: 256px; POSITION: absolute; TOP: 96px" runat="server" Width="88px" Height="20px" Font-Names="Arial" BorderStyle="Solid" Font-Size="8pt" BackColor="#FFE0C0" BorderColor="#FFC080" BorderWidth="1px">.</asp:Label>
			<asp:Label id="m_ctlControllingTime" style="Z-INDEX: 45; LEFT: 256px; POSITION: absolute; TOP: 116px" runat="server" Width="88px" Height="20px" Font-Names="Arial" BorderStyle="Solid" Font-Size="8pt" BackColor="#FFE0C0" BorderColor="#FFC080" BorderWidth="1px">.</asp:Label>
			<asp:Label id="m_ctlMotionTime" style="Z-INDEX: 128; LEFT: 256px; POSITION: absolute; TOP: 136px" runat="server" Width="88px" Height="20px" Font-Names="Arial" BorderStyle="Solid" Font-Size="8pt" BackColor="#FFE0C0" BorderColor="#FFC080" BorderWidth="1px">.</asp:Label>
			<asp:Label id="m_ctlQuickStops" style="Z-INDEX: 44; LEFT: 256px; POSITION: absolute; TOP: 156px" runat="server" Width="88px" Height="20px" Font-Names="Arial" BorderStyle="Solid" Font-Size="8pt" BackColor="#FFE0C0" BorderColor="#FFC080" BorderWidth="1px">.</asp:Label>
			<asp:Label id="m_ctlErrorRampStops" style="Z-INDEX: 43; LEFT: 256px; POSITION: absolute; TOP: 176px" runat="server" Width="88px" Height="20px" Font-Names="Arial" BorderStyle="Solid" Font-Size="8pt" BackColor="#C0FFC0" BorderColor="#00C000" BorderWidth="1px">.</asp:Label>
			<asp:Label id="m_ctlOperatorRampStops" style="Z-INDEX: 42; LEFT: 256px; POSITION: absolute; TOP: 196px" runat="server" Width="88px" Height="20px" Font-Names="Arial" BorderStyle="Solid" Font-Size="8pt" BackColor="#C0FFC0" BorderColor="#00C000" BorderWidth="1px">.</asp:Label>
			<asp:Label id="m_ctlTransitionsEmgStop2QuickStop" style="Z-INDEX: 41; LEFT: 256px; POSITION: absolute; TOP: 216px" runat="server" Width="88px" Height="20px" Font-Names="Arial" BorderStyle="Solid" Font-Size="8pt" BackColor="#FFE0C0" BorderColor="#FFC080" BorderWidth="1px">.</asp:Label>
			<asp:Label id="m_lblKPSDrivesContactorOnTime" style="Z-INDEX: 87; LEFT: 8px; POSITION: absolute; TOP: 76px" runat="server" Width="224px" Height="20px" Font-Names="Arial" Font-Size="8pt">KPSDrivesContactorOnTime  [h]</asp:Label>
			<asp:Label id="m_lblKPSOperatingTime" style="Z-INDEX: 86; LEFT: 8px; POSITION: absolute; TOP: 96px" runat="server" Width="224px" Height="20px" Font-Names="Arial" Font-Size="8pt">KPSOperatingTime [h]</asp:Label>
			<asp:Label id="m_lblControllingTime" style="Z-INDEX: 69; LEFT: 8px; POSITION: absolute; TOP: 116px" runat="server" Width="224px" Height="20px" Font-Names="Arial" Font-Size="8pt">ControllingTime [h:mm:ss.ms]</asp:Label>
			<asp:Label id="m_lblMotionTime" style="Z-INDEX: 70; LEFT: 8px; POSITION: absolute; TOP: 136px" runat="server" Width="224px" Height="20px" Font-Names="Arial" Font-Size="8pt">MotionTime [h:mm:ss.ms]</asp:Label>
			<asp:Label id="m_lblQuickStops" style="Z-INDEX: 71; LEFT: 8px; POSITION: absolute; TOP: 156px" runat="server" Width="224px" Height="20px" Font-Names="Arial" Font-Size="8pt" ForeColor="Crimson">QuickStops</asp:Label>
			<asp:Label id="m_LblErrorRampStops" style="Z-INDEX: 72; LEFT: 8px; POSITION: absolute; TOP: 176px" runat="server" Width="224px" Height="20px" Font-Names="Arial" Font-Size="8pt" ForeColor="Crimson">ErrorRampStops</asp:Label>
			<asp:Label id="m_lblOperatorRampStops" style="Z-INDEX: 73; LEFT: 8px; POSITION: absolute; TOP: 196px" runat="server" Width="224px" Height="20px" Font-Names="Arial" Font-Size="8pt" ForeColor="Crimson">OperatorRampStops</asp:Label>
			<asp:Label id="m_lblTransitionsEmgStop2QuickStop" style="Z-INDEX: 74; LEFT: 8px; POSITION: absolute; TOP: 216px" runat="server" Width="200px" Height="20px" Font-Names="Arial" Font-Size="8pt" ForeColor="Crimson">TransitionsEmgStop2QuickStop</asp:Label>
			<asp:Label id="LblTitle" style="Z-INDEX: 134; LEFT: 8px; POSITION: absolute; TOP: 8px" runat="server" Width="136px" Height="8px" Font-Names="Arial" Font-Size="Small" Font-Bold="True">LTC</asp:Label>
			<asp:DropDownList id="ctlDDlist" style="Z-INDEX: 135; LEFT: 256px; POSITION: absolute; TOP: 40px" runat="server" Width="184px" Height="32px" AutoPostBack="True" Font-Names="Arial" Font-Size="8pt" tabIndex="1">
				<asp:ListItem Value="0" Selected="True">Cabinet</asp:ListItem>
				<asp:ListItem Value="1">Axis 01</asp:ListItem>
				<asp:ListItem Value="2">Axis 02</asp:ListItem>
				<asp:ListItem Value="3">Axis 03</asp:ListItem>
				<asp:ListItem Value="4">Axis 04</asp:ListItem>
				<asp:ListItem Value="5">Axis 05</asp:ListItem>
				<asp:ListItem Value="6">Axis 06</asp:ListItem>
			</asp:DropDownList>
			<asp:Label id="lbl_SelectDataSet" style="Z-INDEX: 136; LEFT: 256px; POSITION: absolute; TOP: 24px" runat="server" Width="136px" Height="12px" Font-Names="Arial" Font-Size="8pt">Select data set:</asp:Label>
			<asp:Label id="a_ctlKSDOperatingTime" style="Z-INDEX: 64; LEFT: 256px; POSITION: absolute; TOP: 96px" runat="server" Width="88px" Height="20px" Font-Names="Arial" BorderStyle="Solid" Font-Size="8pt" BackColor="#FFE0C0" BorderColor="#FFC080" BorderWidth="1px">.</asp:Label></form>
	</body>
</HTML>
