<%@ Page language="c#" Codebehind="Page00_03.aspx.cs" AutoEventWireup="false" Inherits="LifeTimeCounter.Page00_03" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Page00-03</title>
		<script type="text/javascript" src="KeyDummy.js"></script>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body background="images/bg.jpg" MS_POSITIONING="GridLayout">
		<form id="Page00_03" method="post" runat="server">
			<asp:button id="btnNewEntry" style="Z-INDEX: 101; LEFT: 188px; POSITION: absolute; TOP: 40px" tabIndex="2" runat="server" Font-Size="8pt" Font-Names="Arial" Height="24px" Width="136px" Text="Add new Entry"></asp:button><asp:button id="btnMainMenu_03" style="Z-INDEX: 105; LEFT: 332px; POSITION: absolute; TOP: 40px" tabIndex="3" runat="server" Font-Size="8pt" Font-Names="Arial" Height="24px" Width="136px" Text="Main menu"></asp:button><asp:label id="lblTitle" style="Z-INDEX: 103; LEFT: 8px; POSITION: absolute; TOP: 8px" runat="server" Font-Size="Small" Font-Names="Arial" Height="8px" Width="240px" Font-Bold="True">Maintenance minutes</asp:label>
			<HR style="Z-INDEX: 104; LEFT: 8px; WIDTH: 93.79%; POSITION: absolute; TOP: 72px" width="93.79%" SIZE="2">
			<asp:datagrid id="DataGrid1" style="Z-INDEX: 106; LEFT: 8px; POSITION: absolute; TOP: 84px" tabIndex="4" runat="server" Font-Size="8pt" Font-Names="Arial" Height="0px" Width="460px" AutoGenerateColumns="False" AllowPaging="True" BorderColor="Black">
				<AlternatingItemStyle BackColor="#80FF80"></AlternatingItemStyle>
				<HeaderStyle Font-Bold="True" BorderColor="#C04000" BackColor="#FFC080"></HeaderStyle>
				<Columns>
					<asp:BoundColumn DataField="Name" HeaderText="#Name"></asp:BoundColumn>
					<asp:BoundColumn DataField="Date" HeaderText="#Date"></asp:BoundColumn>
					<asp:BoundColumn DataField="POT" HeaderText="#POT"></asp:BoundColumn>
					<asp:BoundColumn DataField="MOT" HeaderText="#Mot"></asp:BoundColumn>
					<asp:BoundColumn DataField="Type" HeaderText="#Type"></asp:BoundColumn>
				</Columns>
				<PagerStyle BackColor="#FFC080"></PagerStyle>
			</asp:datagrid><asp:dropdownlist id="ctlPulldownBox" style="Z-INDEX: 107; LEFT: 8px; POSITION: absolute; TOP: 40px" tabIndex="1" runat="server" Font-Size="8pt" Font-Names="Arial" Height="28px" Width="173px"></asp:dropdownlist></form>
	</body>
</HTML>
