<%@ Register TagPrefix="iewc" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Page language="c#" Codebehind="Page00_04.aspx.cs" AutoEventWireup="false" Inherits="LifeTimeCounter.Page00_04" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Page00-04</title>
		<style>.CssStyle1 { }
		</style>
		<script type="text/javascript" src="KeyDummy.js"></script>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body background="images/bg.jpg" MS_POSITIONING="GridLayout">
		<form id="Page00_04" method="post" runat="server">
			<asp:label id="lblPageTitle" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 8px" runat="server" Height="8px" Width="240px" Font-Names="Arial" Font-Bold="True" Font-Size="Small">Replacement minutes</asp:label>
			<asp:button id="btnRplList" style="Z-INDEX: 106; LEFT: 244px; POSITION: absolute; TOP: 44px" tabIndex="3" runat="server" Height="24px" Width="112px" Font-Names="Arial" Font-Size="8pt" Text="List"></asp:button>
			<asp:button id="btnMainMenu_04" style="Z-INDEX: 105; LEFT: 124px; POSITION: absolute; TOP: 44px" tabIndex="2" runat="server" Height="24px" Width="112px" Font-Names="Arial" Font-Size="8pt" Text="Main menu"></asp:button>
			<HR style="Z-INDEX: 102; LEFT: 8px; WIDTH: 93.79%; POSITION: absolute; TOP: 72px" width="93.79%" SIZE="2">
			<asp:button id="btnAddEntry" style="Z-INDEX: 103; LEFT: 8px; POSITION: absolute; TOP: 44px" tabIndex="1" runat="server" Height="24px" Width="112px" Font-Names="Arial" Font-Size="8pt" Text="Replace part"></asp:button>
			<iewc:treeview id="TreeView1" style="Z-INDEX: 104; LEFT: 8px; POSITION: absolute; TOP: 80px" tabIndex="4" runat="server" Height="244px" Width="416px" Font-Names="Arial" Font-Size="10pt" AutoPostBack="True" BorderStyle="Inset" CssClass="CssStyle1" DefaultStyle="color:navy;background:none;border:none;" HoverStyle="color:black;background:none;border:solid;border-color:navy;border-width:thin;"></iewc:treeview>
		</form>
		<P></P>
	</body>
</HTML>
