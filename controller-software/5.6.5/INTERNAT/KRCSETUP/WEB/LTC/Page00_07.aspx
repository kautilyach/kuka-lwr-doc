<%@ Page language="c#" Codebehind="Page00_07.aspx.cs" AutoEventWireup="false" Inherits="LifeTimeCounter.Page00_07" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Motor & axis data.</title>
		<script type="text/javascript" src="KeyDummy.js"></script>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout" background="images/bg.jpg">
		<form id="Page00_07" method="post" runat="server">
			<HR style="Z-INDEX: 101; LEFT: 8px; WIDTH: 93.79%; POSITION: absolute; TOP: 72px" width="93.79%" SIZE="2">
			<asp:Label id="lbl_UserName" style="Z-INDEX: 158; LEFT: 8px; POSITION: absolute; TOP: 44px" runat="server" Font-Names="Arial" Font-Size="8pt" Height="20px" Width="344px" BorderStyle="Solid" BorderColor="#000040" BorderWidth="1px">.</asp:Label>
			<asp:Button id="btnMainMenu_07" style="Z-INDEX: 106; LEFT: 356px; POSITION: absolute; TOP: 43px" tabIndex="1" runat="server" Width="100px" Height="24px" Font-Size="8pt" Font-Names="Arial" Text="Main menu"></asp:Button>
			<asp:Table id="ctlTblProgressBar" style="Z-INDEX: 105; LEFT: 8px; POSITION: absolute; TOP: 108px" runat="server" BorderWidth="1px" BorderColor="Transparent" BorderStyle="None" Height="192px" Width="452px"></asp:Table>
			<asp:Label id="LblTitle" style="Z-INDEX: 102; LEFT: 8px; POSITION: absolute; TOP: 8px" runat="server" Width="136px" Height="8px" Font-Names="Arial" Font-Size="Small" Font-Bold="True">LTC</asp:Label>
			<asp:DropDownList id="ctlDDlist" style="Z-INDEX: 104; LEFT: 4px; POSITION: absolute; TOP: 84px" runat="server" Width="460px" Height="32px" AutoPostBack="True" Font-Names="Arial" Font-Size="8pt" tabIndex="2">
				<asp:ListItem Value="0" Selected="True">Axis : power on time</asp:ListItem>
				<asp:ListItem Value="1">Axis : motion time</asp:ListItem>
				<asp:ListItem Value="2">Cabinet : counters</asp:ListItem>
			</asp:DropDownList>
		</form>
	</body>
</HTML>
