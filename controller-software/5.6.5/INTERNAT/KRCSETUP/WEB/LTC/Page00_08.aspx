<%@ Page language="c#" Codebehind="Page00_08.aspx.cs" AutoEventWireup="false" Inherits="LifeTimeCounter.Page00_08" %>
<%@ Register TagPrefix="iewc" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Page00-08</title>
		<script type="text/javascript" src="KeyDummy.js"></script>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body background="images/bg.jpg" MS_POSITIONING="GridLayout">
		<form id="Page00_08" method="post" runat="server">
			<asp:label id="lblTitle_08" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 8px" runat="server" Font-Size="Small" Font-Bold="True" Font-Names="Arial" Width="240px" Height="8px">Replacement minutes</asp:label><asp:textbox id="ctlTextComment" style="Z-INDEX: 108; LEFT: 208px; POSITION: absolute; TOP: 76px" tabIndex="2" runat="server" Font-Size="8pt" Font-Names="Arial" Width="204px" Height="20px"></asp:textbox><asp:label id="lblComment" style="Z-INDEX: 107; LEFT: 148px; POSITION: absolute; TOP: 80px" runat="server" Font-Size="8pt" Font-Names="Arial" Width="52px" Height="12px">Comment:</asp:label><asp:label id="lblSerial" style="Z-INDEX: 106; LEFT: 12px; POSITION: absolute; TOP: 80px" runat="server" Font-Size="8pt" Font-Names="Arial" Width="56px" Height="12px">SerialNo:</asp:label><asp:textbox id="ctlTextSerial" style="Z-INDEX: 105; LEFT: 76px; POSITION: absolute; TOP: 76px" tabIndex="1" runat="server" Font-Size="8pt" Font-Names="Arial" Width="64px" Height="20px"></asp:textbox><asp:datagrid id="DataGrid1" style="Z-INDEX: 104; LEFT: 8px; POSITION: absolute; TOP: 100px" tabIndex="4" runat="server" Font-Size="XX-Small" Font-Names="Arial" Width="424px" Height="0px" AllowPaging="True" AutoGenerateColumns="False" BorderColor="Black">
				<SelectedItemStyle Font-Size="XX-Small" Font-Names="Arial" Wrap="False"></SelectedItemStyle>
				<EditItemStyle Font-Size="XX-Small" Font-Names="Arial" Wrap="False"></EditItemStyle>
				<AlternatingItemStyle Font-Size="XX-Small" Font-Names="Arial" Wrap="False" BackColor="#80FF80"></AlternatingItemStyle>
				<ItemStyle Font-Size="XX-Small" Font-Names="Arial"></ItemStyle>
				<HeaderStyle Font-Size="XX-Small" Font-Names="Arial" BorderColor="#C04000" BackColor="#FFC080"></HeaderStyle>
				<FooterStyle Font-Size="XX-Small" Font-Names="Arial"></FooterStyle>
				<Columns>
					<asp:BoundColumn DataField="PartID" ReadOnly="True" HeaderText="#PartID">
						<HeaderStyle Width="10%"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="Name" ReadOnly="True" HeaderText="#Name">
						<HeaderStyle Width="10%"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="Date" ReadOnly="True" HeaderText="#Date">
						<HeaderStyle Width="15%"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="POT" ReadOnly="True" HeaderText="#POT">
						<HeaderStyle Width="5%"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="MOT" ReadOnly="True" HeaderText="#Mot">
						<HeaderStyle Width="5%"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="OrderNo" ReadOnly="True" HeaderText="#OrderNo">
						<HeaderStyle Width="10%"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="SerialNo" ReadOnly="True" HeaderText="#SerialNo">
						<HeaderStyle Width="10%"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="Description" ReadOnly="True" HeaderText="#Description">
						<HeaderStyle Width="30%"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
					</asp:BoundColumn>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="U" HeaderText="Edit" CancelText="C" EditText="E">
						<HeaderStyle Width="5%"></HeaderStyle>
						<ItemStyle Font-Size="XX-Small" Wrap="False"></ItemStyle>
					</asp:EditCommandColumn>
				</Columns>
				<PagerStyle Font-Size="XX-Small" Font-Names="Arial" BackColor="#FFC080"></PagerStyle>
			</asp:datagrid><asp:button id="btnMainMenu_08" style="Z-INDEX: 103; LEFT: 8px; POSITION: absolute; TOP: 44px" tabIndex="5" runat="server" Font-Size="8pt" Font-Names="Arial" Width="136px" Height="24px" Text="Main menu"></asp:button>
			<HR style="Z-INDEX: 102; LEFT: 8px; WIDTH: 93.79%; POSITION: absolute; TOP: 72px" width="93.79%" SIZE="2">
			<asp:Button id="btnSubmit" style="Z-INDEX: 109; LEFT: 148px; POSITION: absolute; TOP: 44px" tabIndex="3" runat="server" Height="24px" Width="136px" Font-Names="Arial" Font-Size="8pt" Text="ok" Visible="False"></asp:Button>
		</form>
	</body>
</HTML>
