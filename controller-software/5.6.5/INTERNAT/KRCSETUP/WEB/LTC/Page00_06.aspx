<%@ Page language="c#" Codebehind="Page00_06.aspx.cs" AutoEventWireup="false" Inherits="LifeTimeCounter.Page00_06" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Page00-06</title>
		<script type="text/javascript" src="KeyDummy.js"></script>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body background="images/bg.jpg" MS_POSITIONING="GridLayout">
		<form id="Page00_06" method="post" runat="server">
			<asp:label id="lblPageTitle_06" style="Z-INDEX: 100; LEFT: 8px; POSITION: absolute; TOP: 8px" runat="server" Height="8px" Width="420px" Font-Names="Arial" Font-Bold="True" Font-Size="Small">LTC file download</asp:label>
			<asp:button id="btnMainMenu_06" style="Z-INDEX: 103; LEFT: 140px; POSITION: absolute; TOP: 280px" tabIndex="8" runat="server" Height="24px" Width="112px" Font-Names="Arial" Font-Size="8pt" Text="Main menu"></asp:button>
			<asp:checkbox id="cbRawData" style="Z-INDEX: 104; LEFT: 12px; POSITION: absolute; TOP: 88px" tabIndex="1" runat="server" Height="20px" Width="308px" Font-Names="Arial" Text="Raw Data" Checked="True" Font-Size="10pt"></asp:checkbox>
			<asp:checkbox id="cbReplacementData" style="Z-INDEX: 105; LEFT: 12px; POSITION: absolute; TOP: 112px" tabIndex="2" runat="server" Height="20px" Width="308px" Font-Names="Arial" Text="Replacement Data" Checked="True" Font-Size="10pt"></asp:checkbox>
			<asp:checkbox id="cbMaintenanceData" style="Z-INDEX: 106; LEFT: 12px; POSITION: absolute; TOP: 136px" tabIndex="3" runat="server" Height="20px" Width="308px" Font-Names="Arial" Text="Maintenance Data" Checked="True" Font-Size="10pt"></asp:checkbox>
			<asp:checkbox id="cbConfigData" style="Z-INDEX: 107; LEFT: 12px; POSITION: absolute; TOP: 160px" tabIndex="4" runat="server" Height="20px" Width="308px" Font-Names="Arial" Text="Config Data" Checked="True" Font-Size="10pt"></asp:checkbox>
			<asp:checkbox id="cbLanguageData" style="Z-INDEX: 108; LEFT: 12px; POSITION: absolute; TOP: 184px" tabIndex="5" runat="server" Height="20px" Width="308px" Font-Names="Arial" Text="Language Data" Checked="True" Font-Size="10pt"></asp:checkbox>
			<asp:checkbox id="cbMaintenanceConfigData" style="Z-INDEX: 109; LEFT: 12px; POSITION: absolute; TOP: 208px" tabIndex="6" runat="server" Height="20px" Width="308px" Font-Names="Arial" Text="Maintenance Config Data" Checked="True" Font-Size="10pt"></asp:checkbox>
			<asp:button id="btnDownLoad" style="Z-INDEX: 110; LEFT: 12px; POSITION: absolute; TOP: 280px" tabIndex="7" runat="server" Height="24px" Width="124px" Text="Download" Font-Size="8pt" Font-Names="Arial"></asp:button>
		</form>
		<HR style="Z-INDEX: 102; LEFT: 8px; WIDTH: 93.79%; POSITION: absolute; TOP: 72px" width="93.79%" SIZE="2">
		<asp:Label id="lbl_UserName" style="Z-INDEX: 111; LEFT: 8px; POSITION: absolute; TOP: 40px" runat="server" Font-Size="8pt" Font-Names="Arial" Width="424px" Height="20px" BorderStyle="Solid" BorderWidth="1px" BorderColor="#000040"></asp:Label>
	</body>
</HTML>
