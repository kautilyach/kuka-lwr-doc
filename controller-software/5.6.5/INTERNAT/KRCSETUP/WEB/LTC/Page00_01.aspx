<%@ Page language="c#" Codebehind="Page00_01.aspx.cs" AutoEventWireup="false" Inherits="LifeTimeCounter.Page00_01" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Page00-01</title>
		<script type="text/javascript" src="KeyDummy.js"></script>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout" background="images/bg.jpg">
		<form id="Page00_01" method="post" runat="server">
			<asp:Label id="lbl_UserName" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 40px" runat="server" BorderStyle="Solid" Width="424px" Font-Names="Arial" Height="20px" Font-Size="8pt" BorderColor="#000040" BorderWidth="1px"></asp:Label>
			<asp:Label id="lblTitle_01" style="Z-INDEX: 102; LEFT: 8px; POSITION: absolute; TOP: 8px" runat="server" Height="24px" Width="160px" Font-Names="Arial" Font-Bold="True">Main menue</asp:Label>
			<HR style="Z-INDEX: 103; LEFT: 8px; WIDTH: 93.79%; POSITION: absolute; TOP: 72px" width="93.79%" SIZE="2">
			<asp:Button id="btnReplacementLog" style="Z-INDEX: 104; LEFT: 8px; POSITION: absolute; TOP: 80px" runat="server" Height="24px" Width="136px" Text="Replacement log" tabIndex="1" Font-Size="8pt" Font-Names="Arial"></asp:Button>
			<asp:Button id="btnMaintenanceLog" style="Z-INDEX: 105; LEFT: 8px; POSITION: absolute; TOP: 112px" runat="server" Height="24px" Width="136px" Text="Maintenance log" tabIndex="2" Font-Size="8pt" Font-Names="Arial"></asp:Button>
			<asp:Button id="btnLifeTimeData" style="Z-INDEX: 106; LEFT: 8px; POSITION: absolute; TOP: 144px" runat="server" Height="24px" Width="136px" Text="Life time data" tabIndex="3" Font-Size="8pt" Font-Names="Arial"></asp:Button>
			<asp:Button id="btnRawData" style="Z-INDEX: 107; LEFT: 8px; POSITION: absolute; TOP: 176px" runat="server" Height="24px" Width="136px" Text="Raw kernel system data" Visible="False" tabIndex="4" Font-Size="8pt" Font-Names="Arial"></asp:Button>
			<asp:Button id="btnFileTransfer" style="Z-INDEX: 108; LEFT: 8px; POSITION: absolute; TOP: 208px" runat="server" Height="24px" Width="136px" Text="File upload" Visible="False" tabIndex="5" Font-Size="8pt" Font-Names="Arial"></asp:Button>
			<asp:Button id="btnFileDownload" style="Z-INDEX: 109; LEFT: 8px; POSITION: absolute; TOP: 240px" runat="server" Height="24px" Width="136px" Text="File download" Visible="False" tabIndex="6" Font-Size="8pt" Font-Names="Arial"></asp:Button>
			<asp:Button id="btnLogOut" style="Z-INDEX: 110; LEFT: 8px; POSITION: absolute; TOP: 272px" runat="server" Height="24px" Width="136px" Text="Log out" tabIndex="7" Font-Size="8pt" Font-Names="Arial"></asp:Button>
		</form>
	</body>
</HTML>
