<%@ Language=VBScript %>
<!--#INCLUDE VIRTUAL="/_ScriptLibrary/rs.asp"-->
<HTML>
<HEAD>
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
</HEAD>
<BODY>

<%
Class WebRobot

	public sub ConnectRob
   	    set Session("Cross")=Server.CreateObject("WebRobot.ConnectToCrossComm")
	    Session("Cross").CrossComm.ConnectToCross "WebDiagnose"
	end sub
	
	public sub DisconnectRob
		Session("Cross").CrossComm.ServerOff
		set Session("Cross")=nothing
	end sub
		
	public function getVar(varName)
	   getVar=Session("Cross").CrossComm.getVarValue(varName)
	end function

	public function GetLogInfo(dwFilter)
	   GetLogInfo=Session("Cross").CrossComm.RetreiveItems(dwFilter,10) 
	end function

	public function isRemote
		addrWebServer=Request.ServerVariables("LOCAL_ADDR")
		addrBrowser=Request.ServerVariables("REMOTE_ADDR")
		if addrWebServer=addrBrowser then
			isRemote=false
		else
			isRemote=true
		end if	
	end function
	
	public function ReadRegVal(regval)
		HKLMKUKARob = "HKLM\SOFTWARE\Kuka Roboter GmbH\"
		regval = Replace (regval,"/","\")
		Set shell = Server.CreateObject("WScript.Shell")
		temp = shell.RegRead (HKLMKUKARob & regval)
		set shell = nothing
		ReadRegVal = temp
	end function

	public function GetBOFVer()
		Set fs = Server.CreateObject("Scripting.FileSystemObject")
		temp = fs.GetFileVersion("c:\KRC\HMI\Kuka_HMI.exe")
		set fs = nothing
		GetBOFVer = temp
	end function
	
end Class

set public_description=new WebRobot

RSDispatch
%>


</BODY>
</HTML>
