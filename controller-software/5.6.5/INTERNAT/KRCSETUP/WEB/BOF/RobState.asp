<%@ Language=VBScript %>
<% 
	protocol="http://"
	path="/_ScriptLibrary"
	rsFile="/rs.htm"
	serverName=Request.ServerVariables("SERVER_NAME")
	
	urlRemoteScript=protocol+serverName+path+rsFile
	paramRemoteScript=protocol+serverName+path	
	aspToOCXInter=paramRemoteScript+"/Interfaces/WebRobot.asp"
%>
<html>
<head>
<title>Web Diagnosis-Main frame</title>
<script LANGUAGE="JavaScript">
  if(document.images) {
    pRed = new Image(27,27);
    pRed.src = "../Images/red_ball.gif";
    pGrey = new Image(27,27);
    pGrey.src = "../Images/grey_ball.gif";
  }

  function SetImage(imgDocID,bVal) {
  //   imgDocID   - Name of the Image object for exchange
  //   bVal - Value retured from grundsystem 
    if(document.images) 
    {
       if (bVal=="TRUE")
         document.images[imgDocID].src = pRed.src;
       else
         document.images[imgDocID].src = pGrey.src;
    }
  }


function SetFocus(){}

</script>
<script language="JavaScript" src="General_Main.js" type="text/javascript">
</script>
</head>

<body  onload="connectRob();" OnUnload="disConnectRob();" id="MainFrame" bgcolor="#ffffff" background="../Images/bgMain_gBOF.gif" >
<script language="JavaScript" src="<%Response.Write(urlRemoteScript)%>"></script>
<script language="JavaScript">RSEnableRemoteScripting("<%Response.Write(paramRemoteScript)%>");</script>
<font face="Arial" color="#ef8816">&nbsp;&nbsp;&nbsp; </font>
<big><big><strong><font face="Times New Roman" color="#000000">WEB DIAGNOSIS
</font></strong></big></big>


<form name="output">
<table border="0" width="442" height="350">
 <tr><td height="1">&nbsp;</td><td height="1">&nbsp;</td></tr>
 <tr><td width="13" height="346"></td>
 <td width="415" height="346" valign="top">
  <table border="0"  height="123" bordercolorlight="#ffff99" cellspacing="0"
   cellpadding="2" width="401" >
   <tr>
    <td width="395" height="33" bgcolor="#000040" colspan="2"><p align="center"><font
    color="#ffffff"><strong><big>ROBOT STATE INFO</big></strong></font></p></td>
   </tr>
   <tr bgcolor="blue" valign="bottom">
    <td width="341" height="33" bgcolor="#c0c0c0"><FONT 
      face=Arial><STRONG>Drives on
      </STRONG></FONT>
      </td>
    <td width="48" height="33" bgcolor="#c0c0c0" valign="bottom" bordercolor="#000000"> &nbsp;
      <img border="0" src="../Bilder/grey_ball.gif" name="PeriRdy" width="26" height="26"> 
    </td>
   </tr>
   <tr valign="bottom">
    <td width="341" height="33" bgcolor="#ececec"><FONT face=Arial><STRONG>Robot on path</STRONG></FONT>
    </td>
    <td width="48" height="33" bgcolor="#ececec" valign="bottom" bordercolor="#000000"> &nbsp;
      <img border="0" src="../Bilder/grey_ball.gif" name="OnPath" width="26" height="26"> 
    </td>
   </tr>
   <tr valign="bottom">
    <td width="341" height="34" bgcolor="#c0c0c0"><FONT 
      face=Arial><STRONG>Robot program activ</STRONG></FONT>
    </td>
    <td width="48" height="33" bgcolor="#c0c0c0" valign="bottom" bordercolor="#000000"> &nbsp;
      <img border="0" src="../Bilder/grey_ball.gif" name="ProAct" width="26" height="26"> 
    </td>
   </tr>
   <tr valign="bottom">
    <td width="341" height="36" bgcolor="#ececec"><FONT 
      face=Arial><STRONG>Robot in motion </STRONG></FONT>
    </td>
    <td width="48" height="33" bgcolor="#ececec" valign="bottom" bordercolor="#000000"> &nbsp;
       <img border="0" src="../Bilder/grey_ball.gif" name="ProMove" width="26" height="26"> 
    </td>
   </tr>
   <tr valign="bottom">
    <td width="341" height="34" bgcolor="#c0c0c0"><FONT 
      face=Arial><STRONG>Robot in home-position</STRONG></FONT>
    </td>
    <td width="48" height="33" bgcolor="#c0c0c0" valign="bottom" bordercolor="#000000"> &nbsp;
      <img border="0" src="../Bilder/grey_ball.gif" name="InHome" width="26" height="26"> 
    </td>
   </tr>
   <tr valign="bottom">
    <td width="341" height="36" bgcolor="#ececec"><FONT 
      face=Arial><STRONG>Stop message exists </STRONG></FONT>
    </td>
    <td width="48" height="33" bgcolor="#ececec" valign="bottom" bordercolor="#000000"> &nbsp;
      <img border="0" src="../Bilder/grey_ball.gif" name="StopMess" width="26" height="26"> 
    </td>
   </tr>
   <tr valign="bottom">
    <td width="341" height="34" bgcolor="#c0c0c0"><FONT 
      face=Arial><STRONG>Robot enable</STRONG></FONT>
    </td>
    <td width="48" height="33" bgcolor="#c0c0c0" valign="bottom" bordercolor="#000000"> &nbsp;
      <img border="0" src="../Bilder/grey_ball.gif" name="MoveEnable" width="26" height="26"> 
    </td>
   </tr>
  </table>

  <hr width="401" align="left">
  <table border="0" cellspacing="0" cellpadding="2" width="401">
   <tr valign="bottom">
    <td width="186" height="36" bgcolor="#c0c0c0"><FONT 
      face=Arial><STRONG>Operation Mode </STRONG></FONT>
    </td>
    <td width="59%" height="33" bgcolor="#c0c0c0"> 
      <p align="right"> <FONT  face=Arial><STRONG>:</STRONG></FONT> 
      <input  style="BORDER-RIGHT: rgb(192,192,192) groove; BORDER-TOP: medium none; 
          BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; BACKGROUND-COLOR: rgb(255,255,0)" 
          name="ModeOp" size="15"> 
      </p>
    </td>
   </tr>
  </table>

 </td>
</tr>
</table>
</form>
<script language="JavaScript">
var serverURL;
var failerFlag=false;
var errorMsg;

function readVar() {
	var serverRequest;
	var localFailFlag=false;
	serverRequest=serverURL.getVar("$PERI_RDY");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
        SetImage('PeriRdy',serverRequest.return_value);
		localFailFlag=true;
	}
	else
	   SetImage('PeriRdy',serverRequest.return_value);
		
	serverRequest=serverURL.getVar("$On_Path");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
        SetImage('OnPath',serverRequest.return_value);
		localFailFlag=true;
	}
	else	
	   SetImage('OnPath',serverRequest.return_value);
	
	serverRequest=serverURL.getVar("$PRO_ACT");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
        SetImage('ProAct',serverRequest.return_value);
		localFailFlag=true;
	}
	else	
	   SetImage('ProAct',serverRequest.return_value);
		
	serverRequest=serverURL.getVar("$Pro_MOVE");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
        SetImage('ProMove',serverRequest.return_value);
		localFailFlag=true;
	}
	else	
	   SetImage('ProMove',serverRequest.return_value);

	serverRequest=serverURL.getVar("$IN_HOME");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
        SetImage('InHome',serverRequest.return_value);
		localFailFlag=true;
	}
	else	
	   SetImage('InHome',serverRequest.return_value);
	
	serverRequest=serverURL.getVar("$StopMess");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
	    SetImage('StopMess',serverRequest.return_value);
		localFailFlag=true;
	}
	else	
	   SetImage('StopMess',serverRequest.return_value);
	
	serverRequest=serverURL.getVar("$Move_Enable");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
  	    SetImage('MoveEnable',serverRequest.return_value);
		localFailFlag=true;
	}
	else	
       SetImage('MoveEnable',serverRequest.return_value);
	
	serverRequest=serverURL.getVar("$MODE_OP");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
        document.output.ModeOp.value=errorMsg;
		localFailFlag=true;
	}
	else
        document.output.ModeOp.value=serverRequest.return_value;		

	if (localFailFlag) {
		alert("Remote-Aufruf fehlgeschlagen!");
		failureFlag=true;
	}
}

function connectRob() {
	var serverRequest;
	errorMsg="Could not read variable"
	serverURL=RSGetASPObject("<%Response.Write(aspToOCXInter)%>");
	serverRequest=serverURL.ConnectRob();
	readVar();
}

function disConnectRob() {
	if (!failerFlag)
		serverRequest=serverURL.DisconnectRob();
}
</script>

<p>&nbsp;</p>

<p>&nbsp;</p>
</body>
</html>
