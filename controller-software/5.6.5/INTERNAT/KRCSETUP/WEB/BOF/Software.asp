<%@ Language="JScript" %>
<HTML>
	<HEAD>
		<title>Web Diagnosis-Main frame</title>
		<% 
	protocol="http://"
	path="/_ScriptLibrary"
	rsFile="/rs.htm"
	serverName=Request.ServerVariables("SERVER_NAME")
	
	urlRemoteScript=protocol+serverName+path+rsFile
	paramRemoteScript=protocol+serverName+path	
	aspToOCXInter=paramRemoteScript+"/Interfaces/WebRobot.asp"
%>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="General_Main.js" type="text/javascript">
		</script>
		<script language="javascript" id="clientEventHandlersJS">
<!--

function ShowDetails(strDetails) {
document.output.Textarea1.value = strDetails
}

function SetFocus()
{
	document.output.KRCVersion.focus();
}



//-->
		</script>
	</HEAD>
	<body id="MainFrame" bgColor="#ffffff" background="../Images/bgMain_gBOF.gif" onload="connectRob();document.output.KRCVersion.focus();" onunload="disConnectRob();">
		<script language=JavaScript src="<%Response.Write(urlRemoteScript)%>"></script>
		<script language="JavaScript">RSEnableRemoteScripting("<%Response.Write(paramRemoteScript)%>");</script>
		<font face="Arial" color="#ef8816">&nbsp;&nbsp;&nbsp; </font><big><big><strong><font face="Times New Roman" color="#000000">
						WEB DIAGNOSIS </font></strong></big></big>
		<p></p>
		<form id="Form1" name="output">
			<TABLE id="Table1" style="WIDTH: 420px; HEIGHT: 336px" width="420" border="0">
				<tr>
					<td width="13"></td>
					<td>
						<table id="Table2" style="WIDTH: 392px; HEIGHT: 192px" height="192" cellSpacing="0" cellPadding="2" borderColorLight="#ffff99" border="0">
							<tr>
								<td width="100%" bgColor="#000040" colSpan="2" height="16%">
									<p align="center"><font color="#ffffff"><strong><big>SOFTWARE INFO</big></strong></font></p>
								</td>
							</tr>
							<tr vAlign="bottom" bgColor="blue">
								<td width="41%" bgColor="#c0c0c0" height="12%"><FONT face="Arial"><STRONG>KRC Version </STRONG>
									</FONT>
								</td>
								<td width="59%" bgColor="#c0c0c0" height="12%"><FONT face="Arial"><STRONG>:</STRONG></FONT>
									<input id="Text1" style="BORDER-RIGHT: rgb(192,192,192) groove; BORDER-TOP: medium none; BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; BACKGROUND-COLOR: rgb(255,255,0)" onfocus="return ShowDetails(document.output.KRCVersion.value)" name="KRCVersion">
								</td>
							</tr>
							<tr vAlign="bottom">
								<td width="41%" bgColor="#ececec" height="12%"><FONT face="Arial"><STRONG>Version</STRONG></FONT>
								</td>
								<td width="59%" bgColor="#ececec" height="12%"><FONT face="Arial"><STRONG>:</STRONG></FONT>
									<input id="Text2" style="BORDER-RIGHT: rgb(192,192,192) groove; BORDER-TOP: rgb(192,192,192); BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; BACKGROUND-COLOR: rgb(255,255,0)" onfocus="return ShowDetails(document.output.Version.value)" name="Version">
								</td>
							</tr>
							<tr vAlign="bottom">
								<td width="41%" bgColor="#c0c0c0" height="12%"><FONT face="Arial"><STRONG>Kernel Version</STRONG></FONT>
								</td>
								<td width="59%" bgColor="#c0c0c0" height="12%"><FONT face="Arial"><STRONG>:</STRONG></FONT>
									<input id="Text3" style="BORDER-RIGHT: rgb(192,192,192) groove; BORDER-TOP: medium none; BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; BACKGROUND-COLOR: rgb(255,255,0)" onfocus="return ShowDetails(document.output.GSVer.value)" name="GSVer">
								</td>
							</tr>
							<tr vAlign="bottom">
								<td width="41%" bgColor="#ececec" height="12%"><FONT face="Arial"><STRONG>BOF Version </STRONG>
									</FONT>
								</td>
								<td width="59%" bgColor="#ececec" height="12%"><FONT face="Arial"><STRONG>:</STRONG></FONT>
									<input id="Text4" style="BORDER-RIGHT: rgb(192,192,192) groove; BORDER-TOP: medium none; BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; BACKGROUND-COLOR: rgb(255,255,0)" onfocus="return ShowDetails(document.output.BOFVer.value)" name="BOFVer">
								</td>
							</tr>
							<tr vAlign="bottom">
								<td width="41%" bgColor="#c0c0c0" height="12%"><FONT face="Arial"><STRONG>R1Mada Version</STRONG></FONT>
								</td>
								<td width="59%" bgColor="#c0c0c0" height="12%"><FONT face="Arial"><STRONG>:</STRONG></FONT>
									<input id="Text5" style="BORDER-RIGHT: rgb(192,192,192) groove; BORDER-TOP: medium none; BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; BACKGROUND-COLOR: rgb(255,255,0)" onfocus="return ShowDetails(document.output.R1MadaVer.value)" name="R1MadaVer">
								</td>
							</tr>
							<tr vAlign="bottom">
								<td width="41%" bgColor="#ececec" height="12%"><FONT face="Arial"><STRONG>SteuMada Version </STRONG>
									</FONT>
								</td>
								<td width="59%" bgColor="#ececec" height="12%"><FONT face="Arial"><STRONG>:</STRONG></FONT>
									<input id="Text6" style="BORDER-RIGHT: rgb(192,192,192) groove; BORDER-TOP: medium none; BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; BACKGROUND-COLOR: rgb(255,255,0)" onfocus="return ShowDetails(document.output.SteuMadaVer.value)" name="SteuMadaVer">
								</td>
							</tr>
						</table>
						<TEXTAREA id="Textarea1" title="" style="FONT-SIZE: x-small; WIDTH: 392px; BORDER-TOP-STYLE: none; FONT-FAMILY: Arial; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 50px; BACKGROUND-COLOR: silver; BORDER-BOTTOM-STYLE: none" name="Textarea1" rows="3" readOnly cols="46" defaultvalue="Test"></TEXTAREA>
					</td>
				</tr>
			</TABLE>
		</form>
		<script language="JavaScript">
var serverURL;
var failerFlag=false;
var errorMsg;

function readVar() {
	var serverRequest;
	var localFailFlag=false;
	serverRequest=serverURL.getVar("$RCV_INFO[]");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
		document.output.GSVer.value=errorMsg;
		localFailFlag=true;
	}
	else	
		document.output.GSVer.value=serverRequest.return_value;
		
	serverRequest=serverURL.getVar("$V_R1MADA[]");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
		document.output.R1MadaVer.value=errorMsg;
		localFailFlag=true;
	}
	else	
		document.output.R1MadaVer.value=serverRequest.return_value;
	
	serverRequest=serverURL.getVar("$V_STEUMADA[]");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
		document.output.SteuMadaVer.value=errorMsg;
		localFailFlag=true;
	}
	else	
		document.output.SteuMadaVer.value=serverRequest.return_value;
		
	serverRequest=serverURL.GetBOFVer();
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
		document.output.BOFVer.value=errorMsg;
		localFailFlag=true;
	}
	else	
		document.output.BOFVer.value=serverRequest.return_value;
		
	serverRequest=serverURL.ReadRegVal("Version/KRCVersion");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
		document.output.KRCVersion.value=errorMsg;
		localFailFlag=true;
	}
	else	
		document.output.KRCVersion.value=serverRequest.return_value;

	serverRequest=serverURL.ReadRegVal("Version/Version");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
		document.output.Version.value=errorMsg;
		localFailFlag=true;
	}
	else	
		document.output.Version.value=serverRequest.return_value;
	
	if (localFailFlag) {
		alert("Remote-Aufruf fehlgeschlagen!");
		failureFlag=true;
	}
}

function connectRob() {
	var serverRequest;
	errorMsg="Could not read variable"
	serverURL=RSGetASPObject("<%Response.Write(aspToOCXInter)%>");
	serverRequest=serverURL.ConnectRob();
	readVar();
}

function disConnectRob() {
	if (!failerFlag)
		serverRequest=serverURL.DisconnectRob();
}
		</script>
	</body>
</HTML>
