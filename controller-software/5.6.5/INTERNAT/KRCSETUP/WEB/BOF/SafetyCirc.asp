<%@ Language=VBScript %>
<% 
	protocol="http://"
	path="/_ScriptLibrary"
	rsFile="/rs.htm"
	serverName=Request.ServerVariables("SERVER_NAME")
	
	urlRemoteScript=protocol+serverName+path+rsFile
	paramRemoteScript=protocol+serverName+path	
	aspToOCXInter=paramRemoteScript+"/Interfaces/WebRobot.asp"
%>
<html>
<head>
<title>Web Diagnosis-Main frame</title>
<script LANGUAGE="JavaScript">
  if(document.images) {
    pRed = new Image(27,27);
    pRed.src = "../Images/red_ball.gif";
    pGrey = new Image(27,27);
    pGrey.src = "../Images/grey_ball.gif";
  }

  function SetImage(imgDocID,bVal) {
  //   imgDocID   - Name of the Image object for exchange
  //   bVal - Value retured from grundsystem 
    if(document.images) 
    {
       if (bVal=="TRUE")
         document.images[imgDocID].src = pRed.src;
       else
         document.images[imgDocID].src = pGrey.src;
    }
  }


function SetFocus(){}

</script>
<script language="JavaScript" src="General_Main.js" type="text/javascript">
</script>
</head>

<body  onload="connectRob();" OnUnload="disConnectRob();" id="MainFrame" bgcolor="#ffffff" background="../Images/bgMain_gBOF.gif" >
<script language="JavaScript" src="<%Response.Write(urlRemoteScript)%>"></script>
<script language="JavaScript">RSEnableRemoteScripting("<%Response.Write(paramRemoteScript)%>");</script>
<font face="Arial" color="#ef8816">&nbsp;&nbsp;&nbsp; </font>
<big><big><strong><font face="Times New Roman" color="#000000">WEB DIAGNOSIS
</font></strong></big></big>


<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>
<form name="output">
<table border="0" width="442">
 <tr><td width="13"></td>
 <td width="415">
  <table border="0"  height="123" bordercolorlight="#ffff99" cellspacing="0"
   cellpadding="2" width="401" >
   <tr>
    <td width="395" height="33" bgcolor="#000040" colspan="2"><p align="center"><font
    color="#ffffff"><strong><big>SAFETY CIRCUIT INFO</big></strong></font></p></td>
   </tr>
   <tr bgcolor="blue" valign="bottom">
    <td width="186" height="33" bgcolor="#ececec"><FONT 
      face=Arial><STRONG>Alarm Stop is active
      </STRONG></FONT>
      </td>
    <td width="203" height="33" bgcolor="#ececec" valign="bottom" bordercolor="#000000" align="right"> &nbsp;&nbsp;&nbsp;&nbsp;
      <img border="0" src="../Bilder/grey_ball.gif" name="AlarmStop" width="26" height="26"> 
    </td>
   </tr>
   <tr valign="bottom">
    <td width="186" height="33" bgcolor="#c0c0c0"><FONT face=Arial><STRONG>User safety is on</STRONG></FONT>
    </td>
    <td width="203" height="33" bgcolor="#c0c0c0" valign="bottom" bordercolor="#000000" align="right"> &nbsp;
      <img border="0" src="../Bilder/grey_ball.gif" name="UserSaf" width="26" height="26"> 
    </td>
   </tr>
   <tr valign="bottom">
    <td width="186" height="34" bgcolor="#ececec"><FONT 
      face=Arial><STRONG>Drives are on</STRONG></FONT>
    </td>
    <td width="203" height="33" bgcolor="#ececec" valign="bottom" bordercolor="#000000" align="right"> &nbsp;
      <img border="0" src="../Bilder/grey_ball.gif" name="PeriRdy" width="26" height="26"> 
    </td>
   </tr>
   <tr valign="bottom">
    <td width="186" height="36" bgcolor="#c0c0c0"><FONT 
      face=Arial><STRONG>Stop message exists </STRONG></FONT>
    </td>
    <td width="203" height="33" bgcolor="#c0c0c0" valign="bottom" bordercolor="#000000" align="right"> &nbsp;
      <img border="0" src="../Bilder/grey_ball.gif" name="StopMess" width="26" height="26"> 
    </td>
   </tr>
   <tr valign="bottom">
    <td width="186" height="34" bgcolor="#ececec"><FONT 
      face=Arial><STRONG>Robot motion free</STRONG></FONT>
    </td>
    <td width="203" height="33" bgcolor="#ececec" valign="bottom" bordercolor="#000000" align="right"> &nbsp;
      <img border="0" src="../Bilder/grey_ball.gif" name="MoveEnable" width="26" height="26"> 
    </td>
   </tr>
  </table><hr width="401" align="left">
  <table border="0" cellspacing="0" cellpadding="2" width="401">

   <tr valign="bottom">
    <td width="186" height="36" bgcolor="#c0c0c0"><FONT 
      face=Arial><STRONG>Operation Mode </STRONG></FONT>
    </td>
    <td width="59%" height="33" bgcolor="#c0c0c0"> 
      <p align="right"> <FONT  face=Arial><STRONG>:</STRONG></FONT> 
      <input  style="BORDER-RIGHT: rgb(192,192,192) groove; BORDER-TOP: medium none; 
          BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; BACKGROUND-COLOR: rgb(255,255,0)" 
          name="ModeOp" size="15"> 
      </p>
    </td>
   </tr>

  </table>
 </td>
</tr>
</table>
</form>
<script language="JavaScript">
var serverURL;
var failerFlag=false;
var errorMsg;

function readVar() {
	var serverRequest;
	var localFailFlag=false;
	serverRequest=serverURL.getVar("$MODE_OP");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
        document.output.ModeOp.value=errorMsg;
		localFailFlag=true;
	}
	else
        document.output.ModeOp.value=serverRequest.return_value;		
	
	serverRequest=serverURL.getVar("$ALARM_STOP");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
        SetImage('AlarmStop',serverRequest.return_value);
		localFailFlag=true;
	}
	else	
	   SetImage('AlarmStop',serverRequest.return_value);

	serverRequest=serverURL.getVar("$USER_SAF");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
        SetImage('UserSaf',serverRequest.return_value);
		localFailFlag=true;
	}
	else	
	   SetImage('UserSaf',serverRequest.return_value);
	
	serverRequest=serverURL.getVar("$PERI_RDY");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
        SetImage('PeriRdy',serverRequest.return_value);
		localFailFlag=true;
	}
	else	
	   SetImage('PeriRdy',serverRequest.return_value);
		
	
	serverRequest=serverURL.getVar("$StopMess");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
	    SetImage('StopMess',serverRequest.return_value);
		localFailFlag=true;
	}
	else	
	   SetImage('StopMess',serverRequest.return_value);
	
	serverRequest=serverURL.getVar("$Move_Enable");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
  	    SetImage('MoveEnable',serverRequest.return_value);
		localFailFlag=true;
	}
	else	
	   SetImage('MoveEnable',serverRequest.return_value);

	if (localFailFlag) {
		alert("Remote-Aufruf fehlgeschlagen!");
		failureFlag=true;
	}
}

function connectRob() {
	var serverRequest;
	errorMsg="Could not read variable"
	serverURL=RSGetASPObject("<%Response.Write(aspToOCXInter)%>");
	serverRequest=serverURL.ConnectRob();
	readVar();
}

function disConnectRob() {
	if (!failerFlag)
		serverRequest=serverURL.DisconnectRob();
}
</script>

<p>&nbsp;</p>

<p>&nbsp;</p>
</body>
</html>
