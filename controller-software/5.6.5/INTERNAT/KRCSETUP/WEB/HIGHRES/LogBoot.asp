<%@ Language=VBScript %>
<% 
	protocol="http://"
	path="/_ScriptLibrary"
	rsFile="/rs.htm"
	serverName=Request.ServerVariables("SERVER_NAME")
	
	urlRemoteScript=protocol+serverName+path+rsFile
	paramRemoteScript=protocol+serverName+path	
	aspToOCXInter=paramRemoteScript+"/Interfaces/WebRobot.asp"
%>
<html>
<head>
<title>Web Diagnosis-Main frame</title>
<script LANGUAGE="JavaScript">
  if(document.images) {
    pError = new Image(20,20);
    pError.src = "../Images/boot_err.jpg";
    pWarn = new Image(20,20);
    pWarn.src = "../Images/boot_warn.jpg";
    pInfo = new Image(20,20);
    pInfo.src = "../Images/boot_info.jpg";
    pNotRead = new Image(20,20);
    pNotRead.src = "../Images/grey_ball.gif";
  }

  function SetImage(imgDocID,bVal) {
  //   imgDocID   - Name of the Image object for exchange
  //   bVal - Value retured from grundsystem 
    if(document.images) 
    {
       if (bVal=="1")
         document.images[imgDocID].src = pWarn.src;
       else if (bVal=="2")
         document.images[imgDocID].src = pWarn.src;
       else if (bVal=="error")
         document.images[imgDocID].src = pNotRead.src;
       else
         document.images[imgDocID].src = pError.src;
    }
  }

</script>
<script language="JavaScript" src="General_Main.js" type="text/javascript">
</script>
</head>

<body onload="connectRob();" OnUnload="disConnectRob();" id="MainFrame" bgcolor="#ffffff" background="../Images/bgMain_gHIGH.gif" >
<script language="JavaScript" src="<%Response.Write(urlRemoteScript)%>"></script>
<script language="JavaScript">RSEnableRemoteScripting("<%Response.Write(paramRemoteScript)%>");</script>
<font face="Arial" color="#ef8816">&nbsp;&nbsp;&nbsp; </font>
<big><big><strong><font face="Times New Roman" color="#000000">WEB DIAGNOSIS
</font></strong></big></big>

<hr color="white">
<form name="output">
<table border="0" width="520" height="500">
 <tr height="20"><td height="20" width="34"></td><td height="20" width="472">&nbsp;</td></tr>
 <tr><td width="34" height="346"></td>
 <td width="472" height="346" valign="top">
  <table  border="0"  height="123" bordercolorlight="#ffff99" cellspacing="0"
   cellpadding="2" width="462" >
   <tr>
    <td width="461" height="33" bgcolor="#000040" colspan="3"><p align="center"><font color="#ffffff"><strong><big>BOOT LOG
      INFO</big></strong></font></p></td>
   </tr>
   <tr valign="center">
    <td width="36" height="25" bgcolor="#c0c0c0" valign="center">
     <b>1.</b>
    </td>
    <td width="391" height="25" bgcolor="#c0c0c0" valign="center">      
      <input  style="BORDER-RIGHT: rgb(192,192,192) groove; BORDER-TOP: medium none; 
          BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; BACKGROUND-COLOR: rgb(255,255,0)" 
          name="Item0" size="50"> 
    </td>
    <td width="44" height="25" bgcolor="#c0c0c0" bordercolor="#000000" valign="center">
      <img border="0" src="../Images/grey_ball.gif" name="Image0" width="20" height="20"> 
    </td>
   </tr>
   <tr valign="center">
    <td width="36" height="25" bgcolor="#ececec" valign="center">
     <b>2.</b>
    </td>
    <td width="391" height="25" bgcolor="#ececec" valign="center">      
      <input  style="BORDER-RIGHT: rgb(192,192,192) groove; BORDER-TOP: medium none; 
          BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; BACKGROUND-COLOR: rgb(255,255,0)" 
          name="Item1" size="50"> 
    </td>
    <td width="44" height="25" bgcolor="#ececec" bordercolor="#000000" valign="center">
      <img border="0" src="../Images/grey_ball.gif" name="Image1" width="20" height="20"> 
    </td>
   </tr>
   <tr valign="center">
    <td width="36" height="25" bgcolor="#c0c0c0" valign="center">
     <b>3.</b>
    </td>
    <td width="391" height="25" bgcolor="#c0c0c0" valign="center">      
      <input  style="BORDER-RIGHT: rgb(192,192,192) groove; BORDER-TOP: medium none; 
          BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; BACKGROUND-COLOR: rgb(255,255,0)" 
          name="Item2" size="50"> 
    </td>
    <td width="44" height="25" bgcolor="#c0c0c0" bordercolor="#000000" valign="center">
      <img border="0" src="../Images/grey_ball.gif" name="Image2" width="20" height="20"> 
    </td>
   </tr>
   <tr valign="center">
    <td width="36" height="25" bgcolor="#ececec" valign="center">
     <b>4.</b>
    </td>
    <td width="391" height="25" bgcolor="#ececec" valign="center">      
      <input  style="BORDER-RIGHT: rgb(192,192,192) groove; BORDER-TOP: medium none; 
          BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; BACKGROUND-COLOR: rgb(255,255,0)" 
          name="Item3" size="50"> 
    </td>
    <td width="44" height="25" bgcolor="#ececec" bordercolor="#000000" valign="center">
      <img border="0" src="../Images/grey_ball.gif" name="Image3" width="20" height="20"> 
    </td>
   </tr>
   <tr valign="center">
    <td width="36" height="25" bgcolor="#c0c0c0" valign="center">
     <b>5.</b>
    </td>
    <td width="391" height="25" bgcolor="#c0c0c0" valign="center">      
      <input  style="BORDER-RIGHT: rgb(192,192,192) groove; BORDER-TOP: medium none; 
          BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; BACKGROUND-COLOR: rgb(255,255,0)" 
          name="Item4" size="50"> 
    </td>
    <td width="44" height="25" bgcolor="#c0c0c0" bordercolor="#000000" valign="center">
      <img border="0" src="../Images/grey_ball.gif" name="Image4" width="20" height="20"> 
    </td>
   </tr>
   <tr valign="center">
    <td width="36" height="25" bgcolor="#ececec" valign="center">
     <b>6.</b>
    </td>
    <td width="391" height="25" bgcolor="#ececec" valign="center">      
      <input  style="BORDER-RIGHT: rgb(192,192,192) groove; BORDER-TOP: medium none; 
          BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; BACKGROUND-COLOR: rgb(255,255,0)" 
          name="Item5" size="50"> 
    </td>
    <td width="44" height="25" bgcolor="#ececec" bordercolor="#000000" valign="center">
      <img border="0" src="../Images/grey_ball.gif" name="Image5" width="20" height="20"> 
    </td>
   </tr>
   <tr valign="center">
    <td width="36" height="25" bgcolor="#c0c0c0" valign="center">
     <b>7.</b>
    </td>
    <td width="391" height="25" bgcolor="#c0c0c0" valign="center">      
      <input  style="BORDER-RIGHT: rgb(192,192,192) groove; BORDER-TOP: medium none; 
          BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; BACKGROUND-COLOR: rgb(255,255,0)" 
          name="Item6" size="50"> 
    </td>
    <td width="44" height="25" bgcolor="#c0c0c0" bordercolor="#000000" valign="center">
      <img border="0" src="../Images/grey_ball.gif" name="Image6" width="20" height="20"> 
    </td>
   </tr>
   <tr valign="center">
    <td width="36" height="25" bgcolor="#ececec" valign="center">
     <b>8.</b>
    </td>
    <td width="391" height="25" bgcolor="#ececec" valign="center">      
      <input  style="BORDER-RIGHT: rgb(192,192,192) groove; BORDER-TOP: medium none; 
          BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; BACKGROUND-COLOR: rgb(255,255,0)" 
          name="Item7" size="50"> 
    </td>
    <td width="44" height="25" bgcolor="#ececec" bordercolor="#000000" valign="center">
      <img border="0" src="../Images/grey_ball.gif" name="Image7" width="20" height="20"> 
    </td>
   </tr>
   <tr valign="center">
    <td width="36" height="25" bgcolor="#c0c0c0" valign="center">
     <b>9.</b>
    </td>
    <td width="391" height="25" bgcolor="#c0c0c0" valign="center">      
      <input  style="BORDER-RIGHT: rgb(192,192,192) groove; BORDER-TOP: medium none; 
          BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; BACKGROUND-COLOR: rgb(255,255,0)" 
          name="Item8" size="50"> 
    </td>
    <td width="44" height="25" bgcolor="#c0c0c0" bordercolor="#000000" valign="center">
      <img border="0" src="../Images/grey_ball.gif" name="Image8" width="20" height="20"> 
    </td>
   </tr>
   <tr valign="center">
    <td width="36" height="25" bgcolor="#ececec" valign="center">
     <b>10.</b>
    </td>
    <td width="391" height="25" bgcolor="#ececec" valign="center">      
      <input  style="BORDER-RIGHT: rgb(192,192,192) groove; BORDER-TOP: medium none; 
          BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; BACKGROUND-COLOR: rgb(255,255,0)" 
          name="Item9" size="50"> 
    </td>
    <td width="44" height="25" bgcolor="#ececec" bordercolor="#000000" valign="center">
      <img border="0" src="../Images/grey_ball.gif" name="Image9" width="20" height="20"> 
    </td>
   </tr>
   <tr>
     <td width="36" height="25" align="right">
     <b></b>
     </td>
     <td width="391" height="20"  align="center" valign="bottom">
        <font face="Times New Roman" color="#000000" size="3">
         <img border="0" src="../Images/boot_err.jpg" name="Image10" width="20" height="20">:Stop &nbsp; &nbsp; 
         <img border="0" src="../Images/boot_info.jpg" name="Image11" width="20" height="20">:Info&nbsp; &nbsp; 
         <img border="0" src="../Images/boot_warn.jpg" name="Image12" width="20" height="20">:Warning &nbsp; &nbsp; 
        </font> 
     </td>
     <td width="49" height="33" valign="top" bordercolor="#000000"> &nbsp;
     </td>
   </tr>
  </table>

 </td>
</tr>
</table>
</form>
<script language="JavaScript">
var serverURL;
var failerFlag=false;
var errorMsg;
var strLogResult=new String("temp");
var strText=" =LIC= ";
var strType=" =LIT= ";

function GetItemText(){
	var nPos;
	var strValue;
	
	nPos=strLogResult.search(strText);
	if (nPos>-1){
       strValue=strLogResult.slice(0,nPos);
	   strLogResult=strLogResult.slice(nPos+strText.length);
	}
	else
	   strValue="";
	if (strValue=="") 
		strValue="    ---- No more log items----";
	return strValue;
}

function GetItemType(){
	var nPos;
	var strValue;
	nPos=strLogResult.search(strType);
	if (nPos>-1){
   	   strValue=strLogResult.slice(0,nPos);
	   strLogResult=strLogResult.slice(nPos+strType.length);
	}
	else
	   strValue="error";
	return strValue;
}

function readVar() {
	var serverRequest;
	var localFailFlag=false;
	serverRequest=serverURL.GetLogInfo(263);
    if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
        document.output.Item0.value="No Log Items Available";
        //SetImage('PeriRdy',serverRequest.return_value);
		localFailFlag=true;
	}
	else{
		strLogResult=serverRequest.return_value;

		document.output.Item0.value=GetItemText();
		SetImage('Image0',GetItemType());

		document.output.Item1.value=GetItemText();
		SetImage('Image1',GetItemType());

		document.output.Item2.value=GetItemText();
		SetImage('Image2',GetItemType());

		document.output.Item3.value=GetItemText();
		SetImage('Image3',GetItemType());

		document.output.Item4.value=GetItemText();
		SetImage('Image4',GetItemType());

		document.output.Item5.value=GetItemText();
		SetImage('Image5',GetItemType());

		document.output.Item6.value=GetItemText();
		SetImage('Image6',GetItemType());

		document.output.Item7.value=GetItemText();
		SetImage('Image7',GetItemType());

		document.output.Item8.value=GetItemText();
		SetImage('Image8',GetItemType());

		document.output.Item9.value=GetItemText();
		SetImage('Image9',GetItemType());
	}
//	if (localFailFlag) {
//		alert("Remote-Aufruf fehlgeschlagen!");
//		failureFlag=true;
//	}
}

function connectRob() {
	var serverRequest;
	errorMsg="Could not read variable";
	serverURL=RSGetASPObject("<%Response.Write(aspToOCXInter)%>");
	serverRequest=serverURL.ConnectRob();
	readVar();
}

function disConnectRob() {
	if (!failerFlag)
		serverRequest=serverURL.DisconnectRob();
}
</script>

<p>&nbsp;</p>

<p>&nbsp;</p>
</body>
</html>
