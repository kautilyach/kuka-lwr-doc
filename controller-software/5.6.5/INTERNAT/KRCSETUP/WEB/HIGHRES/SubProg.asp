<%@ Language=VBScript %>
<% 
	protocol="http://"
	path="/_ScriptLibrary"
	rsFile="/rs.htm"
	serverName=Request.ServerVariables("SERVER_NAME")
	
	urlRemoteScript=protocol+serverName+path+rsFile
	paramRemoteScript=protocol+serverName+path	
	aspToOCXInter=paramRemoteScript+"/Interfaces/WebRobot.asp"
%>
<html>
<head>
<title>Web Diagnosis-Main frame</title>
<script language="JavaScript" src="General_Main.js" type="text/javascript">
</script>
</head>

<body  onload="connectRob();" OnUnload="disConnectRob();" id="MainFrame" bgcolor="#ffffff" background="../Images/bgMain_gHIGH.gif" >
<script language="JavaScript" src="<%Response.Write(urlRemoteScript)%>"></script>
<script language="JavaScript">RSEnableRemoteScripting("<%Response.Write(paramRemoteScript)%>");</script>
<font face="Arial" color="#ef8816">&nbsp;&nbsp;&nbsp; </font>
<big><big><strong><font face="Times New Roman" color="#000000">WEB DIAGNOSIS
</font></strong></big></big>


<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>


<p>&nbsp; </p>
<form name="output">
<table border="0" width="442">
 <tr><td width="69">&nbsp;&nbsp;&nbsp;&nbsp;</td>
 <td width="359">
  <table border="0"  height="123" bordercolorlight="#ffff99" cellspacing="0"
   cellpadding="2" width="357" >
   <tr>
    <td width="351" height="33" bgcolor="#000040" colspan="2"><p align="center"><font
    color="#ffffff" size="5"><strong>SUBMIT PROGRAM INFO</strong></font></p></td>
   </tr>
   <tr bgcolor="blue" valign="bottom">
    <td width="167" height="33" bgcolor="#c0c0c0"><FONT 
      face=Arial>Program name</FONT>
    </td>
    <td width="178" height="33" bgcolor="#c0c0c0"> <FONT  face=Arial><STRONG>:</STRONG></FONT> 
      <input  style="BORDER-RIGHT: rgb(192,192,192) groove; BORDER-TOP: medium none; 
          BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; BACKGROUND-COLOR: rgb(255,255,0)" 
          name="ProName"> 
    </td>
   </tr>
   <tr valign="bottom">
    <td width="167" height="34" bgcolor="#ececec"><FONT face=Arial>Program State</FONT>
    </td>
    <td width="178" height="34" bgcolor="#ececec"><FONT  face=Arial><STRONG>:</STRONG></FONT> 
      <input style ="BORDER-RIGHT:rgb(192,192,192) groove; 
        BORDER-TOP: medium none; BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; 
        BACKGROUND-COLOR: rgb(255,255,0)" name="ProState">
    </td>
   </tr>
  </table>
 </td>
</tr>
</table>
</form>
<script language="JavaScript">
var serverURL;
var failerFlag=false;
var errorMsg;

function readVar() {
	var serverRequest;
	var localFailFlag=false;
	serverRequest=serverURL.getVar("$PRO_NAME0[]");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
		document.output.ProName.value=errorMsg;
		localFailFlag=true;
	}
	else{
	    if (serverRequest.return_value=="")
	       document.output.ProName.value="No program is selected";
	    else
		   document.output.ProName.value=serverRequest.return_value;
	}	
	
	serverRequest=serverURL.getVar("$Pro_State0");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
		document.output.ProState.value=errorMsg;
		localFailFlag=true;
	}
	else	
		document.output.ProState.value=serverRequest.return_value;
		
	
	if (localFailFlag) {
		alert("Remote-Aufruf fehlgeschlagen!");
		failureFlag=true;
	}
}

function connectRob() {
	var serverRequest;
	errorMsg="Could not read variable"
	serverURL=RSGetASPObject("<%Response.Write(aspToOCXInter)%>");
	serverRequest=serverURL.ConnectRob();
	readVar();
}

function disConnectRob() {
	if (!failerFlag)
		serverRequest=serverURL.DisconnectRob();
}
</script>

<p>&nbsp;</p>

<p>&nbsp;</p>
</body>
</html>
