<%@ Language=VBScript %>
<HTML>
	<HEAD>
		<title>Web Diagnosis-Main frame</title>
		<% 
	protocol="http://"
	path="/_ScriptLibrary"
	rsFile="/rs.htm"
	serverName=Request.ServerVariables("SERVER_NAME")
	
	urlRemoteScript=protocol+serverName+path+rsFile
	paramRemoteScript=protocol+serverName+path	
	aspToOCXInter=paramRemoteScript+"/Interfaces/WebRobot.asp"
%>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" src="General_Main.js" type="text/javascript">
		</script>
		<script language="javascript" id="clientEventHandlersJS">
<!--

function ShowDetails(strDetails) {
document.output.Textarea1.value = strDetails
}


//-->
		</script>
	</HEAD>
	<body onload="connectRob();document.output.RobType.focus()" OnUnload="disConnectRob();" id="MainFrame" bgcolor="#ffffff" background="../Images/bgMain_gHIGH.gif">
		<script language="JavaScript" src="<%Response.Write(urlRemoteScript)%>"></script>
		<script language="JavaScript">RSEnableRemoteScripting("<%Response.Write(paramRemoteScript)%>");</script>
		<font face="Arial" color="#ef8816">&nbsp;&nbsp;&nbsp; </font><big><big><strong><font face="Times New Roman" color="#000000">
						WEB DIAGNOSIS </font></strong></big></big>
		<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</p>
		<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</p>
		<p>&nbsp;&nbsp;&nbsp;&nbsp;
		</p>
		<form name="output">
			<table border="0" width="431">
				<tr>
					<td width="13">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td width="404">
						<table border="0" height="123" bordercolorlight="#ffff99" cellspacing="0" cellpadding="2" width="461">
							<tr>
								<td width="443" height="33" bgcolor="#000040" colspan="2"><p align="center"><font color="#ffffff" size="5"><strong>MECHANICAL 
												INFO</strong></font></p>
								</td>
							</tr>
							<tr bgcolor="blue" valign="bottom">
								<td width="226" height="33" bgcolor="#c0c0c0"><font face="Arial" size="3">Robot Type</font>
								</td>
								<td width="223" height="33" bgcolor="#c0c0c0">
									<FONT face="Arial"><STRONG>:</STRONG></FONT> <input style="BORDER-RIGHT: rgb(192,192,192) groove; BORDER-TOP: medium none; BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; BACKGROUND-COLOR: rgb(255,255,0)" name="RobType" size="24" onfocus="return ShowDetails(document.output.RobType.value)">
								</td>
							</tr>
							<tr valign="bottom">
								<td width="226" height="33" bgcolor="#ececec"><font face="Arial" size="3">Ex. Axis 
										Number</font>
								</td>
								<td width="223" height="33" bgcolor="#ececec">
									<FONT face="Arial"><STRONG>:</STRONG></FONT> <input style="BORDER-RIGHT: rgb(192,192,192) groove; BORDER-TOP: rgb(192,192,192); BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; BACKGROUND-COLOR: rgb(255,255,0)" name="ExAxNum" size="24" onfocus="return ShowDetails(document.output.ExAxNum.value)">
								</td>
							</tr>
							<tr valign="bottom">
								<td width="226" height="34" bgcolor="#c0c0c0"><font face="Arial" size="3">Serial Number 
										of Robot</font>
								</td>
								<td width="223" height="34" bgcolor="#c0c0c0"><FONT face="Arial"><STRONG>:</STRONG></FONT>
									<input style="BORDER-RIGHT:rgb(192,192,192) groove; BORDER-TOP:medium none; BORDER-LEFT:medium none; BORDER-BOTTOM:rgb(192,192,192) groove; BACKGROUND-COLOR:rgb(255,255,0)" name="SerialNo" size="24" onfocus="return ShowDetails(document.output.SerialNo.value)">
								</td>
							</tr>
							<tr valign="bottom">
								<td width="226" height="36" bgcolor="#ececec"><font face="Arial" size="3">BIOS Version</font>
								</td>
								<td width="223" height="36" bgcolor="#ececec"><FONT face="Arial"><STRONG>:</STRONG></FONT>
									<input style="BORDER-RIGHT:rgb(192,192,192) groove; BORDER-TOP:medium none; BORDER-LEFT:medium none; BORDER-BOTTOM:rgb(192,192,192) groove; BACKGROUND-COLOR:rgb(255,255,0)" name="BIOSVer" size="24" onfocus="return ShowDetails(document.output.BIOSVer.value)">
								</td>
							</tr>
						</table>
						<TEXTAREA id="Textarea1" title="" style="FONT-SIZE: x-small; WIDTH: 461px; BORDER-TOP-STYLE: none; FONT-FAMILY: Arial; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 50px; BACKGROUND-COLOR: silver; BORDER-BOTTOM-STYLE: none" name="Textarea1" rows="3" readOnly cols="55" defaultvalue="Test"></TEXTAREA>
					</td>
				</tr>
			</table>
		</form>
		<script language="JavaScript">
var serverURL;
var failerFlag=false;
var errorMsg;

function readVar() {
	var serverRequest;
	var localFailFlag=false;
	serverRequest=serverURL.getVar("$TRAFONAME[]");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
		document.output.RobType.value=errorMsg;
		localFailFlag=true;
	}
	else	
		document.output.RobType.value=serverRequest.return_value;
		
	serverRequest=serverURL.getVar("$EX_AX_NUM");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
		document.output.ExAxNum.value=errorMsg;
		localFailFlag=true;
	}
	else	
		document.output.ExAxNum.value=serverRequest.return_value;
	
	serverRequest=serverURL.getVar("$KR_SERIALNO");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
		document.output.SerialNo.value=errorMsg;
		localFailFlag=true;
	}
	else	
		document.output.SerialNo.value=serverRequest.return_value;
		
	serverRequest=serverURL.getVar("$BIOS_VERSION[]");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
		document.output.BIOSVer.value=errorMsg;
		localFailFlag=true;
	}
	else	
		document.output.BIOSVer.value=serverRequest.return_value;
	
	if (localFailFlag) {
		alert("Remote-Aufruf fehlgeschlagen!");
		failureFlag=true;
	}
}

function connectRob() {
	var serverRequest;
	errorMsg="Could not read variable"
	serverURL=RSGetASPObject("<%Response.Write(aspToOCXInter)%>");
	serverRequest=serverURL.ConnectRob();
	readVar();
}

function disConnectRob() {
	if (!failerFlag)
		serverRequest=serverURL.DisconnectRob();
}
		</script>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
	</body>
</HTML>
