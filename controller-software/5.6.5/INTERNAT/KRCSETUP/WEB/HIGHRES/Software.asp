<%@ Language=VBScript %>
<HTML>
	<HEAD>
		<title>Web Diagnosis-Main frame</title>
		<% 
	protocol="http://"
	path="/_ScriptLibrary"
	rsFile="/rs.htm"
	serverName=Request.ServerVariables("SERVER_NAME")
	
	urlRemoteScript=protocol+serverName+path+rsFile
	paramRemoteScript=protocol+serverName+path	
	aspToOCXInter=paramRemoteScript+"/Interfaces/WebRobot.asp"
%>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" src="General_Main.js" type="text/javascript">
		</script>
		<script language="javascript" id="clientEventHandlersJS">
<!--

function ShowDetails(strDetails) {
document.output.Textarea1.value = strDetails
}


//-->
		</script>
	</HEAD>
	<body onload="connectRob();document.output.KRCVersion.focus();" OnUnload="disConnectRob();" id="MainFrame" bgcolor="#ffffff" background="../Images/bgMain_gHIGH.gif">
		<script language="JavaScript" src="<%Response.Write(urlRemoteScript)%>"></script>
		<script language="JavaScript">RSEnableRemoteScripting("<%Response.Write(paramRemoteScript)%>");</script>
		<font face="Arial" color="#ef8816">&nbsp;&nbsp;&nbsp; </font><big><big><strong><font face="Times New Roman" color="#000000">
						WEB DIAGNOSIS </font></strong></big></big>
		<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</p>
		<p>
		</p>
		<form name="output">
			<BR>
			<BR>
			<TABLE width="475" border="0">
				<tr>
					<td width="63"></td>
					<td width="398">
						<table border="0" height="123" bordercolorlight="#ffff99" cellspacing="0" cellpadding="2" width="396">
							<tr>
								<td width="100%" height="16%" bgcolor="#000040" colspan="2"><p align="center"><font color="#ffffff" size="5"><strong>SOFTWARE 
												INFO</strong></font></p>
								</td>
							</tr>
							<tr bgcolor="blue" valign="bottom">
								<td width="175" height="12%" bgcolor="#c0c0c0"><FONT face="Arial">KRC Version</FONT>
								</td>
								<td width="209" height="12%" bgcolor="#c0c0c0">
									<FONT face="Arial"><STRONG>:</STRONG></FONT> <input style="BORDER-RIGHT: rgb(192,192,192) groove; BORDER-TOP: medium none; BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; BACKGROUND-COLOR: rgb(255,255,0)" onfocus="return ShowDetails(document.output.KRCVersion.value)" name="KRCVersion">
								</td>
							</tr>
							<tr valign="bottom">
								<td width="175" height="12%" bgcolor="#ececec"><FONT face="Arial">Version</FONT>
								</td>
								<td width="209" height="12%" bgcolor="#ececec">
									<FONT face="Arial"><STRONG>:</STRONG></FONT> <input style="BORDER-RIGHT: rgb(192,192,192) groove; BORDER-TOP: rgb(192,192,192); BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; BACKGROUND-COLOR: rgb(255,255,0)" onfocus="return ShowDetails(document.output.Version.value)" name="Version">
								</td>
							</tr>
							<tr valign="bottom">
								<td width="175" height="12%" bgcolor="#c0c0c0"><FONT face="Arial">Kernel Version</FONT>
								</td>
								<td width="209" height="12%" bgcolor="#c0c0c0"><FONT face="Arial"><STRONG>:</STRONG></FONT>
									<input style="BORDER-RIGHT:rgb(192,192,192) groove; BORDER-TOP:medium none; BORDER-LEFT:medium none; BORDER-BOTTOM:rgb(192,192,192) groove; BACKGROUND-COLOR:rgb(255,255,0)" onfocus="return ShowDetails(document.output.GSVer.value)" name="GSVer">
								</td>
							</tr>
							<tr valign="bottom">
								<td width="175" height="12%" bgcolor="#ececec"><FONT face="Arial">BOF Version</FONT>
								</td>
								<td width="209" height="12%" bgcolor="#ececec"><FONT face="Arial"><STRONG>:</STRONG></FONT>
									<input style="BORDER-RIGHT:rgb(192,192,192) groove; BORDER-TOP:medium none; BORDER-LEFT:medium none; BORDER-BOTTOM:rgb(192,192,192) groove; BACKGROUND-COLOR:rgb(255,255,0)" onfocus="return ShowDetails(document.output.BOFVer.value)" name="BOFVer">
								</td>
							</tr>
							<tr valign="bottom">
								<td width="175" height="12%" bgcolor="#c0c0c0"><FONT face="Arial">R1Mada Version</FONT>
								</td>
								<td width="209" height="12%" bgcolor="#c0c0c0"><FONT face="Arial"><STRONG>:</STRONG></FONT>
									<input style="BORDER-RIGHT: rgb(192,192,192) groove; BORDER-TOP: medium none; BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; BACKGROUND-COLOR: rgb(255,255,0)" onfocus="return ShowDetails(document.output.R1MadaVer.value)" name="R1MadaVer">
								</td>
							</tr>
							<tr valign="bottom">
								<td width="175" height="12%" bgcolor="#ececec"><FONT face="Arial">SteuMada Version</FONT>
								</td>
								<td width="209" height="12%" bgcolor="#ececec"><FONT face="Arial"><STRONG>:</STRONG></FONT>
									<input style="BORDER-RIGHT:rgb(192,192,192) groove; BORDER-TOP:medium none; BORDER-LEFT:medium none; BORDER-BOTTOM:rgb(192,192,192) groove; BACKGROUND-COLOR:rgb(255,255,0)" onfocus="return ShowDetails(document.output.SteuMadaVer.value)" name="SteuMadaVer">
								</td>
							</tr>
						</table>
						<TEXTAREA id="Textarea1" title="" style="FONT-SIZE: x-small; WIDTH: 396px; BORDER-TOP-STYLE: none; FONT-FAMILY: Arial; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 50px; BACKGROUND-COLOR: silver; BORDER-BOTTOM-STYLE: none" name="Textarea1" rows="3" readOnly cols="47" defaultvalue="Test"></TEXTAREA>
					</td>
				</tr>
			</TABLE>
		</form>
		<script language="JavaScript">
var serverURL;
var failerFlag=false;
var errorMsg;

function readVar() {
	var serverRequest;
	var localFailFlag=false;
	serverRequest=serverURL.getVar("$RCV_INFO[]");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
		document.output.GSVer.value=errorMsg;
		localFailFlag=true;
	}
	else	
		document.output.GSVer.value=serverRequest.return_value;
		
	serverRequest=serverURL.getVar("$V_R1MADA[]");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
		document.output.R1MadaVer.value=errorMsg;
		localFailFlag=true;
	}
	else	
		document.output.R1MadaVer.value=serverRequest.return_value;
	
	serverRequest=serverURL.getVar("$V_STEUMADA[]");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
		document.output.SteuMadaVer.value=errorMsg;
		localFailFlag=true;
	}
	else	
		document.output.SteuMadaVer.value=serverRequest.return_value;
		
	serverRequest=serverURL.GetBOFVer();
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
		document.output.BOFVer.value=errorMsg;
		localFailFlag=true;
	}
	else	
		document.output.BOFVer.value=serverRequest.return_value;
		
	serverRequest=serverURL.ReadRegVal("Version/KRCVersion");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
		document.output.KRCVersion.value=errorMsg;
		localFailFlag=true;
	}
	else	
		document.output.KRCVersion.value=serverRequest.return_value;

	serverRequest=serverURL.ReadRegVal("Version/Version");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
		document.output.Version.value=errorMsg;
		localFailFlag=true;
	}
	else	
		document.output.Version.value=serverRequest.return_value;
	
	if (localFailFlag) {
		alert("Remote-Aufruf fehlgeschlagen!");
		failureFlag=true;
	}
}

function connectRob() {
	var serverRequest;
	errorMsg="Could not read variable"
	serverURL=RSGetASPObject("<%Response.Write(aspToOCXInter)%>");
	serverRequest=serverURL.ConnectRob();
	readVar();
}

function disConnectRob() {
	if (!failerFlag)
		serverRequest=serverURL.DisconnectRob();
}
		</script>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
	</body>
</HTML>
