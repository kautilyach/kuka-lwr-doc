<%@ Language=VBScript %>
<% 
	protocol="http://"
	path="/_ScriptLibrary"
	rsFile="/rs.htm"
	serverName=Request.ServerVariables("SERVER_NAME")
	
	urlRemoteScript=protocol+serverName+path+rsFile
	paramRemoteScript=protocol+serverName+path	
	aspToOCXInter=paramRemoteScript+"/Interfaces/WebRobot.asp"
%>
<html>
<head>
<title>Web Diagnosis-Main frame</title>
<script language="JavaScript" src="General_Main.js" type="text/javascript">
</script>
</head>

<body  onload="connectRob();" OnUnload="disConnectRob();" id="MainFrame" bgcolor="#ffffff" background="../Images/bgMain_gHIGH.gif" >
<script language="JavaScript" src="<%Response.Write(urlRemoteScript)%>"></script>
<script language="JavaScript">RSEnableRemoteScripting("<%Response.Write(paramRemoteScript)%>");</script>
<font face="Arial" color="#ef8816">&nbsp;&nbsp;&nbsp; </font>
<big><big><strong><font face="Times New Roman" color="#000000">WEB DIAGNOSIS
</font></strong></big></big>


<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>


<p>&nbsp; </p>


<p>&nbsp; </p>
<form name="output">
<table border="0" width="420">
 <tr><td width="50">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
 <td>
  <table border="0"  height="123" bordercolorlight="#ffff99" cellspacing="0"
   cellpadding="2" width="458" >
   <tr>
    <td width="436" height="33" bgcolor="#000040" colspan="2"><p align="center"><font
    color="#ffffff" size="5"><strong>ROBOT PROGRAM INFO</strong></font></p></td>
   </tr>
   <tr bgcolor="blue" valign="bottom">
    <td width="213" height="33" bgcolor="#c0c0c0"><font face="Arial" size="3">Program name</font>
    </td>
    <td width="233" height="33" bgcolor="#c0c0c0"> <FONT  face=Arial><STRONG>:</STRONG></FONT> 
        <input  style="BORDER-RIGHT: rgb(192,192,192) groove; BORDER-TOP: medium none; 
          BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; BACKGROUND-COLOR: rgb(255,255,0)" 
          name="ProName" size="24"> 
  </td>
   </tr>
   <tr valign="bottom">
    <td width="213" height="33" bgcolor="#ececec"><font face="Arial" size="3">Program IP name</font>
    </td>
    <td width="233" height="33" bgcolor="#ececec"> <FONT  face=Arial><STRONG>:</STRONG></FONT> 
        <input  style="BORDER-RIGHT: rgb(192,192,192) groove; BORDER-TOP: medium none; 
          BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; BACKGROUND-COLOR: rgb(255,255,0)" 
          name="ProIPName" size="24"> 
     </td>
   </tr>
   <tr valign="bottom">
    <td width="213" height="34" bgcolor="#c0c0c0"><font face="Arial" size="3">Program State</font>
    </td>
    <td width="233" height="34" bgcolor="#c0c0c0"><FONT  face=Arial><STRONG>:</STRONG></FONT> 
        <input  style="BORDER-RIGHT: rgb(192,192,192) groove; BORDER-TOP: medium none; 
          BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; BACKGROUND-COLOR: rgb(255,255,0)" 
          name="ProState" size="24"> 
    </td>
   </tr>
   <tr valign="bottom">
    <td width="213" height="36" bgcolor="#ececec"><font face="Arial" size="3">Program Mode</font>
    </td>
    <td width="233" height="36" bgcolor="#ececec"><FONT  face=Arial><STRONG>:</STRONG></FONT> 
        <input  style="BORDER-RIGHT: rgb(192,192,192) groove; BORDER-TOP: medium none; 
          BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; BACKGROUND-COLOR: rgb(255,255,0)" 
          name="ProMode" size="24"> 
    </td>
   </tr>
   <tr valign="bottom">
    <td width="213" height="35" bgcolor="c0c0c0"><font face="Arial" size="3">Program Waiting for</font>
    </td>
    <td width="233" height="35" bgcolor="#c0c0c0"><FONT  face=Arial><STRONG>:</STRONG></FONT> 
        <input  style="BORDER-RIGHT: rgb(192,192,192) groove; BORDER-TOP: medium none; 
          BORDER-LEFT: medium none; BORDER-BOTTOM: rgb(192,192,192) groove; BACKGROUND-COLOR: rgb(255,255,0)" 
          name="WaitFor" size="24"> 
    </td>
   </tr>
  </table>
 </td>
</tr>
</table>
</form>
<script language="JavaScript">
var serverURL;
var failerFlag=false;
var errorMsg;

function readVar() {
	var serverRequest;
	var localFailFlag=false;
	serverRequest=serverURL.getVar("$PRO_NAME1[]");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
		document.output.ProName.value=errorMsg;
		localFailFlag=true;
	}
	else{
	    if (serverRequest.return_value=="")
	       document.output.ProName.value="No program is selected";
	    else
		   document.output.ProName.value=serverRequest.return_value;
	}	
	serverRequest=serverURL.getVar("$Pro_IP.Name[]");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
		document.output.ProIPName.value=errorMsg;
		localFailFlag=true;
	}
	else{
	    if (serverRequest.return_value=="")
	       document.output.ProIPName.value="No program is selected";
	    else
		   document.output.ProIPName.value=serverRequest.return_value;
	}	
	
	serverRequest=serverURL.getVar("$Pro_State1");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
		document.output.ProState.value=errorMsg;
		localFailFlag=true;
	}
	else	
		document.output.ProState.value=serverRequest.return_value;
		
	serverRequest=serverURL.getVar("$Wait_For[]");;
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
		document.output.WaitFor.value=errorMsg;
		localFailFlag=true;
	}
	else	
		document.output.WaitFor.value=serverRequest.return_value;
		
	serverRequest=serverURL.getVar("$Pro_Mode");
	if (serverRequest.status==-1 || serverRequest.return_value=="failed") {
		document.output.ProMode.value=errorMsg;
		localFailFlag=true;
	}
	else	
		document.output.ProMode.value=serverRequest.return_value;

	
	if (localFailFlag) {
		alert("Remote-Aufruf fehlgeschlagen!");
		failureFlag=true;
	}
}

function connectRob() {
	var serverRequest;
	errorMsg="Could not read variable"
	serverURL=RSGetASPObject("<%Response.Write(aspToOCXInter)%>");
	serverRequest=serverURL.ConnectRob();
	readVar();
}

function disConnectRob() {
	if (!failerFlag)
		serverRequest=serverURL.DisconnectRob();
}
</script>

<p>&nbsp;</p>

<p>&nbsp;</p>
</body>
</html>
