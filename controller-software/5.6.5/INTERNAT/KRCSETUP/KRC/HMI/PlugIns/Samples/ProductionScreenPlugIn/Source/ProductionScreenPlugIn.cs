using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using KUKARoboter.PlugIn;
using KUKARoboter.KRCModel;


namespace KUKARoboter.ProductionScreenPlugIn
{
	/// <summary>
	/// Summary description for UserControl1.
	/// </summary>
   [PlugInPlacement(DispPlacement.Left, 
       DisplayMode   = DispMode.Normal,
       CommandBarMode = CmdBarMode.Normal)]
   public class ProductionScreenPlugIn : PlugInBase
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.Container components = null;

      public ProductionScreenPlugIn(DispMode DisplayMode, CmdBarMode CommandBarMode) : base(DisplayMode, CommandBarMode)
      {
         // This call is required by the Windows.Forms Form Designer.
         InitializeComponent();
         
         // TODO: Add any initialization after the InitForm call
         this.StartPoint = "XP1";
         this.SecondPoint = "XP2";
         this.EndPoint = "XP10";
      }

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      protected override void Dispose( bool disposing )
      {
         if( disposing )
         {
            if( components != null )
               components.Dispose();
         }
         base.Dispose( disposing );
      }

		#region Component Designer generated code
      /// <summary>
      /// Required method for Designer support - do not modify 
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ProductionScreenPlugIn));
         this.AverageCycleTimeText = new System.Windows.Forms.TextBox();
         this.PartCount = new System.Windows.Forms.Label();
         this.pictureBox1 = new System.Windows.Forms.PictureBox();
         this.XP1 = new KUKARoboter.ProductionScreenPlugIn.ActionPoint();
         this.XP2 = new KUKARoboter.ProductionScreenPlugIn.ActionPoint();
         this.XP3 = new KUKARoboter.ProductionScreenPlugIn.ActionPoint();
         this.XP4 = new KUKARoboter.ProductionScreenPlugIn.ActionPoint();
         this.XP5 = new KUKARoboter.ProductionScreenPlugIn.ActionPoint();
         this.XP6 = new KUKARoboter.ProductionScreenPlugIn.ActionPoint();
         this.XP7 = new KUKARoboter.ProductionScreenPlugIn.ActionPoint();
         this.XP8 = new KUKARoboter.ProductionScreenPlugIn.ActionPoint();
         this.XP9 = new KUKARoboter.ProductionScreenPlugIn.ActionPoint();
         this.XP10 = new KUKARoboter.ProductionScreenPlugIn.ActionPoint();
         this.MaxCycleTimeText = new System.Windows.Forms.TextBox();
         this.PassedPointsText = new System.Windows.Forms.TextBox();
         this.LastCycleTimeText = new System.Windows.Forms.TextBox();
         this.MinCycleTimeText = new System.Windows.Forms.TextBox();
         this.label4 = new System.Windows.Forms.Label();
         this.label5 = new System.Windows.Forms.Label();
         this.label6 = new System.Windows.Forms.Label();
         this.label1 = new System.Windows.Forms.Label();
         this.label2 = new System.Windows.Forms.Label();
         this.label3 = new System.Windows.Forms.Label();
         this.pictureBox1.SuspendLayout();
         this.SuspendLayout();
         // 
         // AverageCycleTimeText
         // 
         this.AverageCycleTimeText.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
         this.AverageCycleTimeText.Location = new System.Drawing.Point(432, 208);
         this.AverageCycleTimeText.Name = "AverageCycleTimeText";
         this.AverageCycleTimeText.TabIndex = 6;
         this.AverageCycleTimeText.Text = "";
         // 
         // PartCount
         // 
         this.PartCount.Font = new System.Drawing.Font("Courier New", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
         this.PartCount.Location = new System.Drawing.Point(248, 56);
         this.PartCount.Name = "PartCount";
         this.PartCount.Size = new System.Drawing.Size(168, 64);
         this.PartCount.TabIndex = 11;
         this.PartCount.Text = "0";
         // 
         // pictureBox1
         // 
         this.pictureBox1.BackgroundImage = ((System.Drawing.Bitmap)(resources.GetObject("pictureBox1.BackgroundImage")));
         this.pictureBox1.Controls.AddRange(new System.Windows.Forms.Control[] {
                                                                                  this.XP1,
                                                                                  this.XP2,
                                                                                  this.XP3,
                                                                                  this.XP4,
                                                                                  this.XP5,
                                                                                  this.XP6,
                                                                                  this.XP7,
                                                                                  this.XP8,
                                                                                  this.XP9,
                                                                                  this.XP10});
         this.pictureBox1.Location = new System.Drawing.Point(0, 16);
         this.pictureBox1.Name = "pictureBox1";
         this.pictureBox1.Size = new System.Drawing.Size(240, 264);
         this.pictureBox1.TabIndex = 10;
         this.pictureBox1.TabStop = false;
         // 
         // XP1
         // 
         this.XP1.BackColor = System.Drawing.Color.Transparent;
         this.XP1.Location = new System.Drawing.Point(8, 1);
         this.XP1.Name = "XP1";
         this.XP1.PointState = KUKARoboter.ProductionScreenPlugIn.ActionPoint.State.InActive;
         this.XP1.Size = new System.Drawing.Size(24, 24);
         this.XP1.TabIndex = 11;
         // 
         // XP2
         // 
         this.XP2.BackColor = System.Drawing.Color.Transparent;
         this.XP2.Location = new System.Drawing.Point(50, 1);
         this.XP2.Name = "XP2";
         this.XP2.PointState = KUKARoboter.ProductionScreenPlugIn.ActionPoint.State.InActive;
         this.XP2.Size = new System.Drawing.Size(24, 24);
         this.XP2.TabIndex = 11;
         // 
         // XP3
         // 
         this.XP3.BackColor = System.Drawing.Color.Transparent;
         this.XP3.Location = new System.Drawing.Point(92, 4);
         this.XP3.Name = "XP3";
         this.XP3.PointState = KUKARoboter.ProductionScreenPlugIn.ActionPoint.State.InActive;
         this.XP3.Size = new System.Drawing.Size(24, 24);
         this.XP3.TabIndex = 11;
         // 
         // XP4
         // 
         this.XP4.BackColor = System.Drawing.Color.Transparent;
         this.XP4.Location = new System.Drawing.Point(150, 60);
         this.XP4.Name = "XP4";
         this.XP4.PointState = KUKARoboter.ProductionScreenPlugIn.ActionPoint.State.InActive;
         this.XP4.Size = new System.Drawing.Size(24, 24);
         this.XP4.TabIndex = 11;
         // 
         // XP5
         // 
         this.XP5.BackColor = System.Drawing.Color.Transparent;
         this.XP5.Location = new System.Drawing.Point(200, 120);
         this.XP5.Name = "XP5";
         this.XP5.PointState = KUKARoboter.ProductionScreenPlugIn.ActionPoint.State.InActive;
         this.XP5.Size = new System.Drawing.Size(24, 24);
         this.XP5.TabIndex = 12;
         // 
         // XP6
         // 
         this.XP6.BackColor = System.Drawing.Color.Transparent;
         this.XP6.Location = new System.Drawing.Point(208, 160);
         this.XP6.Name = "XP6";
         this.XP6.PointState = KUKARoboter.ProductionScreenPlugIn.ActionPoint.State.InActive;
         this.XP6.Size = new System.Drawing.Size(24, 24);
         this.XP6.TabIndex = 12;
         // 
         // XP7
         // 
         this.XP7.BackColor = System.Drawing.Color.Transparent;
         this.XP7.Location = new System.Drawing.Point(208, 210);
         this.XP7.Name = "XP7";
         this.XP7.PointState = KUKARoboter.ProductionScreenPlugIn.ActionPoint.State.InActive;
         this.XP7.Size = new System.Drawing.Size(24, 24);
         this.XP7.TabIndex = 12;
         // 
         // XP8
         // 
         this.XP8.BackColor = System.Drawing.Color.Transparent;
         this.XP8.Location = new System.Drawing.Point(200, 240);
         this.XP8.Name = "XP8";
         this.XP8.PointState = KUKARoboter.ProductionScreenPlugIn.ActionPoint.State.InActive;
         this.XP8.Size = new System.Drawing.Size(24, 24);
         this.XP8.TabIndex = 13;
         // 
         // XP9
         // 
         this.XP9.BackColor = System.Drawing.Color.Transparent;
         this.XP9.Location = new System.Drawing.Point(100, 233);
         this.XP9.Name = "XP9";
         this.XP9.PointState = KUKARoboter.ProductionScreenPlugIn.ActionPoint.State.InActive;
         this.XP9.Size = new System.Drawing.Size(24, 24);
         this.XP9.TabIndex = 13;
         // 
         // XP10
         // 
         this.XP10.BackColor = System.Drawing.Color.Transparent;
         this.XP10.Location = new System.Drawing.Point(32, 205);
         this.XP10.Name = "XP10";
         this.XP10.PointState = KUKARoboter.ProductionScreenPlugIn.ActionPoint.State.InActive;
         this.XP10.Size = new System.Drawing.Size(24, 24);
         this.XP10.TabIndex = 14;
         // 
         // MaxCycleTimeText
         // 
         this.MaxCycleTimeText.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
         this.MaxCycleTimeText.Location = new System.Drawing.Point(432, 152);
         this.MaxCycleTimeText.Name = "MaxCycleTimeText";
         this.MaxCycleTimeText.TabIndex = 5;
         this.MaxCycleTimeText.Text = "";
         // 
         // PassedPointsText
         // 
         this.PassedPointsText.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
         this.PassedPointsText.Location = new System.Drawing.Point(432, 264);
         this.PassedPointsText.Name = "PassedPointsText";
         this.PassedPointsText.TabIndex = 9;
         this.PassedPointsText.Text = "";
         // 
         // LastCycleTimeText
         // 
         this.LastCycleTimeText.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
         this.LastCycleTimeText.Location = new System.Drawing.Point(432, 40);
         this.LastCycleTimeText.Name = "LastCycleTimeText";
         this.LastCycleTimeText.TabIndex = 0;
         this.LastCycleTimeText.Text = "";
         // 
         // MinCycleTimeText
         // 
         this.MinCycleTimeText.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
         this.MinCycleTimeText.Location = new System.Drawing.Point(432, 96);
         this.MinCycleTimeText.Name = "MinCycleTimeText";
         this.MinCycleTimeText.TabIndex = 3;
         this.MinCycleTimeText.Text = "";
         // 
         // label4
         // 
         this.label4.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
         this.label4.Location = new System.Drawing.Point(432, 184);
         this.label4.Name = "label4";
         this.label4.Size = new System.Drawing.Size(112, 23);
         this.label4.TabIndex = 7;
         this.label4.Text = "Average Cycle Time";
         // 
         // label5
         // 
         this.label5.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
         this.label5.Location = new System.Drawing.Point(432, 240);
         this.label5.Name = "label5";
         this.label5.TabIndex = 8;
         this.label5.Text = "Passed Points";
         // 
         // label6
         // 
         this.label6.Location = new System.Drawing.Point(248, 40);
         this.label6.Name = "label6";
         this.label6.TabIndex = 12;
         this.label6.Text = "Parts Produced";
         // 
         // label1
         // 
         this.label1.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
         this.label1.Location = new System.Drawing.Point(432, 16);
         this.label1.Name = "label1";
         this.label1.TabIndex = 1;
         this.label1.Text = "Last Cycle Time";
         // 
         // label2
         // 
         this.label2.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
         this.label2.Location = new System.Drawing.Point(432, 72);
         this.label2.Name = "label2";
         this.label2.TabIndex = 2;
         this.label2.Text = "Min. Cycle Time";
         // 
         // label3
         // 
         this.label3.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
         this.label3.Location = new System.Drawing.Point(432, 128);
         this.label3.Name = "label3";
         this.label3.TabIndex = 4;
         this.label3.Text = "Max. Cycle Time";
         // 
         // ProductionScreenPlugIn
         // 
         this.Controls.AddRange(new System.Windows.Forms.Control[] {
                                                                      this.label6,
                                                                      this.PartCount,
                                                                      this.pictureBox1,
                                                                      this.PassedPointsText,
                                                                      this.label5,
                                                                      this.label4,
                                                                      this.AverageCycleTimeText,
                                                                      this.MaxCycleTimeText,
                                                                      this.label3,
                                                                      this.MinCycleTimeText,
                                                                      this.label2,
                                                                      this.label1,
                                                                      this.LastCycleTimeText});
         this.Name = "ProductionScreenPlugIn";
         this.Size = new System.Drawing.Size(544, 360);
         this.Load += new System.EventHandler(this.ProductionScreenPlugIn_Load);
         this.pictureBox1.ResumeLayout(false);
         this.ResumeLayout(false);

      }
		#endregion

      [System.Runtime.InteropServices.DllImport("Kernel32.dll")]
      private static extern bool QueryPerformanceCounter(ref long lpPerformanceCount);
      [System.Runtime.InteropServices.DllImport("Kernel32.dll")]
      private static extern bool QueryPerformanceFrequency(ref long lpFrequency);
      private string m_StartPoint;
      private string m_SecondPoint;
      private string m_EndPoint;
      private string m_currPoint;
      private string m_PreviousPoint = "";
      private int m_PointCount = 0;
      private Image m_Image = null;
      private double m_CycleTime;
      public event EventHandler CycleTimeChanged;
      bool m_IsTimerRunning = false;
      private System.Windows.Forms.TextBox LastCycleTimeText;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.TextBox MinCycleTimeText;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.TextBox MaxCycleTimeText;
      private System.Windows.Forms.TextBox AverageCycleTimeText;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.TextBox PassedPointsText;
      private long m_TimerStartValue;
      private double m_MinCycleTime;
      private double m_MaxCycleTime;
      private int m_CycleCount = 0;
      private double m_CycleTimeSum = 0;
      private int m_PartsProduced = 0;
      private System.Windows.Forms.PictureBox pictureBox1;
      private KUKARoboter.ProductionScreenPlugIn.ActionPoint XP1;
      private KUKARoboter.ProductionScreenPlugIn.ActionPoint XP2;
      private KUKARoboter.ProductionScreenPlugIn.ActionPoint XP3;
      private KUKARoboter.ProductionScreenPlugIn.ActionPoint XP4;
      private KUKARoboter.ProductionScreenPlugIn.ActionPoint XP5;
      private KUKARoboter.ProductionScreenPlugIn.ActionPoint XP6;
      private KUKARoboter.ProductionScreenPlugIn.ActionPoint XP7;
      private KUKARoboter.ProductionScreenPlugIn.ActionPoint XP8;
      private KUKARoboter.ProductionScreenPlugIn.ActionPoint XP9;
      private KUKARoboter.ProductionScreenPlugIn.ActionPoint XP10;
      private System.Windows.Forms.Label PartCount;
      private System.Windows.Forms.Label label6;
      
      
      private ProdInfo m_ProdInfo;

      private double MinCycleTime
      {
         get{return m_MinCycleTime;}
         set
         {
            m_MinCycleTime = value;
            this.MinCycleTimeText.Text = m_MinCycleTime.ToString("f3");
         }
      }
      private double MaxCycleTime
      {
         get{return m_MaxCycleTime;}
         set
         {
            m_MaxCycleTime = value;
            this.MaxCycleTimeText.Text = m_MaxCycleTime.ToString("f3");
         }
      }

      private double LastCycleTime
      {
         set
         {
            this.LastCycleTimeText.Text = value.ToString("f3");
         }
      }

      private double AverageCycleTime
      {
         get
         {
            if(m_CycleCount>0)
               return (m_CycleTimeSum/(double)m_CycleCount);
            else
               return(0);
         }
      }


    
      private void updateStatistics()
      {
         m_CycleCount ++;
         m_CycleTimeSum += m_CycleTime;
         LastCycleTime = m_CycleTime;
         if(MinCycleTime == 0)
         {
            MinCycleTime = m_CycleTime;
         }
         else if(m_CycleTime < MinCycleTime)
         {
            MinCycleTime = m_CycleTime;
         }

         if(MaxCycleTime == 0)
         {
            MaxCycleTime = m_CycleTime;
         }
         else if(m_CycleTime > MaxCycleTime)
         {
            MaxCycleTime = m_CycleTime;
         }
         AverageCycleTimeText.Text = AverageCycleTime.ToString("f3");
         PassedPointsText.Text = m_PointCount.ToString();
      }

      private void OnNextPoint(object sender, EventArgs e)
      {
         string np = m_ProdInfo.NextPoint.Replace("\"","");
         NextPoint(np);
      }
      
      public void NextPoint(string Name)
      {
         m_PointCount++;
         m_currPoint = Name;
         
         if(IsCycleFinished())
         {
            if(m_IsTimerRunning)
            {
               long TimerStopValue = 0;
               QueryPerformanceCounter(ref TimerStopValue);
               long delta = TimerStopValue - m_TimerStartValue;
               long Frequency = 1;
               QueryPerformanceFrequency(ref Frequency);
               PartsProduced ++;
               m_CycleTime = (double)delta / (double)Frequency;
               updateStatistics();
               QueryPerformanceCounter(ref m_TimerStartValue);
               OnCycleTimeChanged();
            }
            else
            {
               m_IsTimerRunning = true;
               QueryPerformanceCounter(ref m_TimerStartValue);
            }
         }

         if(needClearing())
         {
            foreach(ActionPoint c in this.pictureBox1.Controls)
            {
               if(c.Name != Name)
               {
                  c.Hide();
               }
               c.PointState = KUKARoboter.ProductionScreenPlugIn.ActionPoint.State.InActive;
            }
         }
         foreach(ActionPoint ac in this.pictureBox1.Controls)
         {
            if(ac.Name == m_PreviousPoint)
            {
               ac.PointState = KUKARoboter.ProductionScreenPlugIn.ActionPoint.State.Active;
               break;
            }
         }
         TransferePointToDrawList(Name);
         m_PreviousPoint = Name;
      }
      public bool IsCycleFinished()
      {
         return(!IsFirstCycle() && m_PreviousPoint == m_StartPoint && m_currPoint == m_SecondPoint);
      }
      private bool needClearing()
      {
         return(m_PreviousPoint == m_EndPoint && m_currPoint == m_StartPoint);
      }
      
      public int PointCount
      {
         get{return m_PointCount;}
      }

      public double CycleTime
      {
         get{return m_CycleTime;}
      }
      
      private void OnCycleTimeChanged()
      {
         this.LastCycleTime = m_CycleTime;
         if(CycleTimeChanged != null)
            CycleTimeChanged(this, EventArgs.Empty);
      }

      private bool IsFirstCycle()
      {
         //Eine Art Hack
         return(m_PointCount< 3);
      }

      private int PartsProduced
      {
         get{ return m_PartsProduced;}
         set
         { 
            m_PartsProduced = value;
            PartCount.Text = m_PartsProduced.ToString();
         }
      }

      private void TransferePointToDrawList(string Name)
      {
         foreach(Control c in this.pictureBox1.Controls)
         {
            if(c.Name == Name)
            {
               c.Show();
               break;
            }
         }
      }

      private string StartPoint
      {
         get{return m_StartPoint;}
         set{m_StartPoint = value;}
      }

      private string SecondPoint
      {
         get{return m_SecondPoint;}
         set{m_SecondPoint = value;}
      }

      private string EndPoint
      {
         get{return m_EndPoint;}
         set{m_EndPoint = value;}
      }

      public string ImageFile
      {
         set{m_Image = Image.FromFile(value);}
      }

//      protected override void OnPaint(PaintEventArgs e)
//      {
//         if(m_Image != null)
//         {
//            e.Graphics.DrawImageUnscaled(m_Image, 0, 0);
//         }
//         Pen RedPen = new Pen(Color.Red, 2);
//         foreach(Point p in m_DrawPoints)
//         {
//            e.Graphics.DrawEllipse(RedPen,p.X,p.Y,2,2);
//         }
//         base.OnPaint(e);
//      }

      private void ProductionScreenPlugIn_Load(object sender, System.EventArgs e)
      {
         m_ProdInfo = new ProdInfo();
         this.m_ProdInfo.NextPointChanged += new System.EventHandler(this.OnNextPoint);
         foreach(Control c in this.pictureBox1.Controls)
         {
            c.Hide();
         }
         m_ProdInfo.Init();
      }


	}
}
