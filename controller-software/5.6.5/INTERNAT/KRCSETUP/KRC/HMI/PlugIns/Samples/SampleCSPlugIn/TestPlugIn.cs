using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using KUKARoboter.PlugIn;
using KUKARoboter.KRCModel;
using KUKARoboter.KRCModel.Commands;

namespace KUKARoboter.SampleCSPlugIn
{
	/// <summary>
	/// Summary description for UserControl1.
	/// </summary>
	/// 
   [PlugInPlacement(DispPlacement.Bottom, 
                    DisplayMode   = DispMode.Normal,
                    CommandBarMode = CmdBarMode.Normal)]
	public class CSPlugIn : PlugInBase, IPlugIn
	{
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.RadioButton radioButton1;
      private System.Windows.Forms.RadioButton radioButton2;
      private System.Windows.Forms.TrackBar trackBar1;
      private System.Windows.Forms.TrackBar trackBar2;
      private System.Windows.Forms.TrackBar trackBar3;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public CSPlugIn(DispMode DispMode, CmdBarMode CommandBarMode) : base(DispMode, CommandBarMode)
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

         thisPlugInDisplay.ParamKeyBar[0] = new ParamKeyButton();
         thisPlugInDisplay.ParamKeyBar[0].Text = "Track 1";
         thisPlugInDisplay.ParamKeyBar[0].Value = this.trackBar1.Value.ToString();
         thisPlugInDisplay.ParamKeyBar[0].Increment += new IncrementEventHandler(OnIncParam0);

         thisPlugInDisplay.ParamKeyBar[1] = new ParamKeyButton();
         thisPlugInDisplay.ParamKeyBar[1].Text = "Group 1";
         thisPlugInDisplay.ParamKeyBar[1].Value = this.radioButton1.Checked ? "R1" : "R2";
         thisPlugInDisplay.ParamKeyBar[1].Increment += new IncrementEventHandler(OnIncParam1);

         thisPlugInDisplay.ParamKeyBar[2] = new ParamKeyButton();
         thisPlugInDisplay.ParamKeyBar[2].Text = "Track 2";
         thisPlugInDisplay.ParamKeyBar[2].Value = this.trackBar2.Value.ToString();
         thisPlugInDisplay.ParamKeyBar[2].Increment += new IncrementEventHandler(OnIncParam2);

         thisPlugInDisplay.ParamKeyBar[3] = new ParamKeyButton();
         thisPlugInDisplay.ParamKeyBar[3].Text = "Track 3";
         thisPlugInDisplay.ParamKeyBar[3].Value = this.trackBar3.Value.ToString();
         thisPlugInDisplay.ParamKeyBar[3].Increment += new IncrementEventHandler(OnIncParam3);

      }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
            this.OnDisconnection();

				if( components != null )
					components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
         this.radioButton1 = new System.Windows.Forms.RadioButton();
         this.radioButton2 = new System.Windows.Forms.RadioButton();
         this.groupBox1 = new System.Windows.Forms.GroupBox();
         this.trackBar1 = new System.Windows.Forms.TrackBar();
         this.trackBar2 = new System.Windows.Forms.TrackBar();
         this.trackBar3 = new System.Windows.Forms.TrackBar();
         this.groupBox1.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.trackBar3)).BeginInit();
         this.SuspendLayout();
         // 
         // radioButton1
         // 
         this.radioButton1.Location = new System.Drawing.Point(16, 16);
         this.radioButton1.Name = "radioButton1";
         this.radioButton1.TabIndex = 0;
         this.radioButton1.TabStop = true;
         this.radioButton1.Text = "radioButton1";
         // 
         // radioButton2
         // 
         this.radioButton2.Location = new System.Drawing.Point(16, 40);
         this.radioButton2.Name = "radioButton2";
         this.radioButton2.TabIndex = 1;
         this.radioButton2.TabStop = true;
         this.radioButton2.Text = "radioButton2";
         // 
         // groupBox1
         // 
         this.groupBox1.Controls.AddRange(new System.Windows.Forms.Control[] {
                                                                                this.radioButton2,
                                                                                this.radioButton1});
         this.groupBox1.Location = new System.Drawing.Point(16, 111);
         this.groupBox1.Name = "groupBox1";
         this.groupBox1.Size = new System.Drawing.Size(192, 72);
         this.groupBox1.TabIndex = 2;
         this.groupBox1.TabStop = false;
         this.groupBox1.Text = "groupBox1";
         // 
         // trackBar1
         // 
         this.trackBar1.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right);
         this.trackBar1.Location = new System.Drawing.Point(16, 48);
         this.trackBar1.Name = "trackBar1";
         this.trackBar1.Size = new System.Drawing.Size(192, 45);
         this.trackBar1.TabIndex = 1;
         // 
         // trackBar2
         // 
         this.trackBar2.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right);
         this.trackBar2.Location = new System.Drawing.Point(16, 201);
         this.trackBar2.Name = "trackBar2";
         this.trackBar2.Size = new System.Drawing.Size(192, 45);
         this.trackBar2.TabIndex = 3;
         // 
         // trackBar3
         // 
         this.trackBar3.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right);
         this.trackBar3.Location = new System.Drawing.Point(16, 264);
         this.trackBar3.Name = "trackBar3";
         this.trackBar3.Size = new System.Drawing.Size(192, 45);
         this.trackBar3.TabIndex = 4;
         // 
         // CSPlugIn
         // 
         this.BackColor = System.Drawing.Color.Lime;
         this.Controls.AddRange(new System.Windows.Forms.Control[] {
                                                                      this.trackBar3,
                                                                      this.trackBar2,
                                                                      this.trackBar1,
                                                                      this.groupBox1});
         this.Name = "CSPlugIn";
         this.Size = new System.Drawing.Size(224, 328);
         this.groupBox1.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.trackBar3)).EndInit();
         this.ResumeLayout(false);

      }
		#endregion

      protected override void OnConnection()
      {
         base.OnConnection();

      }

      protected override void OnDisconnection()
      {
         base.OnDisconnection();
      }

      /// <summary>
      /// Das ist die Focus behandlung
      /// </summary>
      /// <param name="e">System.EventArgs</param>
      private void OnIncParam0(object sender,  KRCModel.Commands.IncrementEventArgs args)
      {
         this.trackBar1.Value += (this.trackBar1.SmallChange*(args.Plus?1:-1));
         thisPlugInDisplay.ParamKeyBar[0].Value = this.trackBar1.Value.ToString();
      }         

      private void OnIncParam1(object sender,  KRCModel.Commands.IncrementEventArgs args)
      {
         this.radioButton1.Checked = !this.radioButton1.Checked;
         this.radioButton2.Checked = !this.radioButton1.Checked;
         thisPlugInDisplay.ParamKeyBar[1].Value = this.radioButton1.Checked ? "R1" : "R2";
      }         

      private void OnIncParam2(object sender,  KRCModel.Commands.IncrementEventArgs args)
      {
         this.trackBar2.Value += (this.trackBar2.SmallChange*(args.Plus?1:-1));
         thisPlugInDisplay.ParamKeyBar[2].Value = this.trackBar2.Value.ToString();
      }         

      private void OnIncParam3(object sender,  KRCModel.Commands.IncrementEventArgs args)
      {
         this.trackBar3.Value += (this.trackBar3.SmallChange*(args.Plus?1:-1));
         thisPlugInDisplay.ParamKeyBar[3].Value = this.trackBar3.Value.ToString();
      }         
   }
}
