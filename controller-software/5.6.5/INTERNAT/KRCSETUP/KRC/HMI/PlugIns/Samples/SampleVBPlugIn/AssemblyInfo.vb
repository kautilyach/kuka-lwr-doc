Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("KUKA Roboter GmbH")> 
<Assembly: AssemblyProduct("")> 
<Assembly: AssemblyCopyright("Copyright (C) KUKA Roboter GmbH. 1998-2002. All rights reserved.")> 
<Assembly: AssemblyTrademark("")> 
<Assembly: CLSCompliant(True)> 

<Assembly: KUKARoboter.PlugIn.PlugInInside()> 


'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("73F2B94B-D2F8-4DF1-8692-FAD64D728B04")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("1.2.1.0")> 
<Assembly: AssemblyKeyFile("..\..\..\..\..\ExpW_DotNet\etc\KUKARoboter.snk")> 

