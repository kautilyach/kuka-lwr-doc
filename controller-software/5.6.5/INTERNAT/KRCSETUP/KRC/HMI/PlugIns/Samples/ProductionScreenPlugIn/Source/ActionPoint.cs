using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace KUKARoboter.ProductionScreenPlugIn
{
	/// <summary>
	/// Summary description for ActionPoint.
	/// </summary>
	public class ActionPoint : System.Windows.Forms.UserControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

      public enum State{ Active, InActive};

      private State m_State = State.InActive;

      public State PointState
      {
         get{return m_State;}
         set{
            if(m_State == value)
               return;
            m_State = value;
            Invalidate();
         }
      }

		public ActionPoint()
		{
         this.Size = new Size(24, 24);
         this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
         this.BackColor = Color.Transparent;

         // This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitForm call

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
         // 
         // ActionPoint
         // 
         this.Name = "ActionPoint";
         this.Size = new System.Drawing.Size(24, 24);

      }
		#endregion

      protected override void OnPaint(PaintEventArgs e)
      {
         SolidBrush brush;
         switch(PointState)
         {
            case State.Active: 
               brush = new SolidBrush(Color.Red);
               break;
            case State.InActive:
               brush = new SolidBrush(Color.Black);
               break;
            default:
               throw new System.Exception("Invalid State, maybe new State introduced");
         }
         e.Graphics.FillEllipse(brush,0,0,12,12);
         base.OnPaint(e);
      }
	}
}
