Imports KUKARoboter.PlugIn


Namespace SampleVBPlugIn

    <PlugInPlacementAttribute(DispPlacement.Bottom)> _
    Public Class VBPlugIn
        Inherits KUKARoboter.PlugIn.PlugInBase

#Region " Windows Form Designer generated code "

        Public Sub New(ByVal DisplayState As DispMode, ByVal CommandBarMode As CmdBarMode)
            MyBase.New(DisplayState, CommandBarMode)

            'This call is required by the Windows Form Designer.
            InitializeComponent()

            'Add any initialization after the InitializeComponent() call

        End Sub


        'UserControl1 overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If Not (components Is Nothing) Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub
        Friend WithEvents TrackBar1 As System.Windows.Forms.TrackBar

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.Container

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.TrackBar1 = New System.Windows.Forms.TrackBar()
            CType(Me.TrackBar1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'TrackBar1
            '
            Me.TrackBar1.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right)
            Me.TrackBar1.Location = New System.Drawing.Point(16, 56)
            Me.TrackBar1.Maximum = 100
            Me.TrackBar1.Name = "TrackBar1"
            Me.TrackBar1.Size = New System.Drawing.Size(216, 45)
            Me.TrackBar1.TabIndex = 1
            Me.TrackBar1.Value = 30
            '
            'UserControl1
            '
            Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.TrackBar1})
            Me.Name = "UserControl1"
            Me.Text = "VB PlugIn"
            Me.Size = New System.Drawing.Size(240, 288)
            CType(Me.TrackBar1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

    End Class
End Namespace
