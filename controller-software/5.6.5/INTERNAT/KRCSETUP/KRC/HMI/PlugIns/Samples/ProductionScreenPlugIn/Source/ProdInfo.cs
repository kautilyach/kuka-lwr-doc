using System;
using PrimaryInterOp.Cross3Krc;

namespace KUKARoboter.ProductionScreenPlugIn
{
   public class ProdInfo
   {
      private class CallBack  : ICKCallbackVar, IKCallbackError
      {
         private ICKAsyncVar AsyncVar = null;
         private string m_VarName = null;

         public delegate void OnSetInfoDelegate(string s);
      
         OnSetInfoDelegate onSetInfo = null;

         public CallBack(string VarName, OnSetInfoDelegate osi)
         {
            m_VarName = VarName;
            onSetInfo = osi;
         }

         public void Init()
         {
            KrcServiceFactory kf = new KrcServiceFactory();
            IKService IS = kf.GetService("WBC_KrcLib.AsyncVar","ProductionScreen");
            AsyncVar = IS as ICKAsyncVar;
            if(AsyncVar != null)
            {
               AsyncVar.SetInfo(this,1,m_VarName,10);
            }
            
         }
         
         void IKCallbackError.OnError(int x, ref TKMessage m)
         {

         }

         void IKCallbackError.OnDialog(int x, ref TKDialog d)
         {

         }

         void ICKCallbackVar.OnSetInfo(int x, string s)
         {
            
            if(onSetInfo != null)
               onSetInfo(s);
         }

         void ICKCallbackVar.OnSetMultiVar(int x)
         {
         }

         void ICKCallbackVar.OnSetVar(int x)
         {
         }

         void ICKCallbackVar.OnShowMultiVar(int x, Object o)
         {
         }

         void ICKCallbackVar.OnShowVar(int x, string s)
         {
         }
         
      }
      
      private string nextPoint = "";
      
      public event EventHandler NextPointChanged;
      
      CallBack MyCallBack = null;

      public ProdInfo()
      {
      }

      public void Init()
      {
         MyCallBack = new CallBack("$PRO_IP.P_NAME[]", new CallBack.OnSetInfoDelegate(this.SetInfo));
         MyCallBack.Init();
      }

      public void SetInfo(string s)
      {
         //eigentlich ueberfluessig, da Setinfo von sich aus schon nur Aenderungen mitteilt.
         if(nextPoint != s)
         {
            SetNextPoint(s);
         }
      }

      public string NextPoint
      {
         get{return nextPoint;}
      }
      
      private void SetNextPoint(string val)
      {
         nextPoint = val;
         OnNextPointChanged();
      }

      private void OnNextPointChanged()
      {
         if(NextPointChanged != null)
            NextPointChanged(this,EventArgs.Empty);
      }

	}
}
